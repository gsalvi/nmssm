#!/usr/bin/env python3
import argparse
import ROOT

from importlib import import_module
from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetHelperRun2 import createJMECorrector
# from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jecUncertainties import *
from PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer import puWeight_2016,puWeight_2017,puWeight_2018,puWeight_UL2016,puWeight_UL2017,puWeight_UL2018
from PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer import btagSF2016,btagSF2017
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.crossSectionScales import CrossSectionScales

ROOT.PyConfig.IgnoreCommandLineOptions = True


# Default input file to run over
input_file = ["python/postprocessing/NMSSM/test/test_sample.root"] # 155 events

# Argument parser
parser = argparse.ArgumentParser(description="Process some NanoAOD file(s) and adds jet related stuff to an output root file.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-i", "--input", default=input_file, nargs="+", help="Specifies the input file(s). Separate multiple root file inputs with a space, or use a text file with the data file names (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default=".", help="Name of output directory. (default = %(default)s)")
parser.add_argument("-n", "--maxEntries", default=None, type=int, help="Maximum number of events to process, per input file... (default = %(default)s)")
parser.add_argument("-y", "--year", type=str, help="The year of the input files, e.g. UL2018.", required=True)
parser.add_argument("-r", "--runPeriod", default=None, type=str, help="The run period of the input data, e.g. B", required=False)
parser.add_argument("-j", "--jsonInput", default=None, help="Select events using this JSON file. (default = %(default)s)")
parser.add_argument("--isData", action="store_true", help="If input are data. If not specified, then MC events are assumed.")
args = parser.parse_args()

if args.isData and not args.runPeriod:
    raise Exception("--runPeriod must be set when running on data")

# If input is text file with data filenames, extract each row into a list
# Currently only works for one input text file.
if ".root" not in args.input[0]:
    with open(args.input[0], "r", encoding="utf-8") as file:
        args.input = file.read().splitlines()

# Set output root file ending (can't do multiple datasets at same time or the output will have the wrong postfix)
if "store/mc" in args.input[0]:
    input_name = args.input[0].split("/")
    postfix = "_FromDataset_" + input_name[-5] + "_Skim"
else:
    postfix = None

# Golden JSON file: https://twiki.cern.ch/twiki/bin/view/CMS/PdmVLegacy2018Analysis#Data_Certification
if args.isData and args.jsonInput:
    json = args.jsonInput
elif args.isData:
    json = "python/postprocessing/NMSSM/utilities/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt"
else:
    json = None

# Create a chain of modules to run for the analysis
postproc_chain = []

# Check if data
if "store/data/" in args.input[0]:
    args.isData = True

# Add jet uncertainties modules. Adds JES related branches to the output root file.
# Bodge as UL2017 is missing data files for JER... or should I use UL2016/UL2018 instead?
if args.year == "UL2017":
    print("Warning: Year not defined for JME corrector. Run with default year 2017.")
    tmp_year = "2017"
else:
    tmp_year = args.year

# createJMECorrector must be run with a specific runPeriod for data (defaults to "B" otherwise)?  https://github.com/cms-nanoAOD/nanoAOD-tools/blob/master/python/postprocessing/modules/jme/jetmetHelperRun2.py#L113
jme_corrections_ak4 = createJMECorrector(isMC=(not args.isData), dataYear=tmp_year, runPeriod=args.runPeriod, jesUncert="Total", jetType="AK4PFPuppi")
postproc_chain.append(jme_corrections_ak4())

jme_corrections_ak8 = createJMECorrector(isMC=(not args.isData), dataYear=tmp_year, runPeriod=args.runPeriod, jesUncert="Total", jetType="AK8PFPuppi")
postproc_chain.append(jme_corrections_ak8())

# Add JEC uncertainties module. Do we need this one? It's crashing
# jec_uncert = jecUncertAll_cppOut
# postproc_chain.append(jec_uncert())

# Only do these steps for MC samples
if not args.isData:
    # Add cross-section scaling
    xsec_scale = CrossSectionScales()
    postproc_chain.append(xsec_scale)

    # Add pileup weight module
    if args.year == "2016":
        pu_weights = puWeight_2016
    elif args.year == "2017":
        pu_weights = puWeight_2017
    elif args.year == "2018":
        pu_weights = puWeight_2018
    elif args.year == "UL2016":
        pu_weights = puWeight_UL2016
    elif args.year == "UL2017":
        pu_weights = puWeight_UL2017
    elif args.year == "UL2018":
        pu_weights = puWeight_UL2018
    else:
        print("Warning: Year not defined for pileup weight module. Run with default year 2017.")
        pu_weights = puWeight_2017
    postproc_chain.append(pu_weights())

    # Add b-tag scale factor module. Not all years, UL, and b-tag discriminators supported.
    if "2016" in args.year:
        btag_sf = btagSF2016
    elif "2017" in args.year:
        btag_sf = btagSF2017
    else:
        print("Warning: Year not defined for b-tag scale factor module. Run with default year 2017.")
        btag_sf = btagSF2017
    postproc_chain.append(btag_sf())

    # Something for the PDF? Need to write that oneself...

# Preselection cuts
preselection = None
# preselection = "FatJet_pt[0] > 170 && FatJet_pt[1] > 170 && abs(FatJet_eta[0]) < 2.4 && abs(FatJet_eta[1]) < 2.4" # Jets seem to be ordered from high to low pt
# Does this pre-selection actually work... what if the first jet is outside our eta range, but the third fattest jet would pass the cuts


# Create post processor and run it
# Note: can't have more than on PostProcessor per run or it will seg fault when loading the JES correction files
p = PostProcessor(args.outputDir, args.input,
                  cut=preselection,
                  modules=postproc_chain,
                  maxEntries=args.maxEntries,
                  postfix=postfix,
                  provenance=True,
                  jsonInput=json)
p.run()
