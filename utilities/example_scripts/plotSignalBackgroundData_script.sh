
# Run Analysis

##################
# QCD Background #
##################

# Run taggers and HT bins seperately as it's too big

# ParticleNet
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230207_115630/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_122452/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_122019/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2500toInf_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

# ParticleNet NO EVENT SELECTION
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230207_115630/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_122452/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_122019/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2500toInf_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

# btagHbb
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT1000to1500_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230207_115630/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT1500to2000_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_122452/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/QCD_HT2000toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_122019/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2000toInf_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis

# # QCD Background interactive debug
# python3 python/postprocessing/NMSSM/analysisNMSSM.py \
# -I \
# /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/QCD_HT2000toInf_TuneCP5_PSWeights_13TeV-madgraphMLM-pythia8/NMSSMPostProc/221014_204642/0000/ \
# -O \
# test_debug \
# -t all -y 2016 -n 100



####################
# TTbar Background #
####################

# Run taggers seperately as it's too big

# ParticleNet
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_142725/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-800to1200_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_141743/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-1200to2500_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_145056/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-2500toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_123056/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_particleNet \
-t particleNet -y 2018 -g "Old" --runAnalysis

# ParticleNet NO EVENT SELECTION
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_142725/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-800to1200_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_141743/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-1200to2500_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_145056/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-2500toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_123056/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_particleNet_NoEventSelection \
-t particleNet -y 2018 -g "Old" --runAnalysis --noEventSelection

# btagHbb
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-600to800_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_142725/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-800to1200_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_141743/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-1200to2500_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3b/230206_145056/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/TTJets_HT-2500toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc_v3/230203_123056/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_btagHbb \
-t btagHbb -y 2018 -g "Old" --runAnalysis



#################
# NMSSSM Signal #
#################

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-30_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_114521/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-30_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-40_mSUSY_1200_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_114719/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-40_mSUSY_1200 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-40_mSUSY_1600_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_114915/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-40_mSUSY_1600 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-40_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_115115/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-40_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_120601/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_1200_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_115313/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_1200 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_1600_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_115511/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_1600 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_2000_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_115719/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2000 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_2400_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_115925/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2400 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_120145/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSUSY_3200_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_151026/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_3200 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-80_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_121013/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-100_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_113933/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-100_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-125_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_114132/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-125_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-155_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_114323/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-155_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-200_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230127_174047/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-200_mSUSY_2800 \
-t all -y 2018 -g "Old" --runAnalysis

# mSquark
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-60_mSquark_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_120758/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSquark_2800 \
-t all -y 2018 -g "Old" --runAnalysis

# No Event Selection
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-40_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_113933/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-40_mSUSY_2800_noEventSelection \
-t all -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-80_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_113933/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_noEventSelection \
-t all -y 2018 -g "Old" --runAnalysis --noEventSelection

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/NMSSMCascade_mH-100_mSUSY_2800_TuneCP5_13TeV-madgraph-pythia8/NMSSMPostProc_v3/230128_113933/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-100_mSUSY_2800_noEventSelection \
-t all -y 2018 -g "Old" --runAnalysis --noEventSelection



#####################
#     WJetsToQQ     #
#####################

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/WJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc/230206_152839/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/WJetsToQQ_HT-800toInf \
-t all -y 2018 -g "Old" --runAnalysis



#####################
#     ZJetsToQQ     #
#####################

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/linacre/NMSSM/ZJetsToQQ_HT-800toInf_TuneCP5_13TeV-madgraphMLM-pythia8/NMSSMPostProc/230206_152717/0000/ \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/ZJetsToQQ_HT-800toInf \
-t all -y 2018 -g "Old" --runAnalysis



####################
#       Data       #
####################

# ParticleNet

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0000 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n0 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0001 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n1 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0002 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n2 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0003 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n3 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0004 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n4 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0005 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n5 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-i \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0001/FromDataset_Run2018A_Skim_1703.root \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0002/FromDataset_Run2018A_Skim_2063.root \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0004/FromDataset_Run2018A_Skim_4628.root \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0004/FromDataset_Run2018A_Skim_4708.root \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n6 \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData
# Due to crab job failing two jobs even though they succeded in a previous run (which failed two other jobs: )...

# Broken files??
python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-i \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0000/FromDataset_Run2018A_Skim_642.root \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_broken \
-t particleNet -y 2018 -g "Old" --runAnalysis --isData

# btagHbb

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0000 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n0 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0001 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n1 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0002 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n2 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0003 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n3 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0004 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n4 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-I /pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230505_083327/0005 \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n5 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData

python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py \
-i \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0001/FromDataset_Run2018A_Skim_1703.root \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0002/FromDataset_Run2018A_Skim_2063.root \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0004/FromDataset_Run2018A_Skim_4628.root \
/pnfs/pp.rl.ac.uk/data/cms/store/user/meholmbe/NMSSM/JetHT/NMSSMPostProc/230327_154910/0004/FromDataset_Run2018A_Skim_4708.root \
-O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n6 \
-t btagHbb -y 2018 -g "Old" --runAnalysis --isData
# Due to crab job failing two jobs even though they succeded in a previous run (which failed two other jobs: )...


###########################
# Plot Search Region Bins #
###########################

# ParticleNet - Old mass grid
python3 python/postprocessing/NMSSM/utilities/plotSignalBackgroundDataHistograms.py \
-S \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2800_20230504_091721 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_20230504_091701 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-100_mSUSY_2800_20230504_091652 \
-s "m_{H}60 m_{SUSY}2800" "m_{H}80 m_{SUSY}2800" "m_{H}100 m_{SUSY}2800" \
-B \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_particleNet_20230512_101437 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_particleNet_20230512_101444 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2500toInf_particleNet_20230512_101507 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_particleNet_20230512_102017 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_particleNet_20230512_102325 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_particleNet_20230512_102421 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_particleNet_20230512_102528 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/WJetsToQQ_HT-800toInf_20230504_091550 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/ZJetsToQQ_HT-800toInf_20230504_091535 \
-b QCD TTJets "W+Jets" "Z+Jets" \
-t particleNet -O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/plots/ --cutsHT 2000

# btagHbb
python3 python/postprocessing/NMSSM/utilities/plotSignalBackgroundDataHistograms.py \
-S \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2800_20230504_091721 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_20230504_091701 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-100_mSUSY_2800_20230504_091652 \
-s "m_{H}60 m_{SUSY}2800" "m_{H}80 m_{SUSY}2800" "m_{H}100 m_{SUSY}2800" \
-B \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_btagHbb_20230517_105735 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_btagHbb_20230517_105823 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2000toInf_btagHbb_20230517_105851 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_btagHbb_20230517_105437 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_btagHbb_20230517_105515 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_btagHbb_20230517_105553 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_btagHbb_20230517_110420 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/WJetsToQQ_HT-800toInf_20230504_091550 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/ZJetsToQQ_HT-800toInf_20230504_091535 \
-b QCD TTJets "W+Jets" "Z+Jets" \
-t btagHbb -O /opt/ppd/scratch/meili/NMSSM/plots/btagHbb --cutsHT 2000


# With data

# ParticleNet
python3 python/postprocessing/NMSSM/utilities/plotSignalBackgroundDataHistograms.py \
-S \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2800_20230504_091721 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_20230504_091701 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-100_mSUSY_2800_20230504_091652 \
-s "m_{H}60 m_{SUSY}2800" "m_{H}80 m_{SUSY}2800" "m_{H}100 m_{SUSY}2800" \
-B \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_particleNet_20230512_101437 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_particleNet_20230512_101444 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2500toInf_particleNet_20230512_101507 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_particleNet_20230512_102017 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_particleNet_20230512_102325 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_particleNet_20230512_102421 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_particleNet_20230512_102528 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/WJetsToQQ_HT-800toInf_20230504_091550 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/ZJetsToQQ_HT-800toInf_20230504_091535 \
-b QCD TTJets "W+Jets" "Z+Jets" \
-D \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n0_20230513_170006 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n1_20230512_105341 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n2_20230512_105342 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n3_20230512_105342 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n4_20230512_105343 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n5_20230512_105344 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_particleNet_n6_20230512_140009 \
-d "Data" \
-t particleNet -O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/plots/ --cutsHT 2000

# btagHbb
python3 python/postprocessing/NMSSM/utilities/plotSignalBackgroundDataHistograms.py \
-S \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-60_mSUSY_2800_20230504_091721 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_20230504_091701 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-100_mSUSY_2800_20230504_091652 \
-s "m_{H}60 m_{SUSY}2800" "m_{H}80 m_{SUSY}2800" "m_{H}100 m_{SUSY}2800" \
-B \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1000to1500_btagHbb_20230517_105735 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT1500to2000_btagHbb_20230517_105823 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/QCD_HT2000toInf_btagHbb_20230517_105851 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-600to800_btagHbb_20230517_105437 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-800to1200_btagHbb_20230517_105515 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-1200to2500_btagHbb_20230517_105553 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/TTJets_HT-2500toInf_btagHbb_20230517_110420 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/WJetsToQQ_HT-800toInf_20230504_091550 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/ZJetsToQQ_HT-800toInf_20230504_091535 \
-b QCD TTJets "W+Jets" "Z+Jets" \
-D \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n0_20230515_182318 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n1_20230515_182319 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n2_20230515_182319 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n3_20230515_182320 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n4_20230515_182320 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n5_20230515_182321 \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/Data_Run2_2018_JSON_btagHbb_n6_20230515_182321 \
-d "Data" \
-t btagHbb -O /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/plots/ --cutsHT 2000



###########################
# Plot Dataset Comparison #
###########################

# Compare different HT bins in QCD ParticleNet
python3 python/postprocessing/NMSSM/utilities/plotDatasetComparison.py \
-I /opt/ppd/scratch/meili/NMSSM/QCD_HT1000toInf_particleNet_2023* \
-O /opt/ppd/scratch/meili/NMSSM/plots/particleNet_oldGrid/ -t particleNet

# Compare particleNet and btagHbb in signal
python3 python/postprocessing/NMSSM/utilities/plotDatasetComparison.py \
-I /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_2023* /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_2023* \
-O /opt/ppd/scratch/meili/NMSSM/plots/particleNet_vs_btagHbb/ --compareTaggers



#################################
# Check which files are missing #
#################################

# for i in {4000..4999}; do
#     file_name="FromDataset_Run2018A_Skim_${i}_FakeFriend_btagHbb.root"
#     [ ! -f $file_name ] && echo "$file_name"
# done