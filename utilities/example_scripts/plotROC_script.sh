#!/bin/sh


########################################
#            Plot ROC curves           #
########################################

# Only QCD and TTbar

echo "Plotting particleNet ROC curves..."
python3 python/postprocessing/NMSSM/utilities/plotROC.py \
-tp /opt/ppd/scratch/meili/NMSSM/roc/NMSSMCascade_mH-80_mSUSY_2800_ROC_2023*/btag_efficiency_particleNet_2D.dat \
-fp \
/opt/ppd/scratch/meili/NMSSM/old_mass_grid/QCD_HT1000toInf_particleNet_20230317_165024/btag_efficiency_particleNet_2D.dat \
/opt/ppd/scratch/meili/NMSSM/old_mass_grid/TTJets_HT-600toInf_particleNet_20230317_165003/btag_efficiency_particleNet_2D.dat \
-l QCD TTJets -o roc_particleNet_2D_NMSSMCascade_mH-80_mSUSY_2800.pdf \
-O /opt/ppd/scratch/meili/NMSSM/plots/ROC

echo "Plotting btagHBB ROC curves..."
python3 python/postprocessing/NMSSM/utilities/plotROC.py \
-tp /opt/ppd/scratch/meili/NMSSM/roc/NMSSMCascade_mH-80_mSUSY_2800_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
-fp \
/opt/ppd/scratch/meili/NMSSM/roc/QCD_HT1000toInf_btagHbb_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
/opt/ppd/scratch/meili/NMSSM/roc/TTJets_HT-600toInf_btagHbb_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
-l QCD TTJets -o roc_btagHbb_2D_NMSSMCascade_mH-80_mSUSY_2800.pdf \
-O /opt/ppd/scratch/meili/NMSSM/plots/ROC

# Only QCD and TTbar after mass event selection

echo "Plotting particleNet ROC curves..."
python3 python/postprocessing/NMSSM/utilities/plotROC.py \
-tp /opt/ppd/scratch/meili/NMSSM/roc/NMSSMCascade_mH-80_mSUSY_2800_ROC_2023*/btag_efficiency_particleNet_2D.dat \
-fp \
/opt/ppd/scratch/meili/NMSSM/loose_cut_1p9/QCD_HT1000toInf_particleNet_ROC_20230319_113435/btag_efficiency_particleNet_2D.dat \
/opt/ppd/scratch/meili/NMSSM/loose_cut_1p9/TTJets_HT-600toInf_particleNet_ROC_20230319_113326/btag_efficiency_particleNet_2D.dat \
-l QCD TTJets -o roc_particleNet_2D_NMSSMCascade_mH-80_mSUSY_2800.pdf \
-O /opt/ppd/scratch/meili/NMSSM/plots/ROC/post_mass  --massSelectROC

echo "Plotting btagHBB ROC curves..."
python3 python/postprocessing/NMSSM/utilities/plotROC.py \
-tp /opt/ppd/scratch/meili/NMSSM/roc/NMSSMCascade_mH-80_mSUSY_2800_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
-fp \
/opt/ppd/scratch/meili/NMSSM/roc/QCD_HT1000toInf_btagHbb_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
/opt/ppd/scratch/meili/NMSSM/roc/TTJets_HT-600toInf_btagHbb_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
-l QCD TTJets -o roc_btagHbb_2D_NMSSMCascade_mH-80_mSUSY_2800.pdf \
-O /opt/ppd/scratch/meili/NMSSM/plots/ROC/post_mass  --massSelectROC

# All background

echo "Plotting particleNet ROC curves..."
python3 python/postprocessing/NMSSM/utilities/plotROC.py \
-tp /opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_2023*/btag_efficiency_particleNet_2D.dat \
-fp \
/opt/ppd/scratch/meili/NMSSM/loose_cut_1p9/QCD_HT1000toInf_particleNet_2023*/btag_efficiency_particleNet_2D.dat \
/opt/ppd/scratch/meili/NMSSM/loose_cut_1p9/TTJets_HT-600toInf_particleNet_2023*/btag_efficiency_particleNet_2D.dat \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/WJetsToQQ_HT-800toInf_2023*/btag_efficiency_particleNet_2D.dat \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/ZJetsToQQ_HT-800toInf_2023*/btag_efficiency_particleNet_2D.dat \
-l QCD TTJets W+Jets Z+Jets -o roc_particleNet_2D_NMSSMCascade_mH-80_mSUSY_2800_all-bkg.pdf \
-O /opt/ppd/scratch/meili/NMSSM/plots/ROC

echo "Plotting btagHBB ROC curves..."
python3 python/postprocessing/NMSSM/utilities/plotROC.py \
-tp /opt/ppd/scratch/meili/NMSSM/roc/tag_FakeFriends_v1/NMSSMCascade_mH-80_mSUSY_2800_ROC_2023*/btag_efficiency_btagHbb_2D.dat \
-fp \
/opt/ppd/scratch/meili/NMSSM/roc/QCD_HT1000toInf_btagHbb_2023*/btag_efficiency_btagHbb_2D.sdat \
/opt/ppd/scratch/meili/NMSSM/roc/TTJets_HT-600toInf_btagHbb_2023*/btag_efficiency_btagHbb_2D.dat \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/roc/WJetsToQQ_HT-800toInf_2023*/btag_efficiency_btagHbb_2D.dat \
/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/roc/ZJetsToQQ_HT-800toInf_2023*/btag_efficiency_btagHbb_2D.dat \
-l QCD TTJets "W+Jets" "Z+Jets" -o roc_btagHbb_2D_NMSSMCascade_mH-80_mSUSY_2800.pdf \
-O /opt/ppd/scratch/meili/NMSSM/plots/ROC

