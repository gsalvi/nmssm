#!/usr/bin/env python3

# TODO: this is currently broken, because it assumes the TriggerSelection and KinematicEventSelection is applied in the FakeFriends. The results will therefore not include the Trigger or Kinematic selections.

import argparse

# Import variables from plotUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.fileReadUtils import *
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotBackgroundEstimation import backgroundEstimation

# Argument parser
parser = argparse.ArgumentParser(description="Process root file(s) and plot Search Region Bin histograms.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-S", "--signalDirs", default=None, nargs="+", help="Specifies the directories with signal files.")
parser.add_argument("-s", "--signalNames", default=None, nargs="+", help="Specifies the names of the signal files for plot legends.")
parser.add_argument("-B", "--backgroundDirs", default=None, nargs="+", help="Specifies the directories with background files.")
parser.add_argument("-b", "--backgroundNames", default=None, nargs="+", help="Specifies the names of the background files for plot legends")
parser.add_argument("-D", "--dataDirs", default=None, nargs="+", help="Specifies the directory with data files.")
parser.add_argument("-d", "--dataName", default=None, type=str, help="Specifies the name of the data for plot legends")
parser.add_argument("-O", "--outputDir", default="./", help="The output directory, has to already exist. (default = %(default)s)")
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, either btagHbb or particleNet, or all for both. (default = %(default)s)")
parser.add_argument("-l", "--log", action="store_true", help="Plot with logarithmic y-axis.")
parser.add_argument("--cutsHT", default=[0], nargs="+", type=int, help="HT cuts for the plotting. If a single value has been defined, this is a minimum value. If multiple cuts have been defined, multiple plots will be created with HT ranges between the values specified. (default = %(default)s)")
args = parser.parse_args()

HIST_LINEWIDTH = 4

# Creates a stacked histogram of backgrounds, with the signal overlayed as lines, and data overlayed as points
def plotSignalBackgroundDataHistograms(branch, signal_dirs, signal_name, background_dirs, background_name, data_dirs, data_name, output_dir, output, tagger, ht_min=0, ht_max=None, ylog=False):

    # Set branch specific values
    if "massSumRegion" in branch:
        branch = "FatJets_massSumRegion_%s%s_nominal" % (tagger, "Retrained" if tagger == "particleNet" else "")
        bins = 10
        xmin = 0.5
        xmax = 10.5
        start = xmin
        stop = xmax
        xlabel = "Search Region Bin Number"
        xticks = range(1,11,1)
        minor_xticks = False
        figsize = (10, 12) if data_dirs else None
        event_filter = "FatJets_massDiffRegion_%s%s_nominal ==  3 && FatJets_dbtRegion_%s == 1" % (tagger, "Retrained" if tagger == "particleNet" else "", tagger)
        bkg_estimation = True if data_dirs else False # Use data driven background estimation
    elif "HT" in branch:
        bins = 100
        xmin = ht_min
        xmax = 4000
        start = xmin
        stop = xmax
        xlabel = "$H_{T}$ [GeV]"
        xticks = None # Default x-ticks
        minor_xticks = True
        figsize = None
        event_filter = None
        bkg_estimation = False # Use data driven background estimation
    else:
        raise ValueError("No predefined values for branch %s" % branch)

    # Create dictionary of histograms
    signal_dict = {}
    background_dict = {}

    # Create figure
    fig = plt.figure(figsize=figsize)
    if data_dirs:
        grid = fig.add_gridspec(2, 1, hspace=0.05, height_ratios=[4, 1])
    else:
        grid = fig.add_gridspec(1, 1)
    main_ax = fig.add_subplot(grid[0])

    # Create list of colour indices
    colour_ids = []

    # Process data
    if data_dirs is not None:

        # Get file names
        data_files = getFileList(data_dirs, tagger)

        # Read data file and create histogram
        data_hist = createHistogram(data_files, branch, event_filter, bins, start, stop, ht_min, ht_max, True, False)

    # Background sum histogram
    bkg_sum_list  = np.zeros(bins)
    bkg_variance_list  = np.zeros(bins)

    # Process backgrounds
    if background_dirs is not None:

        colour_id = 0 # Keeps track of colours for plotting

        # Create a list of all backgrounds except QCD for QCD data-driven background estimation
        if bkg_estimation:
            for bkg_dir in background_dirs:
                if "QCD" not in bkg_dir:
                    bkg_no_qcd += getFileList(bkg_dir, tagger)

        # Read all background files
        for bkg_dir, bkg_name in zip(background_dirs, background_name):

            # Get file names
            files = getFileList(bkg_dir, tagger)

            # Create and add histogram to dict
            if "QCD" in bkg_name and bkg_estimation:
                background_dict[bkg_name] = backgroundEstimation(data_files, bkg_no_qcd, tagger, 1, bins, start, stop, ht_min, ht_max)
            else:
                background_dict[bkg_name] = createHistogram(files, branch, event_filter, bins, start, stop, ht_min, ht_max)

            # Add colour index
            colour_ids.append(colour_id)
            colour_id += 1

            # Add histogram values to background sum histogram
            bkg_sum_list += background_dict[bkg_name].values()
            bkg_variance_list += background_dict[bkg_name].variances()

        # Stack the histograms
        background_stack = hist.Stack.from_dict(background_dict)[::-1]
        colour_ids = colour_ids[::-1]

    # Process signals
    if signal_dirs is not None:

        colour_id = NUM_COLORS-1 # Keeps track of colours for plotting

        # Read all signal files
        for sig_dir, sig_name in zip(signal_dirs, signal_name):

            # Get file names
            files = getFileList(sig_dir, tagger)

            # Create and add histogram to dict
            signal_dict[sig_name] = createHistogram(files, branch, event_filter, bins, start, stop, ht_min, ht_max)

            # Add colour index
            colour_ids.append(colour_id)
            colour_id -= 1

        # Stack the histograms
        signal_stack = hist.Stack.from_dict(signal_dict)

    # Set colours
    colour_list = [scalarMap.to_rgba(i) for i in colour_ids]
    colour_list[:len(background_dict)] = colour_list[:len(background_dict)][::-1]
    if data_dirs is not None:
        colour_list.append((0,0,0,1))
    main_ax.set_prop_cycle(color=colour_list)

    # Plot stacked histograms
    if background_dirs is not None:
        background_stack.plot(stack=True, histtype="fill", alpha=OPACITY, flow="none")
        errors = np.sqrt(bkg_variance_list) # I don't understand how the error bars are calculated for validation hist wtf
        main_ax.fill_between(np.linspace(xmin, xmax-1, bins), bkg_sum_list-errors, bkg_sum_list+errors, step='post', hatch='//', facecolor='none', alpha=0, zorder=2)
        main_ax.fill_between([xmax-1, xmax], bkg_sum_list[-2:]-errors[-2:], bkg_sum_list[-2:]+errors[-2:], step='pre', hatch='//', facecolor='none', alpha=0, zorder=2) # Stupid matplotlib doesn't include the last bin
    if signal_dirs is not None:
        signal_stack.plot(linewidth=HIST_LINEWIDTH, flow="none")
    if data_dirs is not None:
        data_hist.plot(histtype="errorbar", label=data_name, alpha=OPACITY, yerr=True, marker='o', flow="none")

    # Set axis limits
    plt.xlim(xmin=xmin, xmax=xmax)
    if ylog:
        main_ax.set_yscale('log')
        main_ax.set_ylim(ymin=None, ymax=main_ax.get_ylim()[1]*10) # Increase y-axis range to fit legend
    else:
        main_ax.set_ylim(ymin=0, ymax=main_ax.get_ylim()[1]*1.45) # Increase y-axis range to fit legend

    # Set labels
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    main_ax.set_xlabel(xlabel)
    main_ax.set_ylabel("Events")

    # Set HT bin label
    main_ax.text(0.95, 0.75, "$H_{T}$ %i%s GeV"%(ht_min, "-"+str(int(ht_max)) if ht_max is not None else "+"), fontsize=25, transform=main_ax.transAxes, horizontalalignment="right", verticalalignment="top")

    # Set legend
    handles, labels = main_ax.get_legend_handles_labels() # Hack to change the signal legend linewidth
    for i, handle in enumerate(handles):
        if i >= len(background_dict) and not (data_dirs is not None and i == len(handles)-1):
            handles[i] = matplotlib.lines.Line2D([], [], color=scalarMap.to_rgba(colour_ids[i]), linewidth=HIST_LINEWIDTH)
            labels[i] = " ".join(["$" + l + "$" for l in labels[i].split(" ")]) # Make "math mode"
    handles[:len(background_dict)] = handles[:len(background_dict)][::-1] # Flip order of background handles
    labels[:len(background_dict)] = labels[:len(background_dict)][::-1] # Flip order of background labels
    plt.legend(handles=handles, labels=labels, loc='upper right', ncol=2)

    # Plot ratio if data
    if data_dirs:

        subplot_ax = fig.add_subplot(grid[1], sharex=main_ax)

        # Plot horizontal line
        subplot_ax.axhline(y=1, xmin=0, xmax=bins+1, color="gray")

        # Plot ratio
        ratio_vals = np.divide(data_hist.values(), bkg_sum_list, out=np.zeros_like(data_hist.values()), where=bkg_sum_list!=0)
        ratio_err = np.sqrt(data_hist.variances())/bkg_sum_list
        subplot_ax.errorbar(np.linspace(1,bins,bins,endpoint=True), ratio_vals, alpha=OPACITY, yerr=ratio_err, linestyle='none', color="black", marker="o", markersize=10)

        # Plot background errors
        errors = np.divide(errors, bkg_sum_list, out=np.zeros_like(errors), where=bkg_sum_list!=0)
        subplot_ax.fill_between(np.linspace(xmin, xmax-1, bins), 1-errors, 1+errors, step='post', hatch='//', facecolor='w', alpha=0, zorder=2)
        subplot_ax.fill_between([xmax-1, xmax], 1-errors[-2:], 1+errors[-2:], step='pre', hatch='//', facecolor='w', alpha=0, zorder=2) # Stupid matplotlib doesn't include the last bin

        # Set axes
        subplot_ax.set_ylim(ymin=0, ymax=2)

        # Set tick labels
        plt.setp(main_ax.get_xticklabels(), visible=False)
        if xticks is not None:
            subplot_ax.set_xticks(xticks)
        if not minor_xticks:
            main_ax.tick_params(axis='x', which='minor', bottom=False, top=False)
            subplot_ax.tick_params(axis='x', which='minor', bottom=False, top=False)

        # Set labels 
        subplot_ax.set_xlabel(xlabel)
        subplot_ax.set_ylabel("Data/Bkg")
        main_ax.set_xlabel("")

    else:
        main_ax.set_xlabel(xlabel)

        # Set tick labels
        if xticks is not None:
            main_ax.set_xticks(xticks)
        if not minor_xticks:
            main_ax.minorticks_off()

    # Save figure
    plt.savefig(output_dir+"/"+output)
    plt.close()
    print("Plotted histogram: " + output_dir+"/"+output + "\n")


if __name__ == "__main__":

    # Check that output directory exists
    if not os.path.exists(args.outputDir):
        raise NotADirectoryError("Output directory %s is not a directory." % (args.outputDir))

    # Check HT cuts
    if any(i < -1 for i in args.cutsHT[1:-1]):
        raise ValueError("Upper HT cuts must be positive.")

    # Check that the taggers exist
    btagger_list = ["particleNet", "btagHbb"]
    if "all" in args.taggers:
        args.taggers = btagger_list

    # Sort background list
    args.backgroundDirs = datasetListSort(args.backgroundDirs)

    # Check that the numbers of signa/background names and directories are the same
    if len(args.signalDirs) != len(args.signalNames) or len(args.backgroundDirs) != len(args.backgroundNames):
        raise Exception("The number of signal/background names and directories must match.")

    # List of branches to plot
    branch_list = ["massSumRegion", "HT_nom"]

    for b in branch_list:
        print("Plotting branch: %s" % b)
        for t in args.taggers:
            if t not in btagger_list:
                raise Exception("The b-tagger(s) are not defined: %s" % t)
            else:
                # Run plotting for all HT bins
                for c in range(len(args.cutsHT)):

                    # Output name suffix
                    output = b + "_HT" + str(args.cutsHT[c]) + "to" + (str(args.cutsHT[c+1]) if c < len(args.cutsHT)-1 else "Inf") + "_" + t + ("_log" if args.log else "") + ".pdf"

                    # Plot branch
                    plotSignalBackgroundDataHistograms(b, args.signalDirs, args.signalNames, args.backgroundDirs, args.backgroundNames, args.dataDirs, args.dataName, args.outputDir, output, t, float(args.cutsHT[c]), float(args.cutsHT[c+1]) if c < len(args.cutsHT)-1 else None, args.log)
