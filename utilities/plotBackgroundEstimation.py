#!/usr/bin/env python3

# Import variables from plotUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.fileReadUtils import *


# Return list with number of background in specified region (TR = 1, VR = 2, CR = 3), for sideband or signal bin
def countBackgroundMC(background_list, tagger="particleNet", region=2, sideband=False, ht_min=0, ht_max=None):

    # Open input files
    rdf = ROOT.RDataFrame("Events", background_list)

    # Remove events that does not pass HT criteria
    if ht_max is None:
        rdf = rdf.Filter("HT >= %f" % (ht_min))
    else:
        rdf = rdf.Filter("HT >= %f && HT < %f" % (ht_min, ht_max))

    mc_background = []
    for i in range(10):
        if sideband:
            mc_background.append(np.sum(rdf.Filter("FatJets_massSumRegion_%s_nominal == %i+1 && (FatJets_massDiffRegion_%s_nominal ==  1 || FatJets_massDiffRegion_%s_nominal ==  5) && FatJets_dbtRegion_%s == %i" % (tagger, i, tagger, tagger, tagger, region)).AsNumpy(['weight_mc'])['weight_mc']))
        else:
            mc_background.append(np.sum(rdf.Filter("FatJets_massSumRegion_%s_nominal == %i+1 && FatJets_massDiffRegion_%s_nominal ==  3 && FatJets_dbtRegion_%s == %i" % (tagger, i, tagger, tagger, region)).AsNumpy(['weight_mc'])['weight_mc']))

    return np.array(mc_background)


# Calculate Fi
def factorCalculation(file_list, tagger, bins, data=False, ht_min=0, ht_max=None):

    # Open input files
    rdf = ROOT.RDataFrame("Events", file_list)

    # Remove events that does not pass HT criteria
    if ht_max is None:
        rdf = rdf.Filter("HT >= %f" % (ht_min))
    else:
        rdf = rdf.Filter("HT >= %f && HT < %f" % (ht_min, ht_max))

    # Filter uninteresting events
    rdf = rdf.Filter("FatJets_massSumRegion_%s_nominal > 0 && FatJets_massDiffRegion_%s_nominal > 0  && FatJets_dbtRegion_%s > 0" % (tagger, tagger, tagger))

    # Calculate Fi
    factor = []
    for i in range(bins):
        if data:
            s = rdf.Filter("FatJets_massSumRegion_%s_nominal == %i+1 && FatJets_massDiffRegion_%s_nominal ==  3 && FatJets_dbtRegion_%s == 3" % (tagger, i, tagger, tagger)).Count().GetValue()
            u = rdf.Filter("FatJets_massSumRegion_%s_nominal == %i+1 && (FatJets_massDiffRegion_%s_nominal ==  1 || FatJets_massDiffRegion_%s_nominal ==  5) && FatJets_dbtRegion_%s == 3" % (tagger, i, tagger, tagger, tagger)).Count().GetValue()
        else:
            s = np.sum(rdf.Filter("FatJets_massSumRegion_%s_nominal == %i+1 && FatJets_massDiffRegion_%s_nominal ==  3 && FatJets_dbtRegion_%s == 3" % (tagger, i, tagger, tagger)).AsNumpy(['weight_mc'])['weight_mc'])
            u = np.sum(rdf.Filter("FatJets_massSumRegion_%s_nominal == %i+1 && (FatJets_massDiffRegion_%s_nominal ==  1 || FatJets_massDiffRegion_%s_nominal ==  5) && FatJets_dbtRegion_%s == 3" % (tagger, i, tagger, tagger, tagger)).AsNumpy(['weight_mc'])['weight_mc'])
        factor.append(s/u)
        print(i, s, u, s/u)

    return factor

# Datadriven background estimation for QCD
def backgroundEstimation(file_list, background_list, tagger, region, bins, start, stop, ht_min, ht_max):

        # Branch names etc
        branch = "FatJets_massSumRegion_%s_nominal" % tagger

        # Get Fi
        factor = factorCalculation(file_list, tagger, bins, True if background_list else False, ht_min, ht_max)

        # Create background prediction histogram
        estimation_hist = createHistogram(file_list, branch, "(FatJets_massDiffRegion_%s_nominal ==  1 || FatJets_massDiffRegion_%s_nominal ==  5) && FatJets_dbtRegion_%s == %i" % (tagger, tagger, tagger, region), bins, start, stop, ht_min, ht_max, weighted=False if background_list else True)

        # Remove MC background
        estimation = np.array(estimation_hist.values())
        if background_list is not None:
            mc_background = countBackgroundMC(background_list, tagger, region, True, ht_min, ht_max)
            estimation = estimation - mc_background
            print("Number of events in sideband after MC background removed ", np.array(estimation_hist.values()))
            print("MC background in sidebands ", mc_background)

        # Calculate estimation S = F * (U + D)
        estimation = [[estimation[i]*factor[i], estimation_hist.variances()[i]*factor[i]] for i in range(bins)]
        estimation_hist[:] = np.array(estimation)

        return estimation_hist

def plotBackgroundEstimation(background_list, data_list=None, mc_background_list=None, tagger="particleNet", ylog=True, output_dir="./", ht_min=0, ht_max=None, plot_ratio=True, dbt_region=None):

    # Set values
    bins = 10
    xmin = 0.5
    xmax = 10.5
    start = xmin
    stop = xmax
    xlabel = "Search Region Bin Number"
    xticks = range(1,bins+1,1)

    # Read data file if it exists
    if data_list is not None:
        # Create background prediction histogram
        estimation_hist = backgroundEstimation(data_list, mc_background_list, tagger, 2, bins, start, stop, ht_min, ht_max)

        # Create validation histogram
        validation_hist = createHistogram(data_list, "FatJets_massSumRegion_%s_nominal" % tagger, "FatJets_massDiffRegion_%s_nominal ==  3 && FatJets_dbtRegion_%s == 2" % (tagger, tagger), bins, start, stop, ht_min, ht_max, weighted=False)

        # Count MC background
        background_mc = countBackgroundMC(mc_background_list, tagger, 2, False, ht_min, ht_max)
        validation_hist[:] = [[validation_hist.values()[i]-background_mc[i], validation_hist.variances()[i]] for i in range(bins)]

        print("Number of events in signal bins after MC background removed", validation_hist.values())
        print("MC background in signal bins ", background_mc)

        # Labels
        validation_label = "Data VR observed"
        background_label = "Data VR prediction"
        ratio_y_label = "Data/Pred"
        output = "background_estimation_data_%ito%s_%s_VR.pdf" % (ht_min, str(ht_max) if ht_max is not None else "Inf", tagger)
    elif dbt_region == 2: # Plot QCD VR
             # Create background prediction histogram
        estimation_hist = backgroundEstimation(background_list, None, tagger, 2, bins, start, stop, ht_min, ht_max)

        # Create validation histogram
        validation_hist = createHistogram(background_list, "FatJets_massSumRegion_%s_nominal" % tagger, "FatJets_massDiffRegion_%s_nominal ==  3 && FatJets_dbtRegion_%s == 2" % (tagger, tagger), bins, start, stop, ht_min, ht_max)

        # Labels
        validation_label = "QCD VR observed"
        background_label = "QCD VR prediction"
        ratio_y_label = "Sim/Pred"
        output = "background_estimation_%ito%s_%s_VR.pdf" % (ht_min, str(ht_max) if ht_max is not None else "Inf", tagger)   
    else: # Plot QCD TR
        # Create background prediction histogram
        estimation_hist = backgroundEstimation(background_list, None, tagger, 1, bins, start, stop, ht_min, ht_max)

        # Create validation histogram
        validation_hist = createHistogram(background_list, "FatJets_massSumRegion_%s_nominal" % tagger, "FatJets_massDiffRegion_%s_nominal ==  3 && FatJets_dbtRegion_%s == 1" % (tagger, tagger), bins, start, stop, ht_min, ht_max)

        # Labels
        validation_label = "QCD TR observed"
        background_label = "QCD TR prediction"
        ratio_y_label = "Sim/Pred"
        output = "background_estimation_%ito%s_%s_TR.pdf" % (ht_min, str(ht_max) if ht_max is not None else "Inf", tagger)

    # Create figure
    fig = plt.figure()
    if plot_ratio:
        grid = fig.add_gridspec(2, 1, hspace=0.05, height_ratios=[3, 1])
    else:
        grid = fig.add_gridspec(1, 1)
    main_ax = fig.add_subplot(grid[0])

    print("est hist variances ", estimation_hist.variances())
    print("val hist variances ", validation_hist.variances())

    # Plot estimation
    estimation_hist.plot(histtype="fill", label=background_label, color="plum", alpha=0.5, flow="none")
    errors = np.sqrt(estimation_hist.variances())
    main_ax.fill_between(np.linspace(xmin, xmax-1, bins), estimation_hist.values()-errors, estimation_hist.values()+errors, step='post', hatch='//', facecolor='w', alpha=0, zorder=2)
    main_ax.fill_between([xmax-1, xmax], estimation_hist.values()[-2:]-errors[-2:], estimation_hist.values()[-2:]+errors[-2:], step='pre', hatch='//', facecolor='w', alpha=0, zorder=2) # Stupid matplotlib doesn't include the last bin

    # Plot validation
    validation_hist.plot(histtype="errorbar", label=validation_label, yerr=True, marker='o', color="black", flow="none")

    # Set axis limits
    plt.xlim(xmin=xmin, xmax=xmax)
    if ylog:
        main_ax.set_yscale('log')
        main_ax.set_ylim(ymin=None, ymax=main_ax.get_ylim()[1]*3) # Increase y-axis range to fit legend
    else:
        main_ax.set_ylim(ymin=0, ymax=main_ax.get_ylim()[1]*1.25) # Increase y-axis range to fit legend

    # Set labels
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    main_ax.set_ylabel("Events")

    # Set HT bin label
    main_ax.text(0.03, 0.95, "$H_{T}$ %i%s GeV"%(ht_min, "-"+str(int(ht_max)) if ht_max is not None else "+"), fontsize=25, transform=main_ax.transAxes, horizontalalignment="left", verticalalignment="top")

    # Set legend
    plt.legend(loc='upper right')

    # Make it into a ratio plot
    if plot_ratio:

        subplot_ax = fig.add_subplot(grid[1], sharex=main_ax)

        # Plot horizontal line
        subplot_ax.axhline(y=1, xmin=0, xmax=bins+1, color="plum")

        # Plot ratio with validation hist errors
        ratio = np.divide(validation_hist.values(), estimation_hist.values())
        ratio_err = np.divide(np.sqrt(validation_hist.variances()), estimation_hist.values())
        subplot_ax.errorbar(range(1,bins+1), ratio, yerr=ratio_err, xerr=0, marker='o', markersize='10',color='black', linestyle='none')

        # Plot prediction errors
        errors = np.divide(errors, estimation_hist.values())
        subplot_ax.fill_between(np.linspace(xmin, xmax-1, bins), 1-errors, 1+errors, step='post', hatch='//', facecolor='w', alpha=0, zorder=2)
        subplot_ax.fill_between([xmax-1, xmax], 1-errors[-2:], 1+errors[-2:], step='pre', hatch='//', facecolor='w', alpha=0, zorder=2) # Stupid matplotlib doesn't include the last bin

        # Set axes
        subplot_ax.set_ylim(ymin=0, ymax=2)

        # Set tick labels
        plt.setp(main_ax.get_xticklabels(), visible=False)
        main_ax.tick_params(axis='x', which='minor', bottom=False, top=False)
        subplot_ax.tick_params(axis='x', which='minor', bottom=False, top=False)
        subplot_ax.set_xticks(xticks)

        # Set labels 
        subplot_ax.set_xlabel(xlabel)
        subplot_ax.set_ylabel(ratio_y_label)
        main_ax.set_xlabel("")

    else:
        main_ax.set_xlabel(xlabel)

        # Set tick labels
        main_ax.set_xticks(xticks)
        main_ax.minorticks_off()

    # Save figure
    plt.savefig(output_dir+"/"+output)
    plt.close()
    print("Plotted histogram: " + output_dir+"/"+output + "\n")



if __name__ == "__main__":

    # ParticleNet tight cut
    if True:
        # Specify directories
        base_directory = "/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/"
        qcd_dir = ["QCD_HT1000to1500_particleNet_20230512_101437", "QCD_HT1500to2000_particleNet_20230512_101444", "QCD_HT2500toInf_particleNet_20230512_101507"]
        data_dir = ["Data_Run2_2018_JSON_particleNet_n0_20230513_170006", "Data_Run2_2018_JSON_particleNet_n1_20230512_105341", "Data_Run2_2018_JSON_particleNet_n2_20230512_105342", "Data_Run2_2018_JSON_particleNet_n3_20230512_105342", "Data_Run2_2018_JSON_particleNet_n4_20230512_105343", "Data_Run2_2018_JSON_particleNet_n5_20230512_105344", "Data_Run2_2018_JSON_particleNet_n6_20230512_140009"]
        ttbar_dir = ["TTJets_HT-600to800_particleNet_20230512_102017", "TTJets_HT-800to1200_particleNet_20230512_102325", "TTJets_HT-1200to2500_particleNet_20230512_102421", "TTJets_HT-2500toInf_particleNet_20230512_102528"]
        wjets_dir = ["WJetsToQQ_HT-800toInf_20230504_091550"]
        zjets_dir = ["ZJetsToQQ_HT-800toInf_20230504_091535"]
        out_dir = "./"

        ht_min = 0
        ht_max = None

        # Specify tagger
        tagger = "particleNet"

        # Get list of file names
        qcd_files = getFileList([base_directory+d for d in qcd_dir], tagger)
        data_files = getFileList([base_directory+d for d in data_dir], tagger)
        ttbar_files = getFileList([base_directory+d for d in ttbar_dir], tagger)
        wjets_files = getFileList([base_directory+d for d in wjets_dir], tagger)
        zjets_files = getFileList([base_directory+d for d in zjets_dir], tagger)

        mc_background_files = ttbar_files + wjets_files + zjets_files

        # Plot
        plotBackgroundEstimation(qcd_files, None, None, tagger, True, out_dir, ht_min, ht_max) # QCD Tag Region
        plotBackgroundEstimation(qcd_files, None, None, tagger, True, out_dir, ht_min, ht_max, True, 2) # QCD Validation Region
        plotBackgroundEstimation(None, data_files, mc_background_files, tagger, True, out_dir, ht_min, ht_max) # Data Validation Region


    # ParticleNet loose cut
    # NOTE: need to remove "_nominal" from branch names
    if False:
        # Specify directories
        base_directory = "/opt/ppd/scratch/meili/NMSSM/old_mass_grid/"
        qcd_dir = ["QCD_HT1000toInf_particleNet_20230317_165024"]
        ttbar_dir = ["TTJets_HT-600toInf_particleNet_20230317_165003"]
        wjets_dir = ["WJetsToQQ_HT-800toInf_particleNet_20230317_164839"]
        zjets_dir = ["ZJetsToQQ_HT-800toInf_particleNet_20230317_164722"]
        data_dir = []
        out_dir = "./"

        # Specify tagger
        tagger = "particleNet"

        # Get list of file names
        qcd_files = getFileList([base_directory+d for d in qcd_dir], tagger)
        # data_files = getFileList([base_directory+d for d in data_dir], tagger)
        # ttbar_files = getFileList([base_directory+d for d in ttbar_dir], tagger)
        # wjets_files = getFileList([base_directory+d for d in wjets_dir], tagger)
        # zjets_files = getFileList([base_directory+d for d in zjets_dir], tagger)

        # Plot
        plotBackgroundEstimation(qcd_files, None, None, tagger, True, out_dir)
        # plotBackgroundEstimation(qcd_files, data_files, background_mc, tagger, True, out_dir)


    # btagHbb
    if False:
        # Specify directories
        base_directory = "/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/"
        qcd_dir = ["QCD_HT1000to1500_btagHbb_20230517_105735", "QCD_HT1500to2000_btagHbb_20230517_105823", "QCD_HT2000toInf_btagHbb_20230517_105851"]
        data_dir = ["Data_Run2_2018_JSON_btagHbb_n0_20230515_182318", "Data_Run2_2018_JSON_btagHbb_n1_20230515_182319", "Data_Run2_2018_JSON_btagHbb_n2_20230515_182319", "Data_Run2_2018_JSON_btagHbb_n3_20230515_182320", "Data_Run2_2018_JSON_btagHbb_n4_20230515_182320", "Data_Run2_2018_JSON_btagHbb_n5_20230515_182321", "Data_Run2_2018_JSON_btagHbb_n6_20230515_182321"]
        ttbar_dir = ["TTJets_HT-600to800_btagHbb_20230517_105437", "TTJets_HT-800to1200_btagHbb_20230517_105515", "TTJets_HT-1200to2500_btagHbb_20230517_105553", "TTJets_HT-2500toInf_btagHbb_20230517_110420"]
        wjets_dir = ["WJetsToQQ_HT-800toInf_20230504_091550"]
        zjets_dir = ["ZJetsToQQ_HT-800toInf_20230504_091535"]
        out_dir = "./"

        ht_min = 0
        ht_max = None

        # Specify tagger
        tagger = "btagHbb"

        # Get list of file names
        qcd_files = getFileList([base_directory+d for d in qcd_dir], tagger)
        data_files = getFileList([base_directory+d for d in data_dir], tagger)
        ttbar_files = getFileList([base_directory+d for d in ttbar_dir], tagger)
        wjets_files = getFileList([base_directory+d for d in wjets_dir], tagger)
        zjets_files = getFileList([base_directory+d for d in zjets_dir], tagger)

        mc_background_files = ttbar_files + wjets_files + zjets_files

        # Plot
        plotBackgroundEstimation(qcd_files, None, None, tagger, True, out_dir, ht_min, ht_max)
        plotBackgroundEstimation(None, data_files, mc_background_files, tagger, True, out_dir, ht_min, ht_max)
