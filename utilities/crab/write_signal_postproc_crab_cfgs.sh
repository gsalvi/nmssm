 #!/bin/bash

datasets=("NMSSMCascade_mH-100_mSUSY_2800" "NMSSMCascade_mH-125_mSUSY_2800" "NMSSMCascade_mH-155_mSUSY_2800" "NMSSMCascade_mH-200_mSUSY_2800" "NMSSMCascade_mH-30_mSUSY_2800" "NMSSMCascade_mH-40_mSUSY_1200" "NMSSMCascade_mH-40_mSUSY_1600" "NMSSMCascade_mH-40_mSUSY_2800" "NMSSMCascade_mH-60_mSUSY_1200" "NMSSMCascade_mH-60_mSUSY_1600" "NMSSMCascade_mH-60_mSUSY_2000" "NMSSMCascade_mH-60_mSUSY_2400" "NMSSMCascade_mH-60_mSUSY_2800" "NMSSMCascade_mH-60_mSUSY_3200" "NMSSMCascade_mH-60_mSUSY_800" "NMSSMCascade_mH-60_mSquark_2800" "NMSSMCascade_mH-80_mSUSY_2800")
POST="_TuneCP5_13TeV-madgraph-pythia8/linacre-UL2018_NANO_v3-00000000000000000000000000000000/USER"

#######################

for NAME in ${datasets[@]}; do
    DATASET="/"${NAME}${POST}
    mkdir -p "crab_jobs_postprocessing/"
    sed -e "s/%NAME%/${NAME}/g;s#%DATASET%#${DATASET}#g;" crab_cfg_signal_template.py > "crab_jobs_postprocessing/crab_cfg_${NAME}.py"
done

# Note, crab submission only works if the crab cfg is at 'python/postprocessing/NMSSM/utilities/crab/crab_cfg.py', so the generated configs must be copied before submission, e.g.:
# for f in `ls python/postprocessing/NMSSM/utilities/crab/crab_jobs_postprocessing/crab_cfg_NMSSMCascade*.py`; do cp $f python/postprocessing/NMSSM/utilities/crab/crab_cfg.py; crab submit -c python/postprocessing/NMSSM/utilities/crab/crab_cfg.py; done
