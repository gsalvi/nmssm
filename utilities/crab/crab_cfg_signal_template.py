#!/usr/bin/env python3
from WMCore.Configuration import Configuration
from CRABClient.UserUtilities import config, getUsername

# Crab Configuration File
# Documentation: https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile

# Change the following for each job

input_dataset = '%DATASET%'
input_dbs = 'phys03' # Where the data can be found: 'global', 'phys01', 'phys02', or 'phys03'
crab_script_args = ['-d=False','-y=UL2018'] # Can also add --jsonInput/-j and --isData/-d. NOTE: Can't use store_action flags, as you have to use the equality (=) sign?! Also, crab doesn't like double dashes or spaces in script arguments... see pyCfgParams because the scripsArgs documentation is ass in https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile#CRAB_configuration_parameters

output_tag = 'NMSSMPostProc_v3'

request_name = None # Can figure out the request name automatically, if TuneCP5...

if request_name is None and "_TuneCP5_" in input_dataset:
    request_name = input_dataset.split("_TuneCP5_")[0].strip("/")
elif request_name is None and "_MiniAOD" in input_dataset:
    request_name = input_dataset.split("_MiniAOD")[0].split("/")[-1]
elif request_name is None:
    raise ValueError("Can't determine request_name, please specify manually.")


# Output root file name, used in PSet.py, don't use the one in crabPostProc.py for some reason or it won't run
if "-d=True" in crab_script_args:
    output_name = "FromDataset_" + input_dataset.split("/")[2] + "_Skim.root"
else:
    output_name = "FromDataset_" + input_dataset.split("/")[1] + "_Skim.root"


# Set configuration

config = Configuration()

config.section_("General")
config.General.requestName = request_name
config.General.transferLogs = True

config.section_("JobType")
config.JobType.pluginName = 'Analysis'
config.JobType.psetName = 'python/postprocessing/NMSSM/utilities/crab/PSet.py'
config.JobType.scriptExe = 'python/postprocessing/NMSSM/utilities/crab/crab_script.sh'
config.JobType.scriptArgs = crab_script_args # Arguments for runnings the scriptExe
config.JobType.inputFiles = ['python/postprocessing/NMSSM/utilities/crab/crabPostProc.py', 'scripts/haddnano.py'] # scripts/haddnano.py merges the output trees?
config.JobType.sendPythonFolder = True

config.section_("Data")
config.Data.inputDataset = input_dataset
config.Data.inputDBS = input_dbs
config.Data.splitting = 'FileBased'
config.Data.unitsPerJob = 20 # How many files per job if "FileBased"
# config.Data.totalUnits = 2 # How many files to process if "FileBased"
config.Data.outLFNDirBase = '/store/user/%s/NMSSM' % (getUsername())
config.Data.publication = False
config.Data.outputDatasetTag = output_tag

config.section_("Site")
config.Site.whitelist = ["T2_UK_SGrid_RALPP"]
config.Site.storageSite = "T2_UK_SGrid_RALPP"
