#!/usr/bin/env python3
import argparse
import ROOT

from importlib import import_module
from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jetmetHelperRun2 import createJMECorrector
# from PhysicsTools.NanoAODTools.postprocessing.modules.jme.jecUncertainties import *
from PhysicsTools.NanoAODTools.postprocessing.modules.common.puWeightProducer import puWeight_2016,puWeight_2017,puWeight_2018,puWeight_UL2016,puWeight_UL2017,puWeight_UL2018
from PhysicsTools.NanoAODTools.postprocessing.modules.btv.btagSFProducer import btagSF2016,btagSF2017
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.crossSectionScales import CrossSectionScales

# This takes care of converting the input files from CRAB
from PhysicsTools.NanoAODTools.postprocessing.framework.crabhelper import inputFiles, runsAndLumis

ROOT.PyConfig.IgnoreCommandLineOptions = True


# Default input file to run over
input_file = ["python/postprocessing/NMSSM/test/test_sample.root"] # 155 events

# Argument parser
parser = argparse.ArgumentParser(description="Process some NanoAOD file(s) and adds jet related stuff to an output root file.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-y", "--year", type=str, help="The year of the input files, e.g. UL2018.", required=True)
parser.add_argument("-j", "--jsonInput", default=None, help="Select events using this JSON file. Note that in crab pwd is /srv. (default = %(default)s)")
parser.add_argument("-d", "--isData", default="False", type=str, help="If input are data, True or False. If not specified, then MC events are assumed.") # Can't use store_true in crab...
args = parser.parse_args()

# Set isData
if args.isData == "True":
    isData = True
elif args.isData == "False":
    isData = False
else:
    raise ValueError("-d/--isData can only be True or False")

# Set output root file name
input_name = inputFiles()[0].split("/")
if isData:
    if "Run2018A" in inputFiles()[0]:
        output_name = "FromDataset_JetHT_Run2018A_Skim.root" if "linacre" in inputFiles()[0] else "FromDataset_JetHT_Run2018A-" + input_name[-3] + "_Skim.root"
        runPeriod = "A"
    elif "Run2018B" in inputFiles()[0]:
        output_name = "FromDataset_JetHT_Run2018B_Skim.root" if "linacre" in inputFiles()[0] else "FromDataset_JetHT_Run2018B-" + input_name[-3] + "_Skim.root"
        runPeriod = "B"
    elif "Run2018C" in inputFiles()[0]:
        output_name = "FromDataset_JetHT_Run2018C_Skim.root" if "linacre" in inputFiles()[0] else "FromDataset_JetHT_Run2018C-" + input_name[-3] + "_Skim.root"
        runPeriod = "C"
    elif "Run2018D" in inputFiles()[0]:
        output_name = "FromDataset_JetHT_Run2018D_Skim.root" if "linacre" in inputFiles()[0] else "FromDataset_JetHT_Run2018D-" + input_name[-3] + "_Skim.root"
        runPeriod = "D"
else:
    output_name = "FromDataset_" + input_name[-5] + "_Skim.root"
    runPeriod = None

# Golden JSON file: https://twiki.cern.ch/twiki/bin/view/CMS/PdmVLegacy2018Analysis#Data_Certification
if isData and args.jsonInput:
    json = args.jsonInput
elif isData:
    json = "CMSSW_12_3_0_pre2/python/PhysicsTools/NanoAODTools/postprocessing/NMSSM/utilities/Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt"
    print("Using default golden json: ", json)
else:
    json = None

# Create a chain of modules to run for the analysis
postproc_chain = []

# Check if data
if "store/data/" in inputFiles()[0]:
    isData = True

# Add jet uncertainties modules. Adds JES related branches to the output root file.
# Bodge as UL2017 is missing data files for JER... or should I use UL2016/UL2018 instead?
if args.year == "UL2017":
    print("Warning: Year not defined for JME corrector. Run with default year 2017.")
    tmp_year = "2017"
else:
    tmp_year = args.year

jme_corrections_ak4 = createJMECorrector(isMC=(not isData), dataYear=tmp_year, runPeriod=runPeriod, jesUncert="Total", jetType="AK4PFPuppi")
postproc_chain.append(jme_corrections_ak4())

jme_corrections_ak8 = createJMECorrector(isMC=(not isData), dataYear=tmp_year, runPeriod=runPeriod, jesUncert="Total", jetType="AK8PFPuppi")
postproc_chain.append(jme_corrections_ak8())

# Add JEC uncertainties module. Do we need this one? It's crashing
# jec_uncert = jecUncertAll_cppOut
# postproc_chain.append(jec_uncert())

# Only do these steps for MC samples
if not isData:
    # Add cross-section scaling
    xsec_scale = CrossSectionScales()
    postproc_chain.append(xsec_scale)

    # Add pileup weight module
    if args.year == "2016":
        pu_weights = puWeight_2016
    elif args.year == "2017":
        pu_weights = puWeight_2017
    elif args.year == "2018":
        pu_weights = puWeight_2018
    elif args.year == "UL2016":
        pu_weights = puWeight_UL2016
    elif args.year == "UL2017":
        pu_weights = puWeight_UL2017
    elif args.year == "UL2018":
        pu_weights = puWeight_UL2018
    else:
        print("Warning: Year not defined for pileup weight module. Run with default year 2017.")
        pu_weights = puWeight_2017
    postproc_chain.append(pu_weights())

    # Add b-tag scale factor module. Not all years, UL, and b-tag discriminators supported.
    if "2016" in args.year:
        btag_sf = btagSF2016
    elif "2017" in args.year:
        btag_sf = btagSF2017
    else:
        print("Warning: Year not defined for b-tag scale factor module. Run with default year 2017.")
        btag_sf = btagSF2017
    postproc_chain.append(btag_sf())

    # Something for the PDF? Need to write that oneself...

# Preselection cuts
preselection = None
# preselection = "FatJet_pt[0] > 170 && FatJet_pt[1] > 170 && abs(FatJet_eta[0]) < 2.4 && abs(FatJet_eta[1]) < 2.4" # Jets seem to be ordered from high to low pt
# Does this pre-selection actually work... what if the first jet is outside our eta range, but the third fattest jet would pass the cuts


# Create post processor and run it
p = PostProcessor(".",
                  inputFiles(),
                  cut=preselection,
                  modules=postproc_chain,
                  provenance=True,
                  fwkJobReport=True, # Needed if running with crab
                  haddFileName=output_name, # Needs to match output file name in PSet.py
                  jsonInput=json
                  # jsonInput=runsAndLumis() # The example used this one, but they set lumi range in PSet.py?
                  )
p.run()

