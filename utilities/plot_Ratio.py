#!/usr/bin/env python3
import os
import argparse
import matplotlib
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors
import mplhep as hep
import hist
from hist import Hist
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True

# Import variables from plotUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *

# Argument parser

# Set more colours
NUM_COLORS = 8
cm = plt.get_cmap("jet")
cNorm  = colors.Normalize(vmin=0, vmax=NUM_COLORS-1)
scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)

HIST_LINEWIDTH = 4
OPACITY = 0.5

matplotlib.rcParams['xtick.major.size'] = 10
matplotlib.rcParams['xtick.minor.size'] = 0
matplotlib.rcParams['xtick.minor.width'] = 0


# Reads in ROOT files in the specified directory, and returns a histogram of the Search Region Bins
def createHistogram(directory, branch_name, bin, start, stop):

   # Get all root files in directory
    events_files = [directory+"/"+f for f in os.listdir(directory) if (os.path.isfile(directory+"/"+f) and "_Skim.root" in f)]
    friends_files = [directory+"/"+f for f in os.listdir(directory) if (os.path.isfile(directory+"/"+f) and "_Friend_btagHbb.root" in f)] # Friend trees
 
    # Create chains with all root files
    chain = ROOT.TChain("Events")
    friend_chain = ROOT.TChain("Friends")

    # Add root files to chains
    for events, friends in zip(events_files, friends_files):
        chain.Add(events)
        friend_chain.Add(friends)

    chain.AddFriend(friend_chain)

    # Make dataframe
    rdf = ROOT.RDataFrame(chain) # Make dataframe. DON'T MAKE IT IN A SEPERATE FUNCTION AND RETURN OR IT WILL SEG FAULT!

    # Create histogram
    histogram = Hist(
        hist.axis.Regular(bins=bin, start=start, stop=stop, name=branch_name)
        )

    # Get specified branch
    branch = rdf.AsNumpy([branch_name])[branch_name] # Get column and turn it into a numpy array (AsNumpy returns a dict)

    # If branch is an array of an array, maybe change it to just: if "FatJet" in branch_name
    if np.shape(branch[0]):
        # print('true')
        
        hist_values = [] # List of values to be inserted in histogram

        fatjet_index_branch = rdf.AsNumpy(["FatJetIndexA_btagHbb"])["FatJetIndexA_btagHbb"] # The index for the FatJet with the highest btag discriminator (and passes some cuts...)

        print(f'Total number of events is {len(branch)}')
        for i, event in enumerate(branch):
            if len(event)==0:
                print(f"skipping event # {i} since it doesn't contain any fatjets")
                continue

            x = event[fatjet_index_branch[i].item()]
            hist_values.append(x)

    else:
        # print('false')
        hist_values = branch

    # Fill histogram
    histogram.fill(hist_values)

    return histogram

Joe_directory='/opt/ppd/data/cms/gsalvi/Joe_new_20221003_101806'
NMSSM_directory='/opt/ppd/data/cms/gsalvi/NMSSM_last_test_20221006_145828'

# -----------------
print('Making HT hists...')
Joe_histogram = createHistogram(Joe_directory,"HT_nom",100,0,10000)
NMSSM_histogram = createHistogram(NMSSM_directory,"HT_nom",100,0,10000)
plotRatio(Joe_histogram, NMSSM_histogram, ylog=True, ylabel="Num Events", num_label='Joe', denom_label='NMSSM', ratio_ylim=[0.5,1.5], output_file="hist_ratio_HT.png")

print('Making GenHT hists...')
Joe_histogram = createHistogram(Joe_directory,"GenHT",100,0,10000)
NMSSM_histogram = createHistogram(NMSSM_directory,"GenHT",100,0,10000)
plotRatio(Joe_histogram, NMSSM_histogram, ylog=True, ylabel="Num Events", num_label='Joe', denom_label='NMSSM', ratio_ylim=[0.5,1.5], output_file="hist_ratio_GenHT.png")

print('Making fj_pt hists...')
Joe_histogram = createHistogram(Joe_directory,"FatJet_pt",100,0,4000)
NMSSM_histogram = createHistogram(NMSSM_directory,"FatJet_pt",100,0,4000)
plotRatio(Joe_histogram, NMSSM_histogram, ylog=True, ylabel="Num Events", num_label='Joe', denom_label='NMSSM', ratio_ylim=[0.5,1.5], output_file="hist_ratio_fjpt.png")

print('Making fj_msd hists...')
Joe_histogram = createHistogram(Joe_directory,"FatJet_msoftdrop",100,0,1200)
NMSSM_histogram = createHistogram(NMSSM_directory,"FatJet_msoftdrop",100,0,1200)
plotRatio(Joe_histogram, NMSSM_histogram, ylog=True, ylabel="Num Events", num_label='Joe', denom_label='NMSSM', ratio_ylim=[0.5,1.5], output_file="hist_ratio_fjmsd.png")

print('Making fj_eta hists...')
Joe_histogram = createHistogram(Joe_directory,"FatJet_eta",100,-2.5,2.5)
NMSSM_histogram = createHistogram(NMSSM_directory,"FatJet_eta",100,-2.5,2.5)
plotRatio(Joe_histogram, NMSSM_histogram, ylog=True, ylabel="Num Events", num_label='Joe', denom_label='NMSSM', ratio_ylim=[0.5,1.5], output_file="hist_ratio_fjeta.png")



