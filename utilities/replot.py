#!/usr/bin/env python3

# Import variables from plotUtils and fileReadUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.fileReadUtils import *
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.massEventSelection import MassGrid


#####################################################
# Re-plot histograms

# FIXME: currently just hardcoded...

mass_grid = MassGrid(False) # Old mass grid
input_directory = "/opt/ppd/scratch/meili/NMSSM/tag_FakeFriends_v1/"
output_directory = "./"


# PLOT BACKGROUND 2D DISTRIBUTION
if False:
    # Set tagger
    tagger = "particleNet"
    mass_text = "ParticleNet" if tagger == "particleNet" else "Softdrop"

    # Specify colourbar limits
    cbar_lim = [None, None] # [min, max]

    bkg_directories = ["QCD_HT1000to1500_particleNet_20230512_101437", "QCD_HT1500to2000_particleNet_20230512_101444", "QCD_HT2500toInf_particleNet_20230512_101507"]
    # bkg_directories = ["TTJets_HT-600to800_particleNet_20230512_102017", "TTJets_HT-800to1200_particleNet_20230512_102325", "TTJets_HT-1200to2500_particleNet_20230512_102421", "TTJets_HT-2500toInf_particleNet_20230512_102528"]
    input_files = getFileList([input_directory+d for d in bkg_directories], tagger)

    # Plot mass histogram
    figure_name = "mass_bkg"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_%s_massRetrained" % tagger, bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger=tagger, ht_min=2000, ht_max=None, xlabel="FatJetA %s mass [GeV]" % mass_text, ylabel="FatJetB %s mass [GeV]" % mass_text, weighted=True)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, cbar_lim=cbar_lim, output_file=output_directory+"massA_vs_massB_%s.png" % tagger)
    
    # Plot TR 2D mass histogram
    figure_name = "mass_bkg_tr"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_%s_massRetrained" % tagger, event_filter = "FatJets_dbtRegion_particleNet == 1", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger=tagger, ht_min=2000, ht_max=None, xlabel="FatJetA %s mass [GeV]" % mass_text, ylabel="FatJetB %s mass [GeV]" % mass_text, weighted=True)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, cbar_lim=cbar_lim, output_file=output_directory+"massA_vs_massB_TR_%s.png" % tagger)

    # Plot VR 2D mass histogram
    figure_name = "mass_bkg_vr"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_%s_massRetrained" % tagger, event_filter = "FatJets_dbtRegion_particleNet == 2", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger=tagger, ht_min=2000, ht_max=None, xlabel="FatJetA %s mass [GeV]" % mass_text, ylabel="FatJetB %s mass [GeV]" % mass_text, weighted=True)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, cbar_lim=cbar_lim, output_file=output_directory+"massA_vs_massB_VR_%s.png" % tagger)

    # Plot TR 2D mass histogram
    figure_name = "mass_bkg_cr"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_%s_massRetrained" % tagger, event_filter = "FatJets_dbtRegion_particleNet == 3", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger=tagger, ht_min=2000, ht_max=None, xlabel="FatJetA %s mass [GeV]" % mass_text, ylabel="FatJetB %s mass [GeV]" % mass_text, weighted=True)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, cbar_lim=cbar_lim, output_file=output_directory+"massA_vs_massB_CR_%s.png" % tagger)

# PLOT DATA 2D DISTRIBUTOIN PARTICLE NET
if True:
    data_dir = ["Data_Run2_2018_JSON_particleNet_n0_20230513_170006", "Data_Run2_2018_JSON_particleNet_n1_20230512_105341", "Data_Run2_2018_JSON_particleNet_n2_20230512_105342", "Data_Run2_2018_JSON_particleNet_n3_20230512_105342", "Data_Run2_2018_JSON_particleNet_n4_20230512_105343", "Data_Run2_2018_JSON_particleNet_n5_20230512_105344", "Data_Run2_2018_JSON_particleNet_n6_20230512_140009"]
    input_files = getFileList([input_directory+d for d in data_dir], "particleNet")

    # Plot TR 2D mass histogram
    figure_name = "mass_tr"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_particleNet_massRetrained", event_filter = "FatJets_dbtRegion_particleNet == 1", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="particleNet", ht_min=0, ht_max=None, xlabel="FatJetA ParticleNet mass [GeV]", ylabel="FatJetB ParticleNet mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_TR_particleNet_data.png")

    # Plot CR 2D mass histogram
    figure_name = "mass_cr"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_particleNet_massRetrained", event_filter = "FatJets_dbtRegion_particleNet == 3", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="particleNet", ht_min=0, ht_max=None, xlabel="FatJetA ParticleNet mass [GeV]", ylabel="FatJetB ParticleNet mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_CR_particleNet_data.png")

    # Plot VR 2D mass histogram
    figure_name = "mass_vr"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_particleNet_massRetrained", event_filter = "FatJets_dbtRegion_particleNet == 2", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="particleNet", ht_min=0, ht_max=None, xlabel="FatJetA ParticleNet mass [GeV]", ylabel="FatJetB ParticleNet mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_VR_particleNet_data.png")

    # Plot CR 2D mass histogram
    figure_name = "mass_nocut"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_particleNet_massRetrained", event_filter = None, bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="particleNet", ht_min=0, ht_max=None, xlabel="FatJetA ParticleNet mass [GeV]", ylabel="FatJetB ParticleNet mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_particleNet_data.png")


# PLOT DATA 2D DISTRIBUTOIN BTAGHBB
if False:
    data_dir = ["Data_Run2_2018_JSON_btagHbb_n0_20230515_182318", "Data_Run2_2018_JSON_btagHbb_n1_20230515_182319", "Data_Run2_2018_JSON_btagHbb_n2_20230515_182319", "Data_Run2_2018_JSON_btagHbb_n3_20230515_182320", "Data_Run2_2018_JSON_btagHbb_n4_20230515_182320", "Data_Run2_2018_JSON_btagHbb_n5_20230515_182321", "Data_Run2_2018_JSON_btagHbb_n6_20230515_182321"]
    input_files = getFileList([input_directory+d for d in data_dir], "btagHbb")

    # Plot TR 2D mass histogram
    figure_name = "mass_trbtag"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_msoftdrop", event_filter = "FatJets_dbtRegion_btagHbb == 1", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="btagHbb", ht_min=0, ht_max=None, xlabel="FatJetA btagHbb mass [GeV]", ylabel="FatJetB btagHbb mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_TR_btagHbb_data.png")

    # Plot CR 2D mass histogram
    figure_name = "mass_crbtag"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_msoftdrop", event_filter = "FatJets_dbtRegion_btagHbb == 3", bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="btagHbb", ht_min=0, ht_max=None, xlabel="FatJetA btagHbb mass [GeV]", ylabel="FatJetB btagHbb mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_CR_btagHbb_data.png")

    # Plot 2D mass histogram with no b-tag cut
    figure_name = "mass_nocutbtag"
    fig, ax = plt.subplots(num=figure_name)
    mass_grid.plotRegions(figure_name)
    histogram = create2DHistogram(input_files, branch="FatJet_msoftdrop", event_filter = None, bins=mass_grid.hmax//2, start=0, stop=mass_grid.hmax, tagger="btagHbb", ht_min=0, ht_max=None, xlabel="FatJetA btagHbb mass [GeV]", ylabel="FatJetB btagHbb mass [GeV]", weighted=False)
    plot2DHist(histogram, figure_name=figure_name, cbar_log=True, output_file=output_directory+"massA_vs_massB_btagHbb_data.png")


####################
# Compare JMS/JMR

# FIXME: currently just hardcoded...

if False:

    signal_directory = "NMSSMCascade_mH-80_mSUSY_2800_20230504_091701"

    input_files = getFileList(input_directory+signal_directory)

    bins = 30
    start = 60
    stop = 100
    xlabel = "FatJetA ParticleNet mass [GeV]"

    histogram_nominal = createHistogram(input_files, branch="FatJet_particleNet_massRetrained_corr", bins=bins, start=start, stop=stop, ht_min=0, ht_max=None, centre_bin=True, weighted=True, xlabel=xlabel, fatjet_only=True)
    histogram_jmr_up = createHistogram(input_files, branch="FatJet_particleNet_massRetrained_corr_JMR_up", bins=bins, start=start, stop=stop, ht_min=0, ht_max=None, centre_bin=True, weighted=True, xlabel=xlabel, fatjet_only=True)
    histogram_jmr_down = createHistogram(input_files, branch="FatJet_particleNet_massRetrained_corr_JMR_down", bins=bins, start=start, stop=stop, ht_min=0, ht_max=None, centre_bin=True, weighted=True, xlabel=xlabel, fatjet_only=True)
    histogram_jms_up = createHistogram(input_files, branch="FatJet_particleNet_massRetrained_corr_JMS_up", bins=bins, start=start, stop=stop, ht_min=0, ht_max=None, centre_bin=True, weighted=True, xlabel=xlabel, fatjet_only=True)
    histogram_jms_down = createHistogram(input_files, branch="FatJet_particleNet_massRetrained_corr_JMS_down", bins=bins, start=start, stop=stop, ht_min=0, ht_max=None, centre_bin=True, weighted=True, xlabel=xlabel, fatjet_only=True)

    plot1DHist([histogram_nominal, histogram_jmr_up, histogram_jmr_down, histogram_jms_up, histogram_jms_down], ylog=False, ylabel="Events", legend_labels=["Nominal", "JMR up", "JMR down", "JMS up", "JMS down"], output_file="mass_systematics.pdf")

    plot1DHist([histogram_nominal, histogram_jmr_up, histogram_jmr_down], ylog=True, ylabel="Events", legend_labels=["Nominal", "JMR up", "JMR down"], output_file="mass_jmr.pdf")
    plot1DHist([histogram_nominal, histogram_jms_up, histogram_jms_down], ylog=True, ylabel="Events", legend_labels=["Nominal", "JMS up", "JMS down"], output_file="mass_jms.pdf")
