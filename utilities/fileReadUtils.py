#!/usr/bin/env python3
import os
import hist
from hist import Hist
import pandas as pd
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True


# Get all root files in a directory (or list of directories) as a list
def getFileList(directories, tagger="particleNet"):

    # Check if single or multiple directories
    if not isinstance(directories, list):
        directories = [directories]

    files = []
    for d in directories:
        files += [d+"/"+f for f in os.listdir(d) if (os.path.isfile(d+"/"+f) and "_FakeFriend_%s.root"%tagger in f)]

        # Check that files were found
        if not files:
            raise Exception("No matchin files for b-tagger %s was found in directory %s." % (tagger, d))
        print(d)

    return files

# Returns a dictionary of file names in a single directory sorted by dataset
def getFileDict(directory, tagger="particleNet"):

    files = getFileList(directory, tagger)

    # Create dictionary
    file_dict = {}

    # Sort files
    for f in files:
 
        dataset = f.split("_Skim")[0].split("FromDataset_")[-1] # The dataset the file comes from
        if "TuneCP5" in dataset:
            dataset = dataset.split("_TuneCP5")[0]

        # Add to dictionary
        if dataset in file_dict:
            file_dict[dataset].append(f)
        else:
            file_dict[dataset] = [f]

    return file_dict

# Take a list of background directories, and sort them depending on which background e.g. [[QCDHT0to1000, QCDHT1000toInf], [TTbar]]
def datasetListSort(dataset_list):
    dataset_dict = {}
    for l in dataset_list:
        dataset_type = l.split("/")[-1].split("_")[0]
        if dataset_type not in dataset_dict:
            dataset_dict[dataset_type] = [l]
        else:
            dataset_dict[dataset_type].append(l)
    return list(dataset_dict.values())

# Reads in ROOT files in the specified directory, and returns a histogram of the specified branch
# Use fatjet_only if only the values from the chosen FatJetA should be used
def createHistogram(files, branch="FatJets_massSumRegion_particleNet_nominal", event_filter=None, bins=10, start=0.5, stop=10.5, ht_min=0, ht_max=None, centre_bin=True, weighted=True, xlabel=None, fatjet_only=False):

    # Open input files
    rdf = ROOT.RDataFrame("Events", files)
    # rdf.Display("FatJets_signalRegion_%s" % (btag)).Print()

    # Remove events that does not pass HT criteria
    if ht_max is None:
        rdf = rdf.Filter("HT_nom >= %f" % (ht_min))
    else:
        rdf = rdf.Filter("HT_nom >= %f && HT_nom < %f" % (ht_min,ht_max))

    # Additional filter
    if event_filter:
        rdf = rdf.Filter(event_filter)

    # Decide which index to use
    if fatjet_only:
        index_name = "FatJetIndexA_particleNet" if "particleNet" in files[0] else "FatJetIndexA_btagHbb"

    # Create histogram
    histogram = Hist(hist.axis.Regular(bins=bins, start=start, stop=stop, name="bin", label=xlabel), storage=hist.storage.Weight())

    # Centre bin w.r.t. the bin number
    shift = 0.5 if centre_bin else 0

    # Get values and weights for histogram
    values = rdf.AsNumpy([branch])[branch]-shift # Get column and turn it into a numpy array (AsNumpy returns a dict). Shift with -0.5 to centre the bins w.r.t. the bin number.
    if weighted:
        weights = rdf.AsNumpy(["weight_mc"])["weight_mc"]
    if fatjet_only:
        fatjet_index_branch = rdf.AsNumpy([index_name])[index_name]

    # Additional filter
    if event_filter:
        rdf = rdf.Filter(event_filter)

    # Only take value from one FatJet per event
    if fatjet_only:
 
        tmp_values = [] # List of values to be filled
 
        # Loop over all events
        for i, v in enumerate(values):
            if len(v) ==0 :
                continue 
            tmp_values.append(v[fatjet_index_branch[i].item()])

        values = tmp_values

    # Fill histogram
    if branch == "weight_mc" or not weighted:
        histogram.fill(values)
    else:
        histogram.fill(values, weight=weights)

    return histogram


# Reads in ROOT files in the specified directory, and returns a 2D histogram of the specified branch for FatJetA and FatJetB
def create2DHistogram(files, branch="FatJets_particleNet_massRetrained", tagger="particleNet", event_filter=None, bins=100, start=0, stop=100, ht_min=0, ht_max=None, xlabel=None, ylabel=None, weighted=True):

    # Open input files
    rdf = ROOT.RDataFrame("Events", files)

    # Remove events that does not pass HT criteria
    if ht_max is None:
        rdf = rdf.Filter("HT_nom >= %f" % (ht_min))
    else:
        rdf = rdf.Filter("HT_nom >= %f && HT_nom < %f" % (ht_min,ht_max))

    # Additional filter
    if event_filter:
        rdf = rdf.Filter(event_filter)

    # Create histogram
    histogram = Hist(hist.axis.Regular(bins=bins, start=start, stop=stop, name="x", label=xlabel),
                     hist.axis.Regular(bins=bins, start=start, stop=stop, name="y", label=ylabel), 
                     storage=hist.storage.Weight())

    # Get values and weights for histogram
    values = rdf.AsNumpy([branch])[branch] # Get column and turn it into a numpy array (AsNumpy returns a dict).
    if weighted:
        weights = rdf.AsNumpy(["weight_mc"])["weight_mc"]
    fatjet_index_a = rdf.AsNumpy(["FatJetIndexA_%s" % tagger])["FatJetIndexA_%s" % tagger]
    fatjet_index_b = rdf.AsNumpy(["FatJetIndexB_%s" % tagger])["FatJetIndexB_%s" % tagger]

    values_a = [] # List of values to be filled
    values_b = [] # List of values to be filled

    # Loop over all events
    for i, v in enumerate(values):
        if len(v) < 2 and fatjet_index_a < 0 and fatjet_index_b < 0:
            continue 
        values_a.append(v[fatjet_index_a[i].item()])
        values_b.append(v[fatjet_index_b[i].item()])

    # Fill histogram
    if branch == "weight_mc" or not weighted:
        histogram.fill(values_a, values_b)
    else:
        histogram.fill(values_a, values_b, weight=weights)

    return histogram


########################################################
# Stuff that probably does not need to be used anymore 

# Read the yields from the condor output log file
def readCondorOutput(directory, tagger="particleNet"):

    # Open log file
    log_file = open(directory+"/job0_out.log", "r")

    # Array for the yield values
    column_names = []
    values = []

    start_read = False

    # Loop over all lines
    for line in log_file:

        # Find the line where our values are
        if "Number of events in signal and sideband regions, %s, HT bin 0+ GeV:" % tagger in line:
            start_read = True
        # Stop reading
        elif "Sum of all bins:" in line and start_read:
            break
        # Start read column titles
        elif start_read and "S" in line:
            column_names = list(filter(None, line.strip().split(" ")))
        # Start read values
        elif start_read and line != "\n":
            value_line = list(filter(None, line.strip().split(" ")[1:]))
            values.append(value_line)   

    df = pd.DataFrame(values, columns = column_names)

    return df


# Create a histogram from the yields dataframe
def createHistogramDF(df, region="S_pred", bins=10, start=0.5, stop=10.5):

    # Create histogram
    histogram = Hist(hist.axis.Regular(bins=bins, start=start, stop=stop, name="bin"))
    histogram[:] = df[region].to_numpy()

    return histogram