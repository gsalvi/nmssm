#!/usr/bin/env python3

# TODO: this is currently broken, because it assumes the TriggerSelection and KinematicEventSelection is applied in the FakeFriends. The results will therefore not include the Trigger or Kinematic selections.

import argparse

# Import variables from plotUtils and fileReadUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.fileReadUtils import *


#################################################
#
# Script that compares branches within datasets
#

# Argument parser
parser = argparse.ArgumentParser(description="Process root file(s) and plot Search Region Bin histograms.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-I", "--inputDir", default=None, nargs="+", type=str, help="Specifies the directories with input files. If only one directory is given, comparisons between datasets withing the folder is performed.")
parser.add_argument("-O", "--outputDir", default="./", help="The output directory, has to already exist. (default = %(default)s)")
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify which b-tagger to use, either btagHbb or particleNet, or all for both. (default = %(default)s)")
parser.add_argument("-c", "--compareTaggers", action="store_true", help="If comparison should be between particleNet and btagHbb.")
args = parser.parse_args()


# Creates a stacked histogram of the specified branch sorted by dataset
def plotStackedHistogram(file_dict, branch="weight_mc", histtype=None, bins=100, start=0, stop=1.5, ylog=False, weighted=True, fatjet_only=False, output_dir="./"):

    # Create dictionary of histograms
    hist_dict = {}

    # Create figure
    fig, ax = plt.subplots()

    # Read all files
    for dataset in file_dict:

        # Special case if branch is "mass"
        if branch == "mass":
            tmp_branch = "FatJet_particleNet_massRetrained" if "particleNet" in file_dict[dataset][0] else "FatJet_msoftdrop"
            tmp_labels = ["ParticleNet", "Softdrop"] # Override default labels
            xlabel = "Mass [GeV]"
            fatjet_only = True
        else:
            tmp_branch = branch
            tmp_labels = None
            xlabel = branch.replace("_", " ")

        # Histogram name
        hist_name = dataset.replace("_", " ") if (not "btagHbb" in dataset and not "particleNet" in dataset) else tmp_branch.strip("FatJet_").replace("_", " ")

        # Create and add 
        hist_dict[hist_name] = createHistogram(file_dict[dataset], tmp_branch, None, bins, start, stop, 0, None, False, weighted, fatjet_only)

    # Stack the histograms
    stack = hist.Stack.from_dict(hist_dict)

    # Set colours
    ax.set_prop_cycle(color=[scalarMap.to_rgba(i) for i in range(len(hist_dict))])

    # Plot figure
    if histtype == "fill":
        stack.plot(stack=True, histtype=histtype)
    else:
        stack.plot(linewidth=HIST_LINEWIDTH)

    # Set y-axis limits
    if ylog:
        ax.set_yscale('log')
        ax.set_ylim(ymin=None, ymax=ax.get_ylim()[1]*10) # Increase y-axis range to fit legend
    else:
        ax.set_ylim(ymin=0, ymax=ax.get_ylim()[1]*1.25) # Increase y-axis range to fit legend

    # Set labels
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    ax.set_xlabel(xlabel)
    ax.set_ylabel("Events")

    # Set legend
    handles, labels = ax.get_legend_handles_labels() # Hack to change the signal legend linewidth
    if not histtype == "fill":
        for i, handle in enumerate(handles):
            handles[i] = matplotlib.lines.Line2D([], [], color=scalarMap.to_rgba(i), linewidth=HIST_LINEWIDTH)
            labels[i] = tmp_labels[i] if tmp_labels else " ".join(["$" + l + "$" for l in labels[i].split(" ")]) # Make "math mode"        
    plt.legend(handles=handles, labels=labels, loc='upper left')

    # Save figure
    plt.savefig(output_dir+"/"+branch+("_log" if ylog else "")+("" if weighted else "_unweighted")+".pdf")
    plt.close()
    print("Plotted histogram: " + output_dir+"/"+branch+("_log" if ylog else "")+("" if weighted else "_unweighted")+".pdf")


# Check that output directory exists
if not os.path.exists(args.outputDir):
    raise NotADirectoryError("Output directory %s is not a directory." % (args.outputDir))


# Check that the taggers exist
btagger_list = ["particleNet", "btagHbb"]
if "all" in args.taggers:
    args.taggers = btagger_list


######################################
# Compare datasets within one folder

if args.inputDir is not None:
    if len(args.inputDir) == 1 and not args.compareTaggers:
        for tagger in args.taggers:

            # Get dictionary of files
            file_dict = getFileDict(args.inputDir, tagger)

            # Weight
            plotStackedHistogram(file_dict=file_dict, branch="weight_mc", histtype=None, bins=100, start=-1, stop=200, ylog=False, weighted=True, output_dir=args.outputDir)

            # HT
            plotStackedHistogram(file_dict=file_dict, branch="HT_nom", histtype="fill", bins=100, start=0, stop=10000, ylog=False, weighted=True, output_dir=args.outputDir)

            # HT log
            plotStackedHistogram(file_dict=file_dict, branch="HT_nom", histtype="fill", bins=100, start=0, stop=10000, ylog=True, weighted=True, output_dir=args.outputDir)



#####################################################
# Compare datasets between folders

# FIXME: currently does nothing :-)

if args.inputDir is not None:
    if not args.compareTaggers:
        for tagger in args.taggers:
            pass # Add whatever...


#####################################################
# Compare datasets between b-taggers

# FIXME: currently assumes particleNet is the first directory and btagHbb is the second...

if args.compareTaggers and args.inputDir is not None:

    # Set more colours
    NUM_COLORS = 4
    cm = plt.get_cmap("jet")
    cNorm  = colors.Normalize(vmin=0, vmax=NUM_COLORS-1)
    scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)

    dir_list = args.inputDir

    # Create dictionary
    file_dict = {btagger_list[i] : getFileList(dir_list[i], btagger_list[i]) for i in range(len(btagger_list))}

    # Plot
    plotStackedHistogram(file_dict, branch="mass", histtype="step", bins=50, start=0, stop=150, ylog=False, weighted=True, fatjet_only=True, output_dir=args.outputDir)
