#!/bin/bash

# Script that fixes some bugs in NanoAODTools when using python3.
# To be run within the NanoAODTools/ folder
# https://github.com/cms-nanoAOD/nanoAOD-tools/issues/295.

# puWeightProducer.py
filename_pu="python/postprocessing/modules/common/puWeightProducer.py"
search_pu="hist.SetDirectory(None)"
replace_pu="hist.SetDirectory(0)"
sed -i "s/$search_pu/$replace_pu/" $filename_pu

# btagSFProducer.py
filename_btag="python/postprocessing/modules/btv/btagSFProducer.py"
search_btag="self.algo, os.path.join(self.inputFilePath, self.inputFileName))"
replace_btag="self.algo, os.path.join(self.inputFilePath, self.inputFileName), True)"
sed -i "s/$search_btag/$replace_btag/" $filename_btag

# haddnano.py
filename_haddnano="scripts/haddnano.py"
search_haddnano="#!\/bin\/env python"
replace_haddnano="#!\/usr\/bin\/env python3"
sed -i "s/$search_haddnano/$replace_haddnano/" $filename_haddnano
