#!/usr/bin/env python3
import os
import argparse
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.cm as mplcm
import matplotlib.colors as colors
import mplhep as hep
import pandas as pd
from matplotlib import ticker as mticker

# Import variables from plotUtils
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *

# Argument parser
parser = argparse.ArgumentParser(description="Process some root file(s) and plot some hisograms to an output root file.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-tp", "--inputTP", default=None, help="Specifies the true positive input file", required=True)
parser.add_argument("-fp", "--inputFP", default=None, nargs="+", help="Specifies the false positive input file(s).", required=True)
parser.add_argument("-l", "--legend", default=None, nargs="+", help="Specifies the description for each FP input file to be used in plot legends.", required=True)
parser.add_argument("-o", "--output", default="./ROC.pdf", help="Name of output file. (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default="./", help="Name of output file. (default = %(default)s)")
parser.add_argument("--massSelectROC", action="store_true", help="If we want to plot ROC curves after mass event selection.")
args = parser.parse_args()

# Set more colours
NUM_COLORS = 3

cm = plt.get_cmap("brg")
cNorm  = colors.Normalize(vmin=0, vmax=NUM_COLORS-1)
scalarMap = mplcm.ScalarMappable(norm=cNorm, cmap=cm)

# Plot the ROC curve
def plotROC(data_frame, output_dir, output_file, suffix=""):

    # Get signal TPR
    true_positive_rates = data_frame.loc[data_frame["Dataset"] == "signal"]["TPR"+suffix]

    # Get backgrounds
    bkgs = list(data_frame["Dataset"].unique())
    bkgs.remove("signal")

    # Get the best cuts
    best_cuts, best_coords = chooseCuts(data_frame, suffix)

    # Set style
    fig, ax = plt.subplots()
    plt.gca().set_aspect('auto', adjustable='box')

    # Set colours
    ax.set_prop_cycle(color=[scalarMap.to_rgba(i) for i in range(len(bkgs))])

    # Plot one curve for each background sample
    for bkg in bkgs:

        # Get background FPR
        false_positive_rates = data_frame.loc[data_frame["Dataset"] == bkg]["FPR"+suffix]

        # Calculate Area Under the Curve
        listx, listy = [ list(tuple) for tuple in zip(*sorted(zip(false_positive_rates, true_positive_rates)))] # sort according to x-axis

        # Make sure we calculate the area in the range 0 to 1
        if listx[-1] < 1:
            listx.append(1)
            listy.append(listy[-1]) # Assumes the TP rate has plateaued
        auc = np.trapz(listy, listx)

        # Plot dashed line for
        if "Total background" in bkg:
            linestyle = 'dotted'
        else:
            linestyle = "solid"

        # Plot curve
        plt.plot(listx, listy, linestyle=linestyle, linewidth=7, label="%s (AUC=%.3f)" % (bkg, auc))

        # Add point of the best cut for each FP sample
        ax.annotate(best_cuts[bkg], xy=best_coords[bkg], xytext=(0,0), textcoords='offset points', size=10)

    # Set HT bin label
    if "btagHbb" in output_file:
        tagger = "btagHbb"
    else:
        tagger = "ParticleNet"
    ax.text(0.97, 0.95, tagger, fontsize=25, transform=ax.transAxes, horizontalalignment="right", verticalalignment="top")

    # Set labels
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    ax.set_xlabel("False Positive Rate")
    ax.set_ylabel("True Positive Rate")
    plt.legend(loc='lower right')

    # Set axis limit
    plt.xlim(xmin=0, xmax=1)
    plt.ylim(ymin=0, ymax=1)
    # plt.xscale('log')
    # plt.yscale('log')

    # Save figure
    plt.savefig(output_dir+"/"+output_file)
    plt.close()
    print("Plotted ROC: " + output_dir+"/"+output_file + "\n")


# Plot other ROC-like curves
def plotCurve(data_frame, x_axis, x_label, output_dir, output_file, suffix="", ymax=1):

    # Get signal TPR
    y_vals = data_frame.loc[data_frame["Dataset"] == "signal"]["TPR"+suffix]

    # Get backgrounds
    bkgs = list(data_frame["Dataset"].unique())
    bkgs.remove("signal")

    # Set style
    fig, ax = plt.subplots()
    plt.gca().set_aspect('auto', adjustable='box')

    # Set colours
    ax.set_prop_cycle(color=[scalarMap.to_rgba(i) for i in range(len(bkgs))])

    # Plot one curve for each background sample
    for bkg in bkgs:

        # Ignore total background
        if "Total background" in bkg:
            continue

        # Get x-values
        x_vals = data_frame.loc[data_frame["Dataset"] == bkg][x_axis+suffix]

        # Plot curve
        plt.plot(x_vals, y_vals, linewidth=7, label="%s" % (bkg))

    # Set labels
    hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
    ax.set_xlabel(x_label)
    ax.set_ylabel("True Positive Rate")
    plt.legend(loc='lower right')

    # Set axis limit
    plt.xlim(xmin=1, xmax=10**8)
    plt.ylim(ymin=0, ymax=ymax)

    # Set axis scale
    if any(x>0 for x in x_vals):
        plt.xscale('log')
        ax.set_xticks([10**i for i in range(9)])
        ax.xaxis.set_minor_locator(mticker.LogLocator(numticks=999, subs="auto"))
    plt.yscale('linear')

    # Save figure
    plt.savefig(output_dir+"/"+output_file)
    plt.close()
    print("Plotted curve: " + output_dir+"/"+output_file + "\n")


# Calculates the optimal cut
def chooseCuts(data_frame, suffix=""):

    # Get signal
    sig_df = data_frame.loc[data_frame["Dataset"] == "signal"]

    # Get backgrounds
    bkgs = list(data_frame["Dataset"].unique())
    bkgs.remove("signal")

    best_cuts = {bkg : 0 for bkg in bkgs} # The best cuts for each FP sample
    best_coords = {bkg : (0,0) for bkg in bkgs} # The corresponding best coordinates
    best_efficiencies = {bkg : 0 for bkg in bkgs} # The best signal vs background efficiency

    # Loop over all background samples
    for bkg in bkgs:
        bkg_df = data_frame.loc[data_frame["Dataset"] == bkg]
        # Loop over all cuts
        for i, cut in enumerate(bkg_df["btagCut"].unique()):
            s = sig_df["TPR"+suffix][i]  # Signal value
            b = bkg_df["FPR"+suffix][i] # Background value
            efficiency = s/np.sqrt(b) if b else 0
            # efficiency = np.sqrt(2*((s+b)*np.log(1+s/b)-s)) if b else 0 # Eq. 97 https://arxiv.org/pdf/1007.1727.pdf
            if efficiency > best_efficiencies[bkg] and s > 0.7:
                best_efficiencies[bkg] = efficiency
                best_coords[bkg] = (b, s)
                best_cuts[bkg] = cut

    return best_cuts, best_coords


# Reads a file and returns lists of its values. 
# dataset: if signal or background
# data_frame: if None creates a new dataframe otherwise saves it to the one specified
# suffix: if postmass roc curves are included
# flavour_check: if 2D plots, specifies if a b parton flavour check should be included or not (default for nFalsePositive and nTruePositive values)
def readFile(file_path, dataset="unknown", data_frame=None, suffix="", flavour_check=False):

    # Read file
    df = pd.read_csv(file_path, sep=" ", header=0)
    df["Dataset"] = dataset

    # All jets passing the b-tag cut in the background sample are considered FP
    if dataset != "signal":
        if suffix:
            df["nFalsePositive"+suffix] = df["nFalsePositive"+suffix] + df["nTruePositive"+suffix]
        else:
            df["nFalsePositive"] = df["nFalsePositive"] + df["nTruePositive"]
            df["nTruePositive"] = np.NaN
    elif (dataset == "signal") and ("nEvents" in df.columns) and (not flavour_check):
        # Ignore the b flavour check, all jets passing the b-tag cut are considered TP
        if suffix: 
            df["nTruePositive"+suffix] = df["nFalsePositive"+suffix] + df["nTruePositive"+suffix]
        else:
            df["nTruePositive"] = df["nFalsePositive"] + df["nTruePositive"]
            df["nFalsePositive"] = np.NaN
    else:
        df["nFalsePositive"] = np.NaN

    # Create new or add data to existing dataframe
    if data_frame is None:
        data_frame = df
    else:
        data_frame = pd.concat([data_frame,df])

    return data_frame

# Compute TP and FP rate
def rates(data_frame, suffix=""):

    if "nEvents" in data_frame.columns: # 2D ROC
        denom_sig = "nEvents"
        denom_bkg = "nEvents"
    else: # 1D ROC
        denom_sig = "nbJetsMC"
        denom_bkg = "nJetsMC"

    # Get TP rates
    if suffix:
        data_frame["TPR"+suffix] = data_frame["nTruePositive"+suffix]/data_frame[denom_sig+suffix]
        # Get FP rates
        data_frame["FPR"+suffix] = data_frame["nFalsePositive"+suffix]/data_frame[denom_sig+suffix]
    else:
        data_frame["TPR"] = data_frame["nTruePositive"]/data_frame[denom_sig]
        # Get FP rates
        data_frame["FPR"] = data_frame["nFalsePositive"]/data_frame[denom_bkg]
    return data_frame


# Adds the total background to an existing data frame
def combinedBackgroundRates(data_frame, suffix=""):

    if "nEvents" in data_frame.columns: # 2D ROC
        denom_bkg = "nEvents" + suffix
    else: # 1D ROC
        denom_bkg = "nJetsMC" + suffix

    combined_df = pd.DataFrame() # A new temporary data frame with all combined values

    bkgs = list(data_frame["Dataset"].unique()) # List of background samples
    bkgs.remove("signal")

    # If we already have called this function for a different suffix, add values to the same "Total background" slice
    tot_bkg_exists = False
    if "Total background" in bkgs:
        combined_df = data_frame.loc[data_frame["Dataset"] == "Total background"] # Reference to the original data_frame??
        tot_bkg_exists = True
        bkgs.remove("Total background")

    # Loop over all backgrounds
    for i, bkg in enumerate(bkgs):
        bkg_df = data_frame.loc[data_frame["Dataset"] == bkg].copy()

        if i == 0:
            combined_df["btagCut"] = bkg_df["btagCut"]
            combined_df[denom_bkg] = bkg_df[denom_bkg]
            combined_df["nFalsePositive"+suffix] = bkg_df["nFalsePositive"+suffix]
        else:
            combined_df["nFalsePositive"+suffix] = combined_df["nFalsePositive"+suffix] + bkg_df["nFalsePositive"+suffix]
            combined_df[denom_bkg] = combined_df[denom_bkg] + bkg_df[denom_bkg]

    # Get FP rates
    combined_df["FPR"+suffix] = combined_df["nFalsePositive"+suffix]/combined_df[denom_bkg]
    combined_df["Dataset"] = "Total background"

    if not tot_bkg_exists:
        data_frame = pd.concat([data_frame,combined_df]) # Add the total combined background to data frame

    return data_frame


# Check that output directory exists
if not os.path.exists(args.outputDir):
    raise NotADirectoryError("Output directory %s is not a directory." % (args.outputDir))

# Check that all legend labels have been specified
if len(args.inputFP) != len(args.legend):
    raise ValueError("List of FP inputs and the corresponding legend list must have the same size.")

# Add special values
if args.massSelectROC:
    suffix = "_PostMass" # Suffix on column name
else:
    suffix = ""

# Read input files
flavour_check = False # If only correctly tagged b-jets should be counted as true positives
input_df = readFile(args.inputTP, "signal", suffix=suffix, flavour_check=flavour_check) # For 1D plots, all b-jets in signal sample are assumed to come from Higgs decay
for file, legend in zip(args.inputFP, args.legend):
    input_df = readFile(file, legend, input_df, suffix=suffix, flavour_check=flavour_check) # For 1D plots, all jets in background sample are considered as background

# Check that the b-tagger cuts are the same
if input_df["btagCut"].nunique() * input_df["Dataset"].nunique() != len(input_df):
    raise ValueError("Input files do not have the same b-tagger cut values.")

# Plot ROC curves after mass event selection
if args.massSelectROC:

    # Compute TP and FP rates
    input_df = rates(input_df, suffix)

    # Plot ROC curve
    plotROC(input_df, args.outputDir, suffix.strip("_")+"_"+args.output, suffix)

    # Plot other curve
    plotCurve(input_df, "nFalsePositive", "Number of False Positives", args.outputDir, "almost"+suffix+"_"+args.output, suffix, ymax=0.2)

    # Plot combined background ROC curve
    input_df = combinedBackgroundRates(input_df, suffix)
    plotROC(input_df, args.outputDir, "total"+suffix+"_"+args.output, suffix)

else:
    # Compute TP and FP rates
    input_df = rates(input_df)

    # Plot ROC curve
    plotROC(input_df, args.outputDir, args.output)

    # Plot other curve
    plotCurve(input_df, "nFalsePositive", "Number of False Positives", args.outputDir, "almost_"+args.output)

    # Plot combined background ROC curve
    input_df = combinedBackgroundRates(input_df)
    plotROC(input_df, args.outputDir, "total_"+args.output)
