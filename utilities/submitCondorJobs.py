#!/usr/bin/env python3
import os
import stat
import argparse
import datetime
import subprocess

# Script to submit NMSSM PostProcessor jobs to HTCondor
# TO DO: make it split into several smaller jobs like in Sam's original script.

# To be run from within the NMSSM/ folder:
#    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -i <INPUT> -O <outputDir> -n <maxEntries>


# Argument Parser
parser = argparse.ArgumentParser(description='Submits NMSSM PostProcessor jobs to HTCondor.')
# General flags
parser.add_argument("--runPostProc", action="store_true", help="Submit a post processing job.")
parser.add_argument("--runAnalysis", action="store_true", help="Submit an analysis job.")
parser.add_argument("-i", "--input", default=None, nargs="+", help="Specifies the input file(s). Separate multiple root file inputs with a space, or use a text file with the data file names (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default=".", help="Name of output directory. (default = %(default)s)")
parser.add_argument("-n", "--maxEntries", default=None, type=int, help="Maximum number of events to process, per input file... (default = %(default)s)")
parser.add_argument("--isData", action="store_true", help="If input is data. If not specified, then MC events are assumed.")
# Postproc flags
parser.add_argument("-y", "--year", default=None, type=str, help="The year of the input files. Only for postprocessing!")
# Analysis flags
parser.add_argument("-I", "--inputDir", default=None, nargs="+", help="Directory to the input files. Will take all matching files in the specified folders as input. Cannot be used at the same time as --input. Only for analysis! (default = %(default)s)")
parser.add_argument("--noEventSelection", action="store_true", help="If analysis should be run without cuts applied, thus no event selection. Needed to create a Friend tree with the same number of events as the input tree.")
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, btagHbb and/or particleNet, or all for all implemented b-taggers. (default = %(default)s)")
parser.add_argument("-g", "--grid", default=None, help="Specify which mass grid to use ('Old', 'New'). (default = %(default)s)")

args = parser.parse_args()


# Returns a dictionary with all the configuration settings
def set_configs(args):

    # Check that input arguments are valid
    if not args.runPostProc and not args.runAnalysis:
        raise ValueError("Please specify one job type to run (--runPostProc and/or --runAnalysis).")
    elif args.runPostProc and args.year is None:
        raise ValueError("A year must be specified for post processing (--year).")
    elif args.runPostProc and args.input is None:
        raise ValueError("Input must be specified for post processing (--input).")

    # Check that the taggers exist
    btagger_list = ["particleNet", "btagHbb"]
    if "all" in args.taggers:
        args.taggers = btagger_list
    else:
        for t in args.taggers:
            if t not in btagger_list:
                raise Exception("The b-tagger(s) are not defined: %s" % t)

    # Check that the mass grid exists
    mass_grid_list = ["Old", "New"]
    if args.grid not in mass_grid_list and args.grid is not None:
        raise ValueError("Mass grid not defined: %s " % args.grid)

    cfg = {}
    # Job directories and file names
    cfg["working_area"] = os.getcwd()
    cfg["nmssm_area"] = "python/postprocessing/NMSSM/"
    cfg["log_name"] = "job$(Process)"
    cfg["condor_subfile"] = "condor_submit_file"
    cfg["job_script"] = "condor_job_script.sh"
    # General arguments
    cfg["runPostProc"] = args.runPostProc
    cfg["runAnalysis"] = args.runAnalysis
    if args.input is None:
        cfg["input"] = args.input
    else:
        cfg["input"] = " ".join(args.input)
    if args.outputDir[0] == "/": # An absolute path
        cfg["outputDir"] = args.outputDir + "_" + datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    else: # Relative path
        cfg["outputDir"] = cfg["working_area"] + "/" + args.outputDir + "_" + datetime.datetime.now().strftime('%Y%m%d_%H%M%S')
    cfg["maxEntries"] = args.maxEntries
    cfg["year"] = args.year
    # The PostProc arguments
    cfg["isData"] = args.isData
    # The Analysis arguments
    cfg["inputDir"] = args.inputDir
    cfg["noEventSelection"] = args.noEventSelection
    cfg["taggers"] = args.taggers
    cfg["mass_grid"] = args.grid
    # Check this is run from CMSSW_VERSION/src/PhysicsTools/NanoAODTools
    if cfg["working_area"].split("/")[-1] != "NanoAODTools":
        raise Exception("Script not run from CMSSW_VERSION/src/PhysicsTools/NanoAODTools")

    return cfg

# Make the bash script that sets up the environment and runs the jobs
def make_job_script(cfg):
    # Make the main python command string
    python_postproc_cmd = "python3 {nmssm_area}/postProcNMSSM.py -y {year}".format(**cfg)
    python_analysis_cmd = "python3 {nmssm_area}/analysisNMSSM.py -y {year}".format(**cfg)
    # Add general arguments
    if cfg["maxEntries"]:
        python_postproc_cmd += " -n {maxEntries}".format(**cfg)
        python_analysis_cmd += " -n {maxEntries}".format(**cfg)
    if cfg["isData"]:
        python_postproc_cmd += " --isData"
        python_analysis_cmd += " --isData"
    # Run PostProc
    if cfg["runPostProc"]:
        python_postproc_cmd += " -i {input} -O {outputDir}".format(**cfg)
    # Run Analysis
    if cfg["runAnalysis"]:
        # If run both
        if cfg["runPostProc"]:
            python_analysis_cmd += " -I {outputDir}".format(**cfg)
        # If input file added
        elif cfg["input"] is not None:
            python_analysis_cmd += " -i {input} -O {outputDir}".format(**cfg)
        # If input directory added
        else:
            python_analysis_cmd += " -I " + " ".join(args.inputDir)
            python_analysis_cmd += " -O {outputDir}".format(**cfg)
        # Add which taggers to run over
        python_analysis_cmd += " -t " + " ".join(args.taggers)
        if cfg["noEventSelection"]:
            python_analysis_cmd += " --noEventSelection"
        # Add mass grid to use
        if cfg["mass_grid"]:
            python_analysis_cmd += " --grid {mass_grid}".format(**cfg)

    # The script body
    text = """
#!/usr/bin/env bash

# Job script for submitting to condor
# Used by condor_submit_file.sh

# Check Host
echo Host: $HOST

# Set environment
source ~/.bash_profile
echo Directory: $TMPDIR

# Ensure CMSSW environment is set up.
cd {working_area}/../..
eval `scramv1 runtime -sh`
echo CMSSW: $CMSSW_RELEASE_BASE $CMSSW_BASE
cd {working_area}

# Check user proxy
echo $X509_USER_PROXY
voms-proxy-info -all

# Run job
echo Running job...
""".format(**cfg)

    if cfg["runPostProc"]:
        text += """
echo {python_cmd}
{python_cmd}
""".format(python_cmd=python_postproc_cmd)

    if cfg["runAnalysis"]:
        text += """
echo {python_cmd}
{python_cmd}
""".format(python_cmd=python_analysis_cmd)

    text += "echo Finished job!"

    return text

# Make the Condor submission file
def make_submit_file(cfg):
    text = """
# Condor submit file
Universe               = vanilla
Executable             = {outputDir}/condor_job_script.sh

# Log files
Log                    = {outputDir}/{log_name}.log
Output                 = {outputDir}/{log_name}_out.log
Error                  = {outputDir}/{log_name}_err.log

# Some OS specifications
Request_memory         = 4 GB
requirements           = (OpSysAndVer =?= "CentOS7")
request_cpus           = 1
Getenv                 = False

# User proxy
use_x509userproxy = True

# Queueueueu
queue
""".format(**cfg)
    return text


# Set configurations
cfg = set_configs(args)

# Make output directory
os.makedirs(cfg['outputDir'])

# Make Condor files
condor_subfile = "{outputDir}/{condor_subfile}".format(**cfg)
with open(condor_subfile, "w", encoding="utf-8") as f:
    f.write(make_submit_file(cfg))

job_script = "{outputDir}/{job_script}".format(**cfg)
with open(job_script, "w", encoding="utf-8") as f:
    f.write(make_job_script(cfg))

# Make submission script executable
st = os.stat(job_script)
os.chmod(job_script, st.st_mode | stat.S_IEXEC)

# Submit to Condor
subprocess.Popen(['condor_submit',condor_subfile]).communicate()
