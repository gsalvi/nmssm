 #!/bin/bash   

OPTION=$1                          
JOBS="jobs"
TEMP="templatecard"
PROC="NMSSMCascade"
PROC1="mSquark"
PROC2="mSUSY"
FRAG="fragment"
PART="_"

       
#######################
if [ "$OPTION" = 1 ];
then
 
    MGL=100000.0
    
    for MSQ  in {2400..3200..400}; do
        MODEL=${PROC}${PART}${PROC1}${PART}${MSQ}
        mkdir -p "${JOBS}/${MODEL}"
	sed -e "s/%MSQ%/${MSQ}/g" ${TEMP}/${PROC}_${PROC1}_proc_card.dat > ${JOBS}/${MODEL}/${MODEL}_proc_card.dat
        cp ${TEMP}/${PROC}_run_card.dat "${JOBS}/${MODEL}/${MODEL}_run_card.dat"
        sed -e "s/%MSQ%/${MSQ}/g;s/%MGL%/${MGL}/g" ${TEMP}/${PROC}_customizecards.dat > ${JOBS}/${MODEL}/${MODEL}_customizecards.dat
        sed -e "s/%MSQ%/${MSQ}/g;s/%MGL%/${MGL}/g" ${TEMP}/${PROC}.slha > ${JOBS}/${MODEL}/${MODEL}.slha
        sed -e "s/%MGL%/${MGL}/g;s/%MSQ%/${MSQ}/g;s/%PROC%/${PROC1}/g" ${TEMP}/${FRAG}.py > ${JOBS}/${MODEL}/${FRAG}.py
    done
    
elif [ "$OPTION" = 2 ];
then
    
    
    for MSQ  in {2400..3200..400}; do
        MGL=$((${MSQ}+10))
        MODEL=${PROC}${PART}${PROC2}${PART}${MSQ}
        mkdir -p "${JOBS}/${MODEL}"
	sed -e "s/%MSQ%/${MSQ}/g" ${TEMP}/${PROC}_${PROC2}_proc_card.dat > ${JOBS}/${MODEL}/${MODEL}_proc_card.dat
        cp ${TEMP}/${PROC}_run_card.dat "${JOBS}/${MODEL}/${MODEL}_run_card.dat"
        sed -e "s/%MSQ%/${MSQ}/g;s/%MGL%/${MGL}/g" ${TEMP}/${PROC}_customizecards.dat > ${JOBS}/${MODEL}/${MODEL}_customizecards.dat
        sed -e "s/%MSQ%/${MSQ}/g;s/%MGL%/${MGL}/g" ${TEMP}/${PROC}.slha > ${JOBS}/${MODEL}/${MODEL}.slha
        sed -e "s/%MGL%/${MGL}/g;s/%MSQ%/${MSQ}/g;s/%PROC%/${PROC2}/g" ${TEMP}/${FRAG}.py > ${JOBS}/${MODEL}/${FRAG}.py
    done
    
else 
    echo "Insert either 1 (for gluino decoupled) or 2 as arguments (for gluino not decoupled)"
    
fi


	


