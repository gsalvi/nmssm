import os
import sys
import argparse
import shutil

#from submitLHECondorJob import submitCondorJob

def submitCondorJob(proc, executable, options, infile, label, outputToTransfer=None, submit=False, isGridpackJob=False):
    hostname = os.uname()[1]
    logDir = os.path.join("logs",proc)
    subfile = "condor_"+proc +"_"+label+".cmd"
    f = open(subfile,"w")
    f.write("Universe = vanilla\n")
    if isGridpackJob :
        f.write("request_cpus=8\n")
        f.write("request_memory=4200MB\n")
        #    f.write('requirements = (OpSysAndVer =?= "SLCern6")\n') # to run on lxplus6                                                      
    f.write("Executable = "+executable+"\n")
    f.write("arguments =  "+(' '.join(options))+"\n")
    f.write("Transfer_Executable = True\n")
    f.write("should_transfer_files = YES\n")
    f.write("transfer_input_files = "+infile+"\n")
    if outputToTransfer is not None:
        f.write("transfer_Output_files = "+outputToTransfer+"\n")
        f.write("WhenToTransferOutput  = ON_EXIT\n")
    f.write("Notification = Never\n")
    f.write("Log=%s/gen_%s_%s.log.$(Cluster).$(Process)\n"%(logDir, proc, label))
    f.write("output=%s/gen_%s_%s.out.$(Cluster).$(Process)\n"%(logDir, proc, label))
    f.write("error=%s/gen_%s_%s.err.$(Cluster).$(Process)\n"%(logDir, proc, label))
    f.write("queue 1\n")
    f.close()
        
    cmd = "condor_submit "+subfile
    print(cmd)
    if submit:
        os.system(cmd)
                

        
if __name__ == '__main__':        
        parser = argparse.ArgumentParser()
        parser.add_argument('proc', help="Names of physics model")
        parser.add_argument('--fragment', '-f', help='Path to gen fragment', required=True)
        parser.add_argument('--qcut-range', dest='qcutRange', nargs=2, type=int, default=[50,100], help="Range of qcuts to scan over")
        #    parser.add_argument('--qcut-list', dest='qcutList', nargs='+', type=int, default=[],help="List of qcuts to scan over")
        parser.add_argument('--qcut-step', dest='qcutStep', type=int, default=2)
        parser.add_argument('--nJetMax' ,help="nJetMax argument in the fragment", default=2)
        parser.add_argument('--outdir','-o', help="Output directory (must be accessible from worker nodes)", default='/home/ppd/kay83529/nmssm/QCUT_study')
        parser.add_argument('--nevents', '-n', help="Number of events per job", type=int, default=25000)
        parser.add_argument('--njobs', '-j', help="Number of condor jobs", type=int, default=1)
        parser.add_argument('--no-sub', dest='noSub', action='store_true', help='Do not submit jobs')
        #    parser.add_argument('--proxy', dest="proxy", help="Path to proxy", default=os.environ["X509_USER_PROXY"])
        #    parser.add_argument('--rseed-start', dest='rseedStart', help='Initial value for random seed', type=int, default=500)
        #parser.add_argument('--executable', help='Path to executable that should be run',default = '/home/ppd/kay83529/nmssm/QCUT_study/runLHEPythiaJob.sh')
        parser.add_argument('--mass', default=0)
        args = parser.parse_args()
        
        proc = args.proc
        fragment = args.fragment
        nevents = args.nevents
        njobs = args.njobs
        #    rseedStart = args.rseedStart
        #executable = args.executable
        qcutRange = range(args.qcutRange[0], args.qcutRange[1]+1, args.qcutStep)
        #  qcutList = args.qcutList
        nJetMax = args.nJetMax
        mass = args.mass
        user      = os.environ['USER']
        hostname  = os.uname()[1]
        script_dir = os.path.dirname(os.path.realpath(__file__))
        executable = script_dir+'/runLHEPythiaJob.sh'

        out_dir= args.outdir
        
        print("Will generate LHE events using tarball and shower them using Pythia")
        
        #need to transfer gen fragment

        fragfile = os.path.basename(fragment)        

        logDir = os.path.join("logs",proc)
        
        if not os.path.isdir(logDir):
            os.makedirs(logDir)
        else:
            shutil.rmtree(logDir)
            os.makedirs(logDir)
            
            
        #outdir = out_dir+'/'+mass
        if not os.path.isdir(out_dir):
            os.makedirs(out_dir)
                
                #    if len(qcutList)>0: qcutRange=qcutList
                
        for qcut in qcutRange:
            print("QCut", qcut)
            for j in range(0,njobs):
                #rseed = str(rseedStart+j)
                #            if mass:
                options = [ str(nevents), fragfile,str(qcut),str(mass)]#, fragfile, str(qcut), str(nJetMax), str(mass), outdir, str(j+1)]
                #else:
                #   options = [proc, str(nevents), fragfile, str(qcut), str(nJetMax), "0.00000", outdir, str(j+1)]
                print("Options:",(' '.join(options)))
                submitCondorJob(proc, executable, options, fragment, label=str(qcut), submit=(not args.noSub))
                
                                    
                                    
