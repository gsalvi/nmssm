# QCUT

#### Checkout Instructions for the tarball production
- `git clone https://github.com/cms-sw/genproductions.git -b mg265UL`
- ``` cd genproductions/bin/MadGraph5_aMCatNLO/```
-``` git clone https://gitlab.cern.ch/gsalvi/nmssm.git```
- ``` cd cd nmssm/QCUT_study/cards```, you will find `templatecard` folder and `writeallcards.sh` script
- Run `writeallcards.sh`, giving an argument of 1 if you want to produce cards with `gluino mass`=10000 (gluino mass is decoupled), of 2 if you want to produce cards with `gluino mass`=`squark mass`+10 GeV (gluino mass is not decoupled). e.g: `source writeallcards.sh 2`.
- in your folder  `jobs` you have now many other folders that have been produced, all referring to the model you decided to study and with a different mSUSY depending on the range specified in `writeallcards.sh`. Inside each of these folders there are all the cards: run_card.dat, proc_card.dat,customizecards.dat, shla file and fragment.py.


To produce the tarball for the mSUSY you want and the model you selected you can now:
-```cd ../.. to genproductions/bin/MadGraph5_aMCatNLO/```
-  run  ```gridpack_generation.sh``` as: ```./gridpack_generation.sh card-name nmssm/QCUT_study/cards/jobs/card-name```

where card-name is depending on the previous selections. 
e.g. ./gridpack_generation.sh NMSSMCascade_mSquark_2800 nmssm/QCUT_study/cards/jobs/NMSSMCascade_mSquark_2800

#### For the QCUT study,

Once the tarball is produced, for the QCUT study:

- ```cd genproductions/bin/MadGraph5_aMCatNLO/QCUT```
- in this directory  there are `runLHEPythiaJob.sh`, `submitLHEPythiaCondorJob.py` scripts. You might need to change the output directory in `submitLHEPythiaCondorJob.py`, decide where you want to save your root files.
- different root files with different qcuts running can be obtained:

python submitLHEPythiaCondorJob.py --fragment <fragment.py> --qcut-range < qcut minim  qcut max> --qcut-step < qcut-step >  --nevents < nevents > --njobs < njobs > --executable runLHEPythiaJob.sh –mass < mass > <Names of physics model>>

An example is: python submitLHEPythiaCondorJob.py --fragment fragment.py --qcut-range 30 120 --qcut-step 5  --nevents 25000 --njobs 1  --executable runLHEPythiaJob.sh --mass 800 nmssm

