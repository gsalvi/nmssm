#!/bin/bash                                                                                                                            
NEVENTS=$1
FRAGFILE=$2
QCUT=$3
MSQ=$4


# Make voms proxy                                                                                                                      
voms-proxy-init --voms cms --out $(pwd)/voms_proxy.txt --hours 4
export X509_USER_PROXY=$(pwd)/voms_proxy.txt

export SCRAM_ARCH=slc7_amd64_gcc700

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_25/src ] ; then
  echo release CMSSW_10_6_25 already exists
else
  scram p CMSSW CMSSW_10_6_25
fi
cd CMSSW_10_6_25/src
eval `scram runtime -sh`

# Download fragment from McM                                                                                                           
mkdir -p Configuration/GenProduction/python/

sed "s/%QCUT%/${QCUT}/" ../../${FRAGFILE} > Configuration/GenProduction/python/NMSSMCascade_mSquark-fragment.py


scram b
cd ../..



GENFILE='GEN_'${MSQ}'_'${QCUT}'.root'
                                                                                                                                                                                                                                                                          
cmsDriver.py Configuration/GenProduction/python/NMSSMCascade_mSquark-fragment.py --python_filename NMSSMCascade_mSquark_1_cfg.py --eventcontent RAWSIM,LHE --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN,LHE  --fileout file:${GENFILE}  --conditions 106X_upgrade2018_realistic_v4 --beamspot Realistic25ns13TeVEarly2018Collision --step LHE,GEN --geometry DB:Extended --era Run2_2018 --no_exec --mc -n ${NEVENTS};



cmsRun NMSSMCascade_mSquark_1_cfg.py


