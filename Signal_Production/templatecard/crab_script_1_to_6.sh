NEVENTS=200
NEVENTSGEN=$((3*$NEVENTS))
RANDOM_SEED=$(($1+10000))
NJOB=$1
#FRAGFILE=fragment.py

echo "I am using "${RANDOM_SEED} "and number of job= " ${NJOB} 

#voms-proxy-init --voms cms --out $(pwd)/voms_proxy.txt --hours 4
#export X509_USER_PROXY=$(pwd)/voms_proxy.txt



export SCRAM_ARCH=slc7_amd64_gcc700

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_25/src ] ; then
  echo release CMSSW_10_6_25 already exists
else
  scram p CMSSW CMSSW_10_6_25
fi
cd CMSSW_10_6_25/src
eval `scram runtime -sh`

# Download fragment from McM                                                                                                        
                                                                                                                                     
mkdir -p Configuration/GenProduction/python/

cp ../../fragment.py  Configuration/GenProduction/python/NMSSMCascade-fragment.py


scram b
cd ../..

cmsDriver.py Configuration/GenProduction/python/NMSSMCascade-fragment.py --python_filename NMSSMCascade_1_step_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN  --fileout file:NMSSMCascade_1_step_cfg.root  --conditions 106X_upgrade2018_realistic_v4 --beamspot Realistic25ns13TeVEarly2018Collision --step LHE,GEN --geometry DB:Extended --era Run2_2018 --no_exec --mc -n ${NEVENTSGEN};




echo "process.RandomNumberGeneratorService.generator.initialSeed = $RANDOM_SEED" >> NMSSMCascade_1_step_cfg.py
echo "process.RandomNumberGeneratorService.externalLHEProducer.initialSeed = $RANDOM_SEED" >> NMSSMCascade_1_step_cfg.py
echo "process.source.firstRun = cms.untracked.uint32($NJOB)" >> NMSSMCascade_1_step_cfg.py



cmsRun NMSSMCascade_1_step_cfg.py

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_17_patch1/src ] ; then
  echo release CMSSW_10_6_17_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_17_patch1
fi
cd CMSSW_10_6_17_patch1/src
eval `scram runtime -sh`

scram b
cd ../..



# cmsDriver command //second_step                                                                                                    
#cmsDriver.py  --python_filename NMSSMCascade_2_step_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM --fileout file:NMSSMCascade_2_step_cfg.root --conditions 106X_upgrade2018_realistic_v11_L1v1 --beamspot Realistic25ns13TeVEarly2018Collision --step SIM --geometry DB:Extended --filein file:NMSSMCascade_1_cfg.root --era Run2_2018 --runUnscheduled --no_exec --mc -n ${NEVENTS};


echo "process.RandomNumberGeneratorService.generator.initialSeed = $RANDOM_SEED" >> NMSSMCascade_2_step_cfg.py
sed -i "s/input = cms.untracked.int32(-1)/input = cms.untracked.int32($NEVENTS)/g" NMSSMCascade_2_step_cfg.py

cmsRun  NMSSMCascade_2_step_cfg.py



####
source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_17_patch1/src ] ; then
  echo release CMSSW_10_6_17_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_17_patch1
fi
cd CMSSW_10_6_17_patch1/src
eval `scram runtime -sh`

scram b
cd ../..

# cmsDriver command                                                                                                                  
#cmsDriver.py  --python_filename NMSSMCascade_3_step_cfg.py --eventcontent PREMIXRAW --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM-DIGI --fileout file:NMSSMCascade_3_step_cfg.root --pileup_input "dbs:/Neutrino_E-10_gun/RunIISummer20ULPrePremix-UL18_106X_upgrade2018_realistic_v11_L1v1-v2/PREMIX" --conditions 106X_upgrade2018_realistic_v11_L1v1 --step DIGI,DATAMIX,L1,DIGI2RAW --procModifiers premix_stage2 --geometry DB:Extended --filein file:NMSSMCascade_2_step_cfg.root --datamix PreMix --era Run2_2018 --runUnscheduled --no_exec --mc -n ${NEVENTS}

echo "process.RandomNumberGeneratorService.generator.initialSeed = $RANDOM_SEED" >> NMSSMCascade_3_step_cfg.py


cmsRun NMSSMCascade_3_step_cfg.py



######
source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_2_16_UL/src ] ; then
  echo release CMSSW_10_2_16_UL already exists
else
  scram p CMSSW CMSSW_10_2_16_UL
fi
cd CMSSW_10_2_16_UL/src
eval `scram runtime -sh`

scram b
cd ../..

#cmsDriver.py  --python_filename NMSSMCascade_4_step_cfg.py --eventcontent RAWSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier GEN-SIM-RAW --fileout file:NMSSMCascade_4_step_cfg.root --conditions 102X_upgrade2018_realistic_v15 --customise_commands 'process.source.bypassVersionCheck = cms.untracked.bool(True)' --step HLT:2018v32 --geometry DB:Extended --filein file:NMSSMCascade_3_step_cfg.root  --era Run2_2018 --no_exec --mc -n ${NEVENTS}

echo "process.RandomNumberGeneratorService.generator.initialSeed = $RANDOM_SEED" >> NMSSMCascade_4_step_cfg.py

cmsRun NMSSMCascade_4_step_cfg.py




#####

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_17_patch1/src ] ; then
  echo release CMSSW_10_6_17_patch1 already exists
else
  scram p CMSSW CMSSW_10_6_17_patch1
fi
cd CMSSW_10_6_17_patch1/src
eval `scram runtime -sh`

scram b
cd ../..



#cmsDriver.py  --python_filename NMSSMCascade_5_step_cfg.py  --eventcontent AODSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier AODSIM --fileout file:NMSSMCascade_5_step_cfg.root --conditions 106X_upgrade2018_realistic_v11_L1v1 --step RAW2DIGI,L1Reco,RECO,RECOSIM,EI --geometry DB:Extended --filein file:NMSSMCascade_4_step_cfg.root  --era Run2_2018 --runUnscheduled --no_exec --mc -n ${NEVENTS}

echo "process.RandomNumberGeneratorService.generator.initialSeed = $RANDOM_SEED" >> NMSSMCascade_5_step_cfg.py

cmsRun NMSSMCascade_5_step_cfg.py

#####

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_25/src ] ; then
  echo release CMSSW_10_6_25 already exists
else
  scram p CMSSW CMSSW_10_6_25
fi
cd CMSSW_10_6_25/src
eval `scram runtime -sh`

scram b
cd ../..


# cmsDriver command                                                                                                                  
#cmsDriver.py  --python_filename NMSSMCascade_6_step_cfg.py  --eventcontent MINIAODSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier MINIAODSIM --fileout file:NMSSMCascade_6_step_cfg.root --conditions 106X_upgrade2018_realistic_v16_L1v1 --step PAT --procModifiers run2_miniAOD_UL --geometry DB:Extended --filein file:NMSSMCascade_5_step_cfg.root --era Run2_2018 --runUnscheduled --no_exec --mc -n ${NEVENTS}

echo "process.RandomNumberGeneratorService.generator.initialSeed = $RANDOM_SEED" >> NMSSMCascade_6_step_cfg.py

cmsRun -j FrameworkJobReport.xml -p NMSSMCascade_6_step_cfg.py
