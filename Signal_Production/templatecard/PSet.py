
import FWCore.ParameterSet.Config as cms
process = cms.Process('SIM')
process.source = cms.Source("EmptySource")
process.output = cms.OutputModule("PoolOutputModule",
                                  fileName=cms.untracked.string('NMSSMCascade_6_step_cfg.root'))
process.out = cms.EndPath(process.output)
process.maxEvents = cms.untracked.PSet(input=cms.untracked.int32(10))
