export SCRAM_ARCH=slc7_amd64_gcc700

source /cvmfs/cms.cern.ch/cmsset_default.sh
if [ -r CMSSW_10_6_26/src ] ; then
  echo release CMSSW_10_6_26 already exists
else
  scram p CMSSW CMSSW_10_6_26
fi
cd CMSSW_10_6_26/src
eval `scram runtime -sh`

scram b
cd ../..


# cmsDriver command
cmsDriver.py  --python_filename NMSSMCascade_7_step_cfg.py --eventcontent NANOAODSIM --customise Configuration/DataProcessing/Utils.addMonitoring --datatier NANOAODSIM --fileout file:NMSSMCascade_7_step_cfg.root --conditions 106X_upgrade2018_realistic_v16_L1v1 --step NANO --filein file:NMSSMCascade_6_step_cfg.root --era Run2_2018,run2_nanoAOD_106Xv2 --no_exec --mc -n -1
