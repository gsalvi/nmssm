import os                         
inputDatasetString = []
statusLines = os.popen("crab status -d crab_%MPARTICLE%_%MSQ%_%MH%", "r").readlines()
for line in statusLines:
	if line[:15] == "Output dataset:":
		inputDatasetString = line.rstrip()
		for c in range(15,len(inputDatasetString)):
			if inputDatasetString[c] == '/':
				inputDatasetString = inputDatasetString[c:]
				print(inputDatasetString) # for debugging
				break
		break

from CRABClient.UserUtilities import config                                                                                                                   
config = config()                                                                                                                                             
config.Data.unitsPerJob = 125
config.Data.totalUnits = 10000
config.Data.splitting = 'FileBased'                                                                                                                           
config.Data.inputDBS = 'phys03'                                                                                                                               
config.Data.publication = True   
config.General.requestName = 'NANO_%MPARTICLE%_%MSQ%_%MH%' 
config.Data.outputDatasetTag = 'UL2018_NANO_v2'      
config.Data.inputDataset = inputDatasetString
config.General.transferOutputs = True                                                                                                                         
config.General.transferLogs    = True   
config.JobType.maxMemoryMB = 2500                                                                                                                      
config.JobType.pluginName = 'Analysis'                                                                                                                        
config.JobType.psetName = 'NMSSMCascade_7_step_cfg.py'                                                                                    
config.Site.storageSite = 'T2_UK_SGrid_RALPP'



