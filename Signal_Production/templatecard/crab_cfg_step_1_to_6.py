
from CRABClient.UserUtilities import config                                                                       
config = config()                                                                                                
config.Data.unitsPerJob = 200                                                                                      
config.Data.totalUnits = 150000                                                                                     
config.Data.splitting = 'EventBased'                                                                             
config.Data.inputDBS = 'global'                                                                                  
config.Data.publication = True                                                                                   
config.Data.outputDatasetTag = 'UL2018_Version1'                                                       
config.Data.outputPrimaryDataset ='NMSSMCascade_mH-%MH%_%MPARTICLE%_%MSQ%_TuneCP5_13TeV-madgraph-pythia8'                 
config.General.requestName = '%MPARTICLE%_%MSQ%_%MH%'                                                       
config.General.transferOutputs = True                                                                            
config.General.transferLogs    = True   
config.JobType.maxMemoryMB = 3072                                                                         
config.JobType.pluginName = 'PrivateMC'                                                                          
config.JobType.psetName = 'PSet.py'                                        
config.JobType.scriptExe = 'crab_script_1_to_6.sh' 
config.JobType.inputFiles = ['NMSSMCascade_%MPARTICLE%_%MSQ%_slc7_amd64_gcc700_CMSSW_10_6_19_tarball.tar.xz','fragment.py','NMSSMCascade_2_step_cfg.py','NMSSMCascade_3_step_cfg.py','NMSSMCascade_4_step_cfg.py','NMSSMCascade_5_step_cfg.py','NMSSMCascade_6_step_cfg.py']                  
config.Site.storageSite = 'T2_UK_SGrid_RALPP'





