 #!/bin/bash   

OPTION=$1                          
JOBS="jobs"
TEMP="templatecard"
PROC="NMSSMCascade"
PROC1="mSquark"
PROC2="mSUSY"
FRAG="fragment"
PART="_"

mHvalues=(30 35 40 50 60 70 80 90 100 110 125 140 155 170 185 200)

#######################
for MSQ in {800..3200..200}; do
    if [ "$OPTION" = 1 ];
    then
        MPARTICLE=$PROC1
        MGL=100000.0
    elif [ "$OPTION" = 2 ];
    then
        MPARTICLE=$PROC2
        MGL=$((${MSQ}+10))
    else
        echo "Insert either 1 (for gluino decoupled) or 2 as arguments (for gluino not decoupled)"
    fi
    MODEL=${PROC}${PART}${MPARTICLE}${PART}${MSQ}
    mkdir -p "${JOBS}/${MODEL}"
    sed -e "s/%MSQ%/${MSQ}/g" ${TEMP}/${PROC}_${MPARTICLE}_proc_card.dat > ${JOBS}/${MODEL}/${MODEL}_proc_card.dat
    cp ${TEMP}/${PROC}_run_card.dat "${JOBS}/${MODEL}/${MODEL}_run_card.dat"
    sed -e "s/%MSQ%/${MSQ}/g;s/%MGL%/${MGL}/g" ${TEMP}/${PROC}_customizecards.dat > ${JOBS}/${MODEL}/${MODEL}_customizecards.dat
    
    for MH in ${mHvalues[@]}; do
        MODEL=${PROC}${PART}${MPARTICLE}${PART}${MSQ}${PART}${MH}
        mkdir -p "crab_jobs/${MODEL}"
        MNeutralino2=$(echo "($MH*1.0101010101010101)"| bc)
        MNeutralino1=$(echo "($MNeutralino2-$MH-0.1)"| bc)
        sed -e "s/%MGL%/${MGL}/g;s/%MSQ%/${MSQ}/g;s/%MPARTICLE%/${MPARTICLE}/g;s/%MH%/${MH}/g;s/%MNeutralino1%/${MNeutralino1}/g;s/%MNeutralino2%/${MNeutralino2}/g" ${TEMP}/${FRAG}.py > crab_jobs/${MODEL}/${FRAG}.py
        sed -e "s/%MSQ%/${MSQ}/g;s/%MPARTICLE%/${MPARTICLE}/g;s/%MH%/${MH}/g;" ${TEMP}/crab_cfg_step_1_to_6.py > crab_jobs/${MODEL}/crab_cfg_step_1_to_6.py
        sed -e "s/%MSQ%/${MSQ}/g;s/%MPARTICLE%/${MPARTICLE}/g;s/%MH%/${MH}/g;" ${TEMP}/crab_step_6_to_7.py > crab_jobs/${MODEL}/crab_step_6_to_7.py
        cp ${TEMP}/PSet.py "crab_jobs/${MODEL}/PSet.py"
        cp ${TEMP}/crab_script_1_to_6.sh "crab_jobs/${MODEL}/crab_script_1_to_6.sh"
        cp NMSSMCascade_2_step_cfg.py  "crab_jobs/${MODEL}/NMSSMCascade_2_step_cfg.py"
        cp NMSSMCascade_3_step_cfg.py  "crab_jobs/${MODEL}/NMSSMCascade_3_step_cfg.py"
        cp NMSSMCascade_4_step_cfg.py  "crab_jobs/${MODEL}/NMSSMCascade_4_step_cfg.py"
        cp NMSSMCascade_5_step_cfg.py  "crab_jobs/${MODEL}/NMSSMCascade_5_step_cfg.py"
        cp NMSSMCascade_6_step_cfg.py  "crab_jobs/${MODEL}/NMSSMCascade_6_step_cfg.py"
        cp NMSSMCascade_7_step_cfg.py  "crab_jobs/${MODEL}/NMSSMCascade_7_step_cfg.py"

    done
done




	


