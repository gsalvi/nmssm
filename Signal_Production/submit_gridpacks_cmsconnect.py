# run from genproductions/bin/MadGraph5_aMCatNLO
# remember first to run 'voms-proxy-init -voms cms -valid 192:00'

import os
import time

for mSquark in range(800, 3400, 200):
  print("nohup ./submit_cmsconnect_gridpack_generation.sh NMSSMCascade_mSquark_%s nmssm/Signal_Production/jobs/NMSSMCascade_mSquark_%s &"%(mSquark, mSquark))
  os.system("nohup ./submit_cmsconnect_gridpack_generation.sh NMSSMCascade_mSquark_%s nmssm/Signal_Production/jobs/NMSSMCascade_mSquark_%s &"%(mSquark, mSquark))
  time.sleep(180)
  print("nohup ./submit_cmsconnect_gridpack_generation.sh NMSSMCascade_mSUSY_%s nmssm/Signal_Production/jobs/NMSSMCascade_mSUSY_%s &"%(mSquark, mSquark))
  os.system("nohup ./submit_cmsconnect_gridpack_generation.sh NMSSMCascade_mSUSY_%s nmssm/Signal_Production/jobs/NMSSMCascade_mSUSY_%s &"%(mSquark, mSquark))
  time.sleep(180)
