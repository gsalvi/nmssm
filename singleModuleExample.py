#!/usr/bin/env python3

# single-module version of analysisNMSSM.py to produce plots from input FakeFriends, e.g.
# python3 python/postprocessing/NMSSM/singleModuleExample.py -I /mercury/data2/linacre/NMSSM_FakeFriends/v2/out_postproc_v2_NMSSMCascade_mH-60_mSUSY_1200 --outputDir /scratch/xxt18833/out -t particleNet -y 2018 -p Retrained

import os
import argparse
import shutil
import re
import glob

from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.testAnalysis import TestAnalysis

# Argument parser
parser = argparse.ArgumentParser(description="Process some root file(s) and plot some hisograms to an output root file.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-i", "--input", default=None, nargs="+", help="Specifies the input file(s). Separate multiple root file inputs with a comma, or use a text file with the data file names. If no input files has been defined, search for files in inputDir (default = %(default)s)")
parser.add_argument("-I", "--inputDir", default=None, nargs="+", help="Directories to the input files. Will take all matching files in the specified folder as input. Cannot be used at the same time as --input. (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default=None, help="Name of output directory. Defaults to input directory plots folder.")
parser.add_argument("-n", "--maxEntries", default=None, type=int, help="Maximum number of events to process, per input file... (default = %(default)s)")
parser.add_argument("-y", "--year", type=str, help="The year of the input files, e.g. 2018 (no UL). Used for deciding what triggers to use.", required=True)
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, btagHbb and/or particleNet, or all for all implemented b-taggers. (default = %(default)s)")
parser.add_argument("-p", "--particleNetMassTraining", default="Retrained", help="Specify which particleNet mass regression to use ('Original', 'Retrained', 'Alternative'). (default = %(default)s)")
parser.add_argument("-g", "--grid", default=None, help="Specify which mass grid to use ('Old', 'New'). (default = %(default)s)")
parser.add_argument("--isData", action="store_true", help="If input are data. If not specified, then MC events are assumed.")
parser.add_argument("--debug", action="store_true", help="Plot and print additional debug information. Can use a lot of memory. (default = %(default)s)")
args = parser.parse_args()


# Input files to run over
# Only allow either --inputDir or --input as it will crash if we would have complete paths in the --input...
if args.inputDir and args.input:
    raise Exception("Only use either --inputDir or --input.")

if args.input is None: # Look for files in input directory
    inputs = []
    for input_dir in args.inputDir:
        inputs += [input_dir+"/"+f for f in os.listdir(input_dir) if (os.path.isfile(input_dir+"/"+f) and re.search("_Skim_?.*\.root",f))]
elif ".root" not in args.input[0]: # Assume it's a text file that lists the input files. Currently only works for one input text file.
    with open(args.input[0], "r", encoding="utf-8") as file:
        inputs = file.read().splitlines()
elif args.inputDir is None:
    inputs = args.input
else:
    inputs = args.input

# Check that we have inputs
if not inputs:
    raise FileNotFoundError("No inputs files were found.")
for file in inputs:
    # File does not exist locally or is not remote from xrootd
    if not os.path.exists(file) and not file.startswith("root:"):
        raise FileNotFoundError("Input file does not exist: %s" % file)

# Create output directory
if not args.outputDir and not args.inputDir:
    args.outputDir = "./results/"
elif not args.outputDir:
    args.outputDir = args.inputDir[0] # Put the results in the first specified input directory for now...
if args.outputDir[-1] != "/": # Add slash if there is none
    args.outputDir += "/"
plot_dir = args.outputDir + "/plots/" # The output folder for plots
if os.path.exists(plot_dir): # Delete an existing plots folder, needed due to write permissions
    shutil.rmtree(plot_dir)
os.makedirs(plot_dir)

# Remove any leftover .dat files
for filename in glob.glob(args.outputDir+"*.dat"):
    os.remove(filename)

# Check that the taggers exist
btagger_list = ["particleNet", "btagHbb"]
if "all" in args.taggers:
    args.taggers = btagger_list
else:
    for t in args.taggers:
        if t not in btagger_list:
            raise Exception("The b-tagger(s) are not defined: %s" % t)

particleNet_list = ["Original", "Retrained", "Alternative"]
if args.particleNetMassTraining not in particleNet_list:
    raise Exception("The particleNet training '%s' is not defined" % args.particleNetMassTraining)

# Remove any potential UL in year
args.year = int(args.year.replace("UL", ""))

# Set luminosity https://twiki.cern.ch/twiki/bin/viewauth/CMS/LumiRecommendationsRun2
if args.year == 2018:
    lumi = 59830 # pb-1, including golden JSON
elif args.year == 2017:
    lumi = 41480 # pb-1, including golden JSON
elif args.year == 2016:
    lumi = 36310 # pb-1, including golden JSON
elif args.year ==2015:
    lumi = 2270 # pb-1, including golden JSON
else:
    raise ValueError("Year not defined: %s" % args.year)

# Check mass grid
if args.grid is None:
    new_mass_grid = args.grid
elif args.grid == "Old":
    new_mass_grid = False
elif args.grid == "New":
    new_mass_grid = False # Change to true once fixed condor submit
else:
    raise ValueError("Mass grid not defined: %s" % args.grid)

postfix = "_Friend_"

################################
#         Particle Net         #
################################

# Run analysis using particleNet b-tagging
if "particleNet" in args.taggers:

    # Some b-tagger specific constants
    tagger = "particleNet"
    postfix_tmp = postfix + tagger

    # Create a chain of modules to run for the analysis
    analysis_chain_particleNet = []

    # Add TestAnalysis module (only needed for checking the output)
    analysis_chain_particleNet.append(TestAnalysis(isData=args.isData, debug=args.debug, btag=tagger, massTraining=args.particleNetMassTraining, new_mass_grid=new_mass_grid, output_dir=plot_dir))

    # Create post processor and run it
    p_particleNet = PostProcessor(args.outputDir, inputFiles=inputs,
                      modules=analysis_chain_particleNet,
                      maxEntries=args.maxEntries,
                      friend=True,
                      postfix=postfix_tmp,
                      )
    p_particleNet.run()

################################
#            btagHbb           #
################################

# Run analysis using the old btagHbb
if "btagHbb" in args.taggers:

    # Some b-tagger specific constants
    tagger = "btagHbb"
    postfix_tmp = postfix + tagger

    # Create a chain of modules to run for the analysis
    analysis_chain_btagHbb = []

    # Add TestAnalysis module (only needed for checking the output)
    analysis_chain_btagHbb.append(TestAnalysis(isData=args.isData, debug=args.debug, btag=tagger, massTraining="", new_mass_grid=new_mass_grid, output_dir=plot_dir))

    # Create post processor and run it
    p_btagHbb = PostProcessor(args.outputDir, inputFiles=inputs,
                      modules=analysis_chain_btagHbb,
                      maxEntries=args.maxEntries,
                      friend=True,
                      postfix=postfix_tmp,
                      )
    p_btagHbb.run()
