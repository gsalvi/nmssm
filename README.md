# NMSSM

A Next-to-Minimal Supersymmetric Standard Model (NMSSM) analysis for CMS Run 3. This analysis aims to search for light Higgs bosons...

## Contents of Repository

- `postProcNMSSM.py` calculates new variables needed for the analysis and adds them to a new output root file.
- `analysisNMSSM.py` uses the output root file from the above script to make some histograms (e.g. HT, jet energy scale systematics, b-tag correlation...). _Note: currently it creates a Friend tree as I couldn't figure out an easier way for modules in the chain to access the same values._
- `singleModuleExample.py` single-module version of analysisNMSSM.py to quickly run ABCD method or make plots from input FakeFriends.
- `requirements.txt` list of packages and version number that are required.
- `QCUT_study` and `Signal_Production` stand-alone qCut study and configs for signal MC production.

#### Modules

The `modules/` folder contains processing modules needed for the analysis that is not part of the NanoAOD-Tools:

- `basicHistograms.py` creates simple histograms from the input files.
- `btagCorrelationCheck.py` creates some plots that compares the correlation between the chosen b-tagger and mass. Also prints out the b-tagging efficiency values for different b-tag cuts to a `.dat` file in the output folder. These values can be used to plot ROC curves using `/utilities/plotROC.py`.
- `countLeptonicW.py` Count number of leptonic W boson decays and adds the count to a branch.
- `crossSectionScales.py` adds a branch with the cross-section scales to the output `root` file. Values are taken from the [summary table of samples](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns).
- `doublebtagSF.py` add branches with the double b-tag systematics.
- `fatJetSelection.py` chooses the two FatJets (AK8) with the largest b-tag discriminator values that passes the kinematic event selection and adds their indices to the output tree as FatJetIndexA and FatJetIndexB. The two indices are not sorted in any way, i.e. which FatJet gets to be A and B is randomised.
- `jetHT.py` computes HT using AK4 jets and adds them to the output tree. Default pT and eta cuts can be changed, as well as which pT branch to use. Optional HT skim by setting `ht_cut`.
- `jetMassUncertaintes.py` adds branches with the corrected fatjet masses and uncertainties (JMR and JMS).
- `kinematicEventSelection.py` checks that the two chosen fatjets passes the kinematic selection (pT, eta, etc.)
- `massEventSelection.py` uses the two FatJets chosen by the kinematic event selection to count the number events that are located in signal and sideband regions. These numbers are then used for the data driven QCD estimation.
- `particleNetTagger.py` adds a branch with the computed b-tag discriminator using ParticleNet.
- `roc.py` writes values for creating ROC curves after the mass event selection (in the same `.dat` as `btagCorrelationCheck.py`).
- `testAnalysis.py` makes some random HT histograms.
- `triggerSelection.py` select events depending on a list of triggers.
- `weightsMC.py` computes the final weight of each MC event, using the integrated luminosity, generator weight, and cross-section scales. The weight is saved as the branch `weights_mc`.

#### Utilities

The `utilities/` folder contains various helper functions:

- `fileReadUtils.py` contains functions for reading files and creating histograms.
- `nanoAODTools_python3_debug.sh` edits some of the nanoAOD-tools files to fix bugs when going from `python2` to `python3`.
- `plotDatasetComparison.py` a stand-alone plotting script for creating a stacked histogram of specific branches. Each dataset within the input folder get a separate colour.
- `plotROC.py` a stand-alone ROC curve plotting script that takes the b-tag efficiency `.dat` files from running the analysis as inputs. See "Plot ROC Curves" section for further details.
- `plotSignalBackgroundDataHistograms.py` a stand-alone plotting script for creating a stacked histogram of the search region bins for signal, background, and/or data samples.
- `plotUtils.py` contains functions for plotting histograms.
- `plot_Ratio.py` is a standalone utility for making comparison plots between two samples, including a ratio panel. So far it's only been used for validating the new signal samples against those used in the published analysis. The locations of the input root files are currently [hard-coded](https://gitlab.cern.ch/gsalvi/nmssm/-/blob/master/utilities/plot_Ratio.py#L88). The `--noEventSelection` flag should be used when running `analysisNMSSM.py` to produce the input root files.
- `submitCondorJobs.py` is a python script that submits jobs to HTCondor.
- `crab/` folder containing scripts for running the postprocessing step using CRAB. See intructions below.
- `Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt` the Golden JSON file for analysing data.

## Checkout Instructions

Start by checking out CMSSW and nanoAOD-tools, according to the [nanoAOD-tools repository](https://github.com/cms-nanoAOD/nanoAOD-tools):

    cmsrel CMSSW_12_3_0_pre2
    cd CMSSW_12_3_0_pre2/src
    cmsenv
    git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools
    cd PhysicsTools/NanoAODTools

Now clone the NMSSM analysis repository and compile:

    git clone https://gitlab.cern.ch/gsalvi/nmssm.git python/postprocessing/NMSSM
    scram b

Unclear what difference the CMSSW release version makes when using NanoAOD-Tools, but the later `CMSSW_10_X_Y`, `CMSSW_11_X_Y` and `CMSSW_12_X_Y` with `python3` does not seem to work without some changes done by the following `bash` script:

    ./python/postprocessing/NMSSM/utilities/nanoAODTools_python3_debug.sh

Install the corret package version:

    pip3 install -r python/postprocessing/NMSSM/requirements.txt --user

Make sure there is a proxy to access files on the grid:

    voms-proxy-init -rfc --voms cms --valid 192:00


### Updating repository

If the NMSSM repository needs updating, make sure to change to the 'NMSSM' folder before you pull. For example:

    cd python/postprocessing/NMSSM
    git pull
    cd -


## Run the Analysis

    # Run the post processing
    python3 python/postprocessing/NMSSM/postProcNMSSM.py -i python/postprocessing/NMSSM/test/test_qcd_background_files.txt -O out_post_proc -n 1000 -y UL2018

    # Run the analysis
    python3 python/postprocessing/NMSSM/analysisNMSSM.py --inputDir out_post_proc -t all -y 2018 -p Original

This runs the post-processing directly on the machine you are on, which is okay for a couple of thousand events while doing tests. However, for large datasets it is possible to run the post-processing using HTCondor or CRAB, see below.

_Note: it is possible to run files from multiple datasets at the same time, however, the output file name will be wrong for the first post processing step. Unfortunately, no easy fix without changing NanoAODTools. For the analysis step there are no issues, thus one can stitch e.g. HT binned datasets together._

### Options for postProcNMSSM

- `--input` `-i` specify input files directly separated by commas or load a text file with the input files listed. If input files from the grid wants to be used, start the path with `root://cms-xrd-global.cern.ch//` (**Make sure there is a double-slash after cern.ch and the file name**).
- `--outputDir` `-O` specify the directory for the output files. Defaults to current directory.
- `--maxEvents` `-n` maximum number of events to process **per input file**.
- `--year` `-y` the run year of the input file. Normally specified in the file name.
- `--runPeriod` `-r` the run period of the input data, e.g. B. Not required for MC.
- `--jsonInput` `-j` specifies a JSON file for event selection. Defaults to None if processing MC, but defaults to the "Golden JSON" file stored in `utilities\Cert_314472-325175_13TeV_Legacy2018_Collisions18_JSON.txt`.
- `--isData` specified that input files are data samples. Default is False, but if file names contain `store/data/` it is automatically set to True.

Outputs one `*_Skim.root` file per input file.

### Options for analysisNMSSM

- `--input` `-i` specify input files directly separated by commas or load a text file with the input files listed.
- `--inputDir`, `-I` specify directories that contains all the `*_Skim.root` files from running `postProcNMSSM.py`.
- `--outputDir` `-O` specify the directory for the output files. Defaults to input directory `plots/` folder.
- `--maxEvents` `-n` maximum number of events to process **per input file**. Defaults to running over the complete input file(s), i.e. `-n -1`.
- `--year` `-y` The year of input files, e.g. 2018 (no UL), Used for deciding which triggers to use.
- `--taggers` `-t` specifies which b-taggers to run the analysis with (btagHbb or particleNet). `all` runs all b-taggers implemented.
- `--particleNetMassTraining` `-p` Specify which particleNet mass regression to use ('Original', 'Retrained', 'Alternative').
- `--HTcut` `-H` If set >0, apply HT cut at the given threshold (in GeV). Uses nominal HT only (not the systematic variations). Also applies loose version of KinematicEventSelection.
- `--isData` specified that input files are data samples. Default is False.
- `--noEventSelection` runs a seperate analysis without any cuts and event selection. Outputs a friend tree `*_Friend_*.root` with the same number of events as the original input. This option does not shuffle the two chosen FatJets, thus FatJetA will always be the FatJet with the largest b-tag discriminator value.
- `--debug` plots and prints additional debug information. Note: can use a lot of memory for big inputs.

Outputs Friend/FakeFriend `.root` files containing values needed during the run but not (necessarily) afterwards in the specified output directory. Note that the FakeFriend tree only includes the events that passed all the cuts, thus might not contain the same number of events as the original input root file due to limitations with NanoAODTools. It does however, include the branches from the original tree in contrast to the Friend tree. To avoid cuts, run with the `--noEventSelection` flag which creates the Friend output. 

Also outputs histograms and plots in the directory from where the script was run.

## Run with HTCondor

For larger datasets it is better to submit jobs to HTCondor. Using the same arguments as above, but with additional `--runPostProc` and/or `--runAnalysis`:

    # First update grid certificate proxy
    voms-proxy-init -rfc --voms cms --valid 192:00

    # To run post processing only
    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -i python/postprocessing/NMSSM/test/test_qcd_background_files.txt -O out_post_proc -n 1000 -y UL2018 --runPostProc

    # To run analysis only
    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -I out_post_proc_* -O out_analysis -n 1000 -t all -y 2018 --runAnalysis

    # To run both post processing and analysis
    python3 python/postprocessing/NMSSM/utilities/submitCondorJobs.py -i python/postprocessing/NMSSM/test/test_qcd_background_files.txt -O out_post_proc -n 1000 -y UL2018 --runPostProc --runAnalysis

To avoid overwriting of existing folders, the condor script will add the date and time of the job submission at the end of the specified output directory name. If experiencing issues with condor, check "Known Issues" section further down.
_Note: Currently doesn't split the job up into multiple smaller jobs..._

## Run with CRAB

It is also possible to submit jobs using CRAB when processing very large datasets. To change configuration, e.g. datasets or number of jobs, edit `utilities/crab/crab_cfg.py` and then run the following command:

    crab submit -c python/postprocessing/NMSSM/utilities/crab/crab_cfg.py

This writes the output to RAL linux machines in `/pnfs/pp.rl.ac.uk/data/cms/store/user/<YOUR CERN USERNAME>`. Then run `analysisNMSSM.py` on the output from CRAB (locally?).

The file `crabPostProc.py` corresponds to the `postProcNMSSM.py`, but slightly modified.

_NOTE: To add more arguments for the_ 'crabPostProc.py' _script, change the scriptArgs variable. However, Crab doesn't like double dashes or spaces in script arguments thus add more arguments to the argument list using the format_ '-j=jsonFile'. _See_ 'pyCfgParams' _because the scripsArgs documentation is ass in_ [CRAB Configuration File Documentation](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile#CRAB_configuration_parameters).

## Signal postprocessing with CRAB

Multiple crab tasks for postprocessing signal samples can be generated using `source write_signal_postproc_crab_cfgs.sh`. The list of datasets is hardcoded and should be adjusted by editing lines 3-4. You'll probably also want to adjust things like the `output_tag` in the template crab config, `crab_cfg_signal_template.py`. Note, crab submission only works if the crab cfg is located at 'python/postprocessing/NMSSM/utilities/crab/crab_cfg.py', so the generated configs must each be copied there before submission (see the comment on L15 for an automatic way of doing this).


## Plot Search Region Bin Numbers

There is a stand-alone script to plot stacked histograms of search region (signal mass) bin numbers in the utilities folder: `utilities/plotROC.py`. The script uses the Friend root files created by `analysisNMSSM.py` as input. Note that this only works with MC samples, and that one has to run `analysisNMSSM.py` multiple times to create all the input files needed for the stack. 

    python3 python/postprocessing/NMSSM/utilities/plotSignalBackgroundDataHistograms.py -S signal_directory -s signal_name -B background_directory -b background_name

Multiple signals and backgrounds can be added, but the number of signal (background) names and signal (backround) directories must be the same. The signal and background names are used for the stack legend.

### Options for plotSignalBackgroundDataHistograms.py

    - `--signalDirs` `-S` the input directories with signal files.
    - `--signalNames` `-s` the names of the signal files, for plot legend.
    - `--backgroundDirs` `-B` the input directories with background files.
    - `--backgroundNames` `-b` the names of the background files, for plot legend.
    - `--dataDir` `-B` the input directory with data files.
    - `--dataName` `-b` the name of the data files, for plot legend.
    - `--outputDir` `-O` the output directory (has to already exist).
    - `--taggers` `-t` specify b-tagger to run on, either btagHbb or particleNet, or all for both
    - `--log` `-l` plots with a log scale on y-axis if used.
    - `--cutsHT` specify if HT cuts should be applied or not. A single value is used as a minimum HT cut, otherwise multiple plots are made with the HT ranges between the specified numbers.

## Plot ROC Curves

There is a stand-alone script to plot ROC curves of the b-tagging efficiency in the utilities folder: `utilities/plotROC.py`. The script uses the `btag_efficiency_*.dat` files created by `module/btagCorrelationCheck.py` as input, which contains the b-tag cuts with the corresponding values for the True Positive (TP) and False Positive (FP) numbers. Note that this only works with MC samples, and that one has to run `module/btagCorrelationCheck.py` (included when running `analysisNMSSM.py`) multiple times to create all the input files needed. I.e. one time with signal samples for the TP input, and one time for each background sample as the FP inputs.

The `btag_efficiency_*_1D.dat` contains the number of TP/FP for tagging every b-jet in an event, while `btag_efficiency_*_2D.dat` contains the number of TP/FP for tagging two true b-jets per event. The former also contains the total number of jets and b-jets, while the latter contains the total number of events.

    python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp <TP_FILE_PATH>/btag_efficiency_btagHbb_1D.dat -fp <FP_FILE_PATH>/btag_efficiency_btagHbb_1D.dat <ANOTHER_FP_FILE_PATH>/btag_efficiency_btagHbb_1D.dat -l ttbar QCD -o roc_btagHbb_1D.png

Note that the TP and FP values are in general only true for signal and background samples, respectively. For the signal sample, all b-jets are assumed to have come from a Higgs decay, and all b-jets in the background sample are assumed to be FP. Therefore, the real number of FP in background samples is actually the number of FP+TP.

### Options for plotROC.py

- `--inputTP` `-tp` specifies the TP input file path. Required.
- `--inputFP` `-fp` specifies the FP input file path(s). Multiple files can be specified separated by a space. Required.
- `--legend` `-l` specifies the description/label for each FP input file to be used in the plot legends. Each label should be separated with a space and written in the order of the FP input files. Required.
- `--output` `-o` name of output file containing the ROC curve plot.
- `--outputDir` `-O` specifies the directory for output files.
- `--massSelectROC` creates ROC curves using values after mass event selection.

## Known Issues

#### Condor job is terminated without log files

Sometimes the condor job is terminated before finishing, without any log files. This is usually due to the grid certificate has expired before the job finished, or that the memory usage was higher than what was requested in the `submitCondorJobs.py`. To see the information from the job (using the job ID which can be found in the zjob0.log`):

    condor_history -limit 1 <JOB_ID>

Or to print out only the reason for removal:

    condor_history -limit 1 <JOB_ID> -af RemoveReason

To check if it was due to memory usage:

    condor_history -limit 1 <JOB_ID> -af '(RemoteWallClockTime > 345600)' -af '(((RemoteSysCpu + RemoteUserCpu) / RequestCpus) > 345600)' -af '(ResidentSetSize / 10000 > RequestMemory)'

To check if it was because of proxy expiry:

    condor_history -limit 1 <JOB_ID> -af 'JobStartDate + RemoteWallClockTime' -af x509UserProxyExpiration

Sometimes condor claims the job was finished, even though it hasn't. Then try splitting the job up into smaller jobs if possible...

#### CRAB ImportError in job_out.txt

    ImportError: This package should not be accessible on Python 3. Either you are trying to run from the python-future src folder or your installation of python-future is corrupted.

After CRAB has run the script using `python3` and returned a successful error code, it tries to run an additional thing (unclear what) that calls a `python2` module which causes an error. As the job has already been marked as succesful, and written the output file, this error found in the jobs `job_out*.txt` log can be ignored.

#### CRAB tarball too big

If submitting to CRAB fails because the maximum tarball size exceeds 120 MB, a hack is to temporary move any unecessary files for the job. For example, the files/directories:

    NMSSM/test
    NMSSM/.git
    NMSSM/QCUT_study

IMPORTANT! Do NOT delete them.

TO DO: find a permanent solution... e.g. add the necessary folders manually in `crab_cfg.py` (and `crab_script.py`) instead of using `sendPythonFolder`?


#### Unknown branch Muon_isGlobal

    RuntimeError: Unknown branch Muon_isGlobal

Error caused by `isGlobal` branch not existing in the NanoAOD input file. Tends to happen more frequently if old NanoAOD files are used, try to use files created with NanoAOD version 5 (?) or higher.

## Datasets

1. [Jacob's Data and MC datasets](https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fphys03&input=dataset%3D%2F*%2Flinacre-UL2018_NANO_v3-00000000000000000000000000000000%2FUSER)
2. [Giovannas signal samples](https://cmsweb.cern.ch/das/request?view=list&limit=50&instance=prod%2Fphys03&input=dataset%3D%2F*%2Fgsalvi-UL2018_NANO_v4-00000000000000000000000000000000%2FUSER)

## Papers
1. [Light Higgs paper](http://cms.cern.ch/iCMS/analysisadmin/get?analysis=HIG-20-018-paper-v22.pdf)

## Useful Links

1. [NanoAOD-tools repository](https://github.com/cms-nanoAOD/nanoAOD-tools)
2. [NanoAOD-tools Twiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/NanoAODTools)
3. [List of NanoAOD file contents](https://cms-nanoaod-integration.web.cern.ch/integration/cms-swCMSSW_10_6_X/mc106Xul17v2_doc.html)
4. [Summary table of samples](https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns)
5. [CRAB Commands](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3Commands)
6. [CRAB Configuration File Documentation](https://twiki.cern.ch/twiki/bin/view/CMSPublic/CRAB3ConfigurationFile)
7. [General advice for analysing MC and data](https://twiki.cern.ch/twiki/bin/view/CMS/PdmV)
8. [Datasets for Analysis](https://twiki.cern.ch/twiki/bin/view/CMS/PdmVDatasetsUL2018)
9. [Golden JSON and other potentially useful stuff](https://twiki.cern.ch/twiki/bin/view/CMS/PdmVLegacy2018Analysis#Data_Certification)
10. [Official Integrated Luminosity](https://twiki.cern.ch/twiki/bin/edit/CMS/PdmVRun2LegacyAnalysis)
11. [List of triggers (Run 2)](https://twiki.cern.ch/twiki/bin/viewauth/CMS/HLTPathsRunIIList#2018)
12. [More information of trigger cuts etc.](https://hlt-config-editor-confdbv3.app.cern.ch/open?cfg=%2Fcdaq%2Fphysics%2FRun2018%2F2e34%2Fv3.6.1%2FHLT%2FV2&db=online)
