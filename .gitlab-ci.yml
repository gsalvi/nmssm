
# CI for NMSSM repository

# Order of job execution
stages:
  - build
  - pylint
  - post-processing
  - analysis
  - plot
  - stack-plot


# Global variables
variables:
  CMSSW_VERSION: "CMSSW_12_3_0_pre2"
  IMAGE: "gitlab-registry.cern.ch/ci-tools/ci-worker:cs8"


# Requirements for CMSSW setup
.cmssw_setup:
  image: $IMAGE
  tags:
    - cvmfs
  only:
    - merge_requests
    - master

# Unzips CMSSW tar file
.cmssw_unzip:
  variables:
    GIT_STRATEGY: none
  artifacts:
    when: on_success
    expire_in: 1 week
    paths:
      - results
  before_script:
    # Setup project
    - export PATH=$HOME/.local/bin:$PATH
    - source /cvmfs/cms.cern.ch/cmsset_default.sh
    - tar -xf ${CMSSW_VERSION}.tar.gz
    - cd ${CMSSW_VERSION}/src
    - cmsenv
    - cd PhysicsTools/NanoAODTools
    # Install packages (must be after cmsenv or it will use the wrong python etc)
    - yum install gcc-c++ -y
    - yum install zlib-devel -y
    - yum install libjpeg-devel -y
    - pip3 install -r ./python/postprocessing/NMSSM/requirements.txt
    # - pip3 install hist[plot] --user
    # - pip3 install hist[plot] --upgrade --user # Update hist package
    - pip3 show hist

# Run the build stage
# Compiles CMSSW and saves it as a tar file that can be used by later stages
# Maybe use a pre_clone_script runner in the future instead of moving the NMSSM folder...
build-job:
  stage: build
  extends:
    - .cmssw_setup
  artifacts:
    when: on_success
    expire_in: 1 week
    paths:
      - ${CMSSW_VERSION}.tar.gz
  before_script:
    - source /cvmfs/cms.cern.ch/cmsset_default.sh
    - mkdir ../NMSSM
    - mv ./* ../NMSSM/  # Move all contents of the repo to the NMSSM folder
  script:
    - cmsrel $CMSSW_VERSION
    - cd ${CMSSW_VERSION}/src
    - git clone https://github.com/cms-nanoAOD/nanoAOD-tools.git PhysicsTools/NanoAODTools
    - cd PhysicsTools/NanoAODTools
    - cmsenv
    - mv ${CI_PROJECT_DIR}/../NMSSM python/postprocessing/ # Move the NMSSM repo inside the NanoAODTools repo
    - ./python/postprocessing/NMSSM/utilities/nanoAODTools_python3_debug.sh
    - scram b
  after_script:
    - tar czf ${CMSSW_VERSION}.tar.gz ${CMSSW_VERSION}


# Check code quality using pylint
pylint-job:
  stage: pylint
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job"]
  variables:
    GIT_STRATEGY: none
  script:
    # Install pylint (don't put in a before_script, as only one before script can be defined, and cmssw_unzip has one)
    - pip3 install pylint --user
    # Run pylint 
    - cd python/postprocessing/NMSSM
    - pylint ../NMSSM/


# Run the post processing script
post-processing-job:
  stage: post-processing
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "pylint-job"]
  script:
    - python3 python/postprocessing/NMSSM/postProcNMSSM.py -y UL2018 -O ${CI_PROJECT_DIR}/results -n 500


# Run the analysis script
analysis-job:
  stage: analysis
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "post-processing-job"]
  script:
    - python3 python/postprocessing/NMSSM/analysisNMSSM.py --inputDir ${CI_PROJECT_DIR}/results -t all -y 2018 --debug

# Run the analysis script with the noEventSelection flag
analysis-no-event-selection-job:
  stage: analysis
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "post-processing-job"]
  script:
    - python3 python/postprocessing/NMSSM/analysisNMSSM.py --inputDir ${CI_PROJECT_DIR}/results -t all -y 2018 --noEventSelection

# Run ROC plot script
# Use the same data as input for both signal and background for simplicity
roc-plot-job:
  stage: plot
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "analysis-job"]
  script:
    - python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp ${CI_PROJECT_DIR}/results/btag_efficiency_btagHbb_1D.dat -fp ${CI_PROJECT_DIR}/results/btag_efficiency_btagHbb_1D.dat -l test_legend -o roc_btagHbb_test_1D.png -O ${CI_PROJECT_DIR}/results/plots
    - python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp ${CI_PROJECT_DIR}/results/btag_efficiency_btagHbb_2D.dat -fp ${CI_PROJECT_DIR}/results/btag_efficiency_btagHbb_2D.dat -l test_legend -o roc_btagHbb_test_2D.png -O ${CI_PROJECT_DIR}/results/plots
    - python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp ${CI_PROJECT_DIR}/results/btag_efficiency_particleNet_1D.dat -fp ${CI_PROJECT_DIR}/results/btag_efficiency_particleNet_1D.dat -l test_legend -o roc_particleNet_test_1D.png -O ${CI_PROJECT_DIR}/results/plots
    - python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp ${CI_PROJECT_DIR}/results/btag_efficiency_particleNet_2D.dat -fp ${CI_PROJECT_DIR}/results/btag_efficiency_particleNet_2D.dat -l test_legend -o roc_particleNet_test_2D.png -O ${CI_PROJECT_DIR}/results/plots

# Run massSelectROC plot script
# Use the same data as input for both signal (TP) and background (FP) for simplicity
mass-roc-plot-job:
  stage: plot
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "analysis-job"]
  script:
    - python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp ${CI_PROJECT_DIR}/results/btag_efficiency_btagHbb_2D.dat -fp ${CI_PROJECT_DIR}/results/btag_efficiency_btagHbb_2D.dat -l test_legend -o roc_btagHbb_test_2D.png -O ${CI_PROJECT_DIR}/results/plots --massSelectROC
    - python3 python/postprocessing/NMSSM/utilities/plotROC.py -tp ${CI_PROJECT_DIR}/results/btag_efficiency_particleNet_2D.dat -fp ${CI_PROJECT_DIR}/results/btag_efficiency_particleNet_2D.dat -l test_legend -o roc_particleNet_test_2D.png -O ${CI_PROJECT_DIR}/results/plots --massSelectROC

# Run SignalBackgroundData plot script
# Use the same data as input for both signal (TP) and background (FP) for simplicity
stack-plot-job:
  stage: plot
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "analysis-job"]
  script:
    - python3 python/postprocessing/NMSSM/utilities/plotSignalBackgroundDataHistograms.py -t all -s Signal -S ${CI_PROJECT_DIR}/results/ -b Background -B ${CI_PROJECT_DIR}/results/ -O ${CI_PROJECT_DIR}/results/plots

# Run dataset comparison script
dataset-comparison-plot-job:
  stage: plot
  extends:
    - .cmssw_setup
    - .cmssw_unzip
  needs: ["build-job", "analysis-job"]
  script:
    - python3 python/postprocessing/NMSSM/utilities/plotDatasetComparison.py -t all -I ${CI_PROJECT_DIR}/results/ -O ${CI_PROJECT_DIR}/results/plots