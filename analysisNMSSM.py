#!/usr/bin/env python3
import os
import argparse
import shutil
import re
import glob

from PhysicsTools.NanoAODTools.postprocessing.framework.postprocessor import PostProcessor
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.jetHT import JetHT
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.countLeptonicW import CountLeptonicW
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.btagCorrelationCheck import BTagCorrelationCheck
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.particleNetTagger import ParticleNetTagger
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.kinematicEventSelection import KinematicEventSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.massEventSelection import MassEventSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.weightsMC import WeightsMC
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.testAnalysis import TestAnalysis
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.roc import ROC
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.triggerSelection import TriggerSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.basicHistograms import BasicHistograms
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.fatJetSelection import FatJetSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.doublebtagSelection import DoublebtagSelection
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.doublebtagSF import DoublebtagSF
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.jetMassUncertainties import JetMassUncertainties


# Argument parser
parser = argparse.ArgumentParser(description="Process some root file(s) and plot some hisograms to an output root file.", formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument("-i", "--input", default=None, nargs="+", help="Specifies the input file(s). Separate multiple root file inputs with a space, or use a text file with the data file names. If no input files has been defined, search for files in inputDir (default = %(default)s)")
parser.add_argument("-I", "--inputDir", default=None, nargs="+", help="Directories to the input files. Will take all matching files in the specified folder as input. Cannot be used at the same time as --input. (default = %(default)s)")
parser.add_argument("-O", "--outputDir", default=None, help="Name of output directory. Defaults to input directory plots folder.")
parser.add_argument("-n", "--maxEntries", default=None, type=int, help="Maximum number of events to process, per input file... (default = %(default)s)")
parser.add_argument("-y", "--year", type=str, help="The year of the input files, e.g. 2018 (no UL). Used for deciding what triggers to use.", required=True)
parser.add_argument("-t", "--taggers", default=["particleNet"], nargs="+", help="Specify b-tagger, btagHbb and/or particleNet, or all for all implemented b-taggers. (default = %(default)s)")
parser.add_argument("-p", "--particleNetMassTraining", default="Retrained", help="Specify which particleNet mass regression to use for plots ('Original', 'Retrained', 'Alternative'). (default = %(default)s)")
parser.add_argument("-g", "--grid", default=None, help="Specify which mass grid to use ('Old', 'New'). (default = %(default)s)")
parser.add_argument("-H", "--HTcut", default=-1, type=int, help="HT threshold used for optional HT skim, in GeV. Uses nominal HT only (not the systematic variations). Also applies loose version of KinematicEventSelection. (default = %(default)s, suggested = 1200)")
parser.add_argument("--isData", action="store_true", help="If input are data. If not specified, then MC events are assumed.")
parser.add_argument("--noEventSelection", action="store_true", help="If analysis should be run without cuts applied, thus no event selection. Needed to create a Friend tree with the same number of events as the input tree.")
parser.add_argument("--debug", action="store_true", help="Plot and print additional debug information. Can use a lot of memory. (default = %(default)s)")
args = parser.parse_args()


# Check that we run only one special case
if args.isData + args.noEventSelection > 1:
    raise Exception("Only use at most one of the following options: --isData, or --noEventSelection.")

if (args.HTcut>0) + args.noEventSelection > 1:
    raise Exception("Only use at most one of the following options: --HTcut, or --noEventSelection.")

# Input files to run over
# Only allow either --inputDir or --input as it will crash if we would have complete paths in the --input...
if args.inputDir and args.input:
    raise Exception("Only use either --inputDir or --input.")

if args.input is None: # Look for files in input directory
    inputs = []
    for input_dir in args.inputDir:
        inputs += [input_dir+"/"+f for f in os.listdir(input_dir) if (os.path.isfile(input_dir+"/"+f) and re.search("_Skim_?\d*\.root",f))]
elif ".root" not in args.input[0]: # Assume it's a text file that lists the input files. Currently only works for one input text file.
    with open(args.input[0], "r", encoding="utf-8") as file:
        inputs = file.read().splitlines()
elif args.inputDir is None:
    inputs = args.input
else:
    inputs = args.input

# Check that we have inputs
if not inputs:
    raise FileNotFoundError("No inputs files were found.")
for file in inputs:
    # File does not exist locally or is not remote from xrootd
    if not os.path.exists(file) and not file.startswith("root:"):
        raise FileNotFoundError("Input file does not exist: %s" % file)

# Create output directory
if not args.outputDir and not args.inputDir:
    args.outputDir = "./results/"
elif not args.outputDir:
    args.outputDir = args.inputDir[0] # Put the results in the first specified input directory for now...
if args.outputDir[-1] != "/": # Add slash if there is none
    args.outputDir += "/"
plot_dir = args.outputDir + "/plots/" # The output folder for plots
if os.path.exists(plot_dir): # Delete an existing plots folder, needed due to write permissions
    shutil.rmtree(plot_dir)
os.makedirs(plot_dir)

# Remove any leftover .dat files
for filename in glob.glob(args.outputDir+"*.dat"):
    os.remove(filename)

# Check that the taggers exist
btagger_list = ["particleNet", "btagHbb"]
if "all" in args.taggers:
    args.taggers = btagger_list
else:
    for t in args.taggers:
        if t not in btagger_list:
            raise Exception("The b-tagger(s) are not defined: %s" % t)

particleNet_list = ["Original", "Retrained", "Alternative"]
if args.particleNetMassTraining not in particleNet_list:
    raise Exception("The particleNet training '%s' is not defined" % args.particleNetMassTraining)

# Remove any potential UL in year
args.year = int(args.year.replace("UL", ""))

# Set luminosity https://twiki.cern.ch/twiki/bin/viewauth/CMS/LumiRecommendationsRun2
if args.year == 2018:
    lumi = 59830 # pb-1, including golden JSON
elif args.year == 2017:
    lumi = 41480 # pb-1, including golden JSON
elif args.year == 2016:
    lumi = 36310 # pb-1, including golden JSON
elif args.year ==2015:
    lumi = 2270 # pb-1, including golden JSON
else:
    raise ValueError("Year not defined: %s" % args.year)

# Check mass grid
if args.grid is None:
    new_mass_grid = args.grid
elif args.grid == "Old":
    new_mass_grid = False
elif args.grid == "New":
    new_mass_grid = False # Change to true once fixed condor submit
else:
    raise ValueError("Mass grid not defined: %s" % args.grid)

# Some general constants used for all b-tagger
ak8_pt_cut = 300 # GeV. Minimum pt for AK8 jets
ak4_pt_cut = 300
ak8_eta_cut = 2.4
ak4_eta_cut = 2.4 if args.year == 2018 else 3.0 # Lower eta due to bad ECAL endcaps
deltaR_cut = 1.4
gen_ht_min = 0 # GeV. Minimum gen-level HT to process
gen_ht_max = float("inf") # GeV. Maximum gen-level HT to process
friend = False # No friend if we have cuts
postfix = "_FakeFriend_" # Output file name ending, a fake Friend tree containing cuts

# No event selection constants
if args.noEventSelection:
    ak8_pt_cut = 0
    ak4_pt_cut = 0
    ak8_eta_cut = float("inf")
    ak4_eta_cut = float("inf")
    deltaR_cut = 0
    gen_ht_min = 0 # GeV. Minimum gen-level HT to process
    gen_ht_max = float("inf") # GeV. Maximum gen-level HT to process
    friend = True
    postfix = "_Friend_" # A real Friend tree with the same number of events in input and output root file

# JME systematics added in the postprocessing step
JME_systs = ["jesTotalUp", "jesTotalDown", "jerUp", "jerDown"] if not args.isData else []

################################
#         Particle Net         #
################################

# Run analysis using particleNet b-tagging
if "particleNet" in args.taggers:

    # Some b-tagger specific constants
    tagger = "particleNet"
    mass_training = "" if args.particleNetMassTraining == "Original" else args.particleNetMassTraining
    postfix_tmp = postfix + tagger
    btag_cut = 0.15 # For one-dimensional b-tag correlation check
    btag_sum_cut = 1.95 # btagA + btagB > btag_sum_cut. 1.99 yields similar number of events in TR as btagHbb for mH 80 mSUSY 2800. 1.95 is chosen to give a reasonable efficiency for all signal points https://gitlab.cern.ch/gsalvi/nmssm/-/merge_requests/26#note_6575773
    btag_cr_cut = 0.6 # End of control region (and start of validation region)
    btag_vr_cut = 0.9 # End of validation region

    # Create a chain of modules to run for the analysis
    analysis_chain_particleNet = []

    # Add HT modules to analysis chain. Done first to save time when --HTcut is set (to perform an HT skim).
    analysis_chain_particleNet.append(JetHT(eta_cut=ak4_eta_cut, pt="pt_nom", ht_cut=args.HTcut, isData=args.isData))
    analysis_chain_particleNet.append(JetHT(eta_cut=ak4_eta_cut, gen_ht_min=gen_ht_min, gen_ht_max=gen_ht_max, isData=args.isData))
    if not args.isData:
        for s in JME_systs:
            analysis_chain_particleNet.append(JetHT(eta_cut=ak4_eta_cut, pt="pt_" + s))

    # Compute btag for particle net
    analysis_chain_particleNet.append(ParticleNetTagger())

    # Choose FatJets, no cuts just the two jets with the highest b-tag discriminators
    analysis_chain_particleNet.append(FatJetSelection(noEventSelection=args.noEventSelection, btag=tagger, skim=(args.HTcut>0)))

    # Add b-tag scale factors
    if not args.isData:
        analysis_chain_particleNet.append(DoublebtagSF(year=args.year))

    # Set weights for the run
    if not args.isData:
        analysis_chain_particleNet.append(WeightsMC(lumi=lumi, input_files=inputs, maxEntries=args.maxEntries, extra_weight=None, btag=tagger))

    # Add jet mass uncertainties
    if not args.isData:
        analysis_chain_particleNet.append(JetMassUncertainties(year=args.year))

    # Compute channel for ttbar events
    if not args.isData:
        analysis_chain_particleNet.append(CountLeptonicW())

    # Add module that plots some basic histograms from the input
    analysis_chain_particleNet.append(BasicHistograms(isData=args.isData, btag=tagger, massTraining=mass_training, output_dir=plot_dir))

    # Add trigger selection
    analysis_chain_particleNet.append(TriggerSelection(year=args.year, isData=args.isData))

    # Some quick btag-mass correlation check. Also removes signal region events if data sample (only needed for b-tag studies)
    analysis_chain_particleNet.append(BTagCorrelationCheck(isData=args.isData, ak8_pt_cut=ak8_pt_cut, btag=tagger, massTraining=mass_training, btag_cut=btag_cut, btag_sum_cut=btag_sum_cut, output_dir=plot_dir))

    # Add kinematic event selection
    analysis_chain_particleNet.append(KinematicEventSelection(isData=args.isData, btag=tagger, ak8_pt_cut=ak8_pt_cut, ak4_pt_cut=ak4_pt_cut, ak8_eta_cut=ak8_eta_cut, ak4_eta_cut=ak4_eta_cut, deltaR_cut=deltaR_cut, JME_systs=JME_systs, skim=(args.HTcut>0)))

    # Add double-b-tag selection
    analysis_chain_particleNet.append(DoublebtagSelection(isData=args.isData, btag=tagger, btag_sum_cut=btag_sum_cut, btag_cr_cut=btag_cr_cut, btag_vr_cut=btag_vr_cut))

    # Add mass event selection
    analysis_chain_particleNet.append(MassEventSelection(isData=args.isData, btag=tagger, new_mass_grid=new_mass_grid))

    # Add module which loops over btag sum cuts and checks efficiency, after mass event selection (only needed for b-tag studies)
    if not args.isData:
        analysis_chain_particleNet.append(ROC(output_dir=plot_dir, btag=tagger, massTraining=args.particleNetMassTraining))

    # Add TestAnalysis module (only needed for checking the output)
    analysis_chain_particleNet.append(TestAnalysis(isData=args.isData, debug=args.debug, btag=tagger, massTraining=args.particleNetMassTraining, new_mass_grid=new_mass_grid, output_dir=plot_dir))

    # Create post processor and run it
    p_particleNet = PostProcessor(args.outputDir, inputFiles=inputs,
                      modules=analysis_chain_particleNet,
                      maxEntries=args.maxEntries,
                      friend=friend,
                      postfix=postfix_tmp,
                      )
    p_particleNet.run()

    # Print out the cuts used for the run
    print("\nMinimum value for the b-tag: %f" % btag_cut)
    print("Tag region, minimum value of the b-tag sum: %f" % btag_sum_cut)
    print("Upper limit of the Control Region b-tag region: %f" % btag_cr_cut)
    print("Upper limit of the b-tag Validation Region (lower limit is the CR cut): %f\n" % btag_vr_cut)


################################
#            btagHbb           #
################################

# Run analysis using the old btagHbb
if "btagHbb" in args.taggers:

    # Some b-tagger specific constants
    tagger = "btagHbb"
    postfix_tmp = postfix + tagger
    btag_cut = 0.3 # For one-dimensional b-tag correlation check
    btag_sum_cut = 1.3 # btagA + btagB > btag_sum_cut
    btag_cr_cut = 0.3 # End of control region (and start of validation region)
    btag_vr_cut = 0.8 # End of validation region

    # Create a chain of modules to run for the analysis
    analysis_chain_btagHbb = []

    # Add HT modules to analysis chain. Done first to save time when --HTcut is set (to perform an HT skim).
    analysis_chain_btagHbb.append(JetHT(eta_cut=ak4_eta_cut, pt="pt_nom", ht_cut=args.HTcut, isData=args.isData))
    analysis_chain_btagHbb.append(JetHT(eta_cut=ak4_eta_cut, gen_ht_min=gen_ht_min, gen_ht_max=gen_ht_max, isData=args.isData))
    if not args.isData:
        for s in JME_systs:
            analysis_chain_btagHbb.append(JetHT(eta_cut=ak4_eta_cut, pt="pt_" + s))

    # Compute btag for particle net (so it's there for comparison with btagHbb)
    analysis_chain_btagHbb.append(ParticleNetTagger())

    # Choose FatJets, no cuts just the two jets with the highest b-tag discriminators
    analysis_chain_btagHbb.append(FatJetSelection(noEventSelection=args.noEventSelection, btag=tagger, skim=(args.HTcut>0)))

    # Set weights for the run
    if not args.isData:
        analysis_chain_btagHbb.append(WeightsMC(lumi=lumi, input_files=inputs, maxEntries=args.maxEntries, extra_weight=None, btag=tagger))

    # Compute channel for ttbar events
    if not args.isData:
        analysis_chain_btagHbb.append(CountLeptonicW())

    # Add module that plots some basic histograms from the input
    analysis_chain_btagHbb.append(BasicHistograms(isData=args.isData, btag=tagger, output_dir=plot_dir))

    # Add trigger selection
    analysis_chain_btagHbb.append(TriggerSelection(year=args.year, isData=args.isData))

    # Some quick btag-mass correlation check. Also removes signal region events if data sample (only needed for b-tag studies)
    analysis_chain_btagHbb.append(BTagCorrelationCheck(isData=args.isData, ak8_pt_cut=ak8_pt_cut, btag=tagger, btag_cut=btag_cut, btag_sum_cut=btag_sum_cut, output_dir=plot_dir))

    # Add kinematic event selection
    analysis_chain_btagHbb.append(KinematicEventSelection(isData=args.isData, btag=tagger, ak8_pt_cut=ak8_pt_cut, ak4_pt_cut=ak4_pt_cut, ak8_eta_cut=ak8_eta_cut, ak4_eta_cut=ak4_eta_cut, deltaR_cut=deltaR_cut, JME_systs=JME_systs, skim=(args.HTcut>0)))

    # Add double-b-tag selection
    analysis_chain_btagHbb.append(DoublebtagSelection(isData=args.isData, btag=tagger, btag_sum_cut=btag_sum_cut, btag_cr_cut=btag_cr_cut, btag_vr_cut=btag_vr_cut))

    # Add mass event selection
    analysis_chain_btagHbb.append(MassEventSelection(isData=args.isData, btag=tagger, new_mass_grid=new_mass_grid))
    
    # Add module which loops over btag sum cuts and checks efficiency, after mass event selection (only needed for b-tag studies)
    if not args.isData:
        analysis_chain_btagHbb.append(ROC(output_dir=plot_dir, btag=tagger, massTraining=""))

    # Add TestAnalysis module (only needed for checking the output)
    analysis_chain_btagHbb.append(TestAnalysis(isData=args.isData, debug=args.debug, btag=tagger, massTraining="", new_mass_grid=new_mass_grid, output_dir=plot_dir))

    # Create post processor and run it
    p_btagHbb = PostProcessor(args.outputDir, inputFiles=inputs,
                      modules=analysis_chain_btagHbb,
                      maxEntries=args.maxEntries,
                      friend=friend,
                      postfix=postfix_tmp,
                      )
    p_btagHbb.run()

    # Print out the cuts used for the run
    print("\nMinimum value for the b-tag: %f" % btag_cut)
    print("Tag region, minimum value of the b-tag sum: %f" % btag_sum_cut)
    print("Upper limit of the Control Region b-tag region: %f" % btag_cr_cut)
    print("Upper limit of the b-tag Validation Region (lower limit is the CR cut): %f\n" % btag_vr_cut)
