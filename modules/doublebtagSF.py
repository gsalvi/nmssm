#!/usr/bin/env python3
import warnings
warnings.simplefilter("ignore")
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Adds branches with the double b-tag systematics for particleNet
class DoublebtagSF(Module):

    def __init__(self, year):
        self.year = year

    def beginJob(self):
        pass

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.input_file = inputFile.GetName()

        # Create branches that save the systematics for all events
        self.out.branch("FatJets_btagSF_particleNet_nominal", "F")
        self.out.branch("FatJets_btagSF_particleNet_up", "F")
        self.out.branch("FatJets_btagSF_particleNet_down", "F")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Return correction factors (nominal, up, down) for ParticleNet event weight
    # Values taken from: AN2020_201_v17.pdf https://cms.cern.ch/iCMS/analysisadmin/cadilines?line=B2G-21-001
    # TODO: rewrite this in a nicer way one day...
    def getSF(self, pt, btag):

        if self.year == 2018:

            if pt < 250: # No scale
                warnings.warn("Jet pT is too low: %f GeV" %pt)
                scale_factor = 1 # Nominal SF
                uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
                uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
            elif pt < 300:
                if btag < 0.8: # No scale
                    warnings.warn("ParticleNet tagger value is too low: %f" %btag)
                    scale_factor = 1 # Nominal SF
                    uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
                    uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
                elif btag < 0.9:
                    scale_factor = 1.136 # Nominal SF
                    uncert_up = [0.174, 0.198, 0.121] # Uncertainty up of SF
                    uncert_down = [0.177, 0.198, 0.121] # Uncertainty down of SF
                elif btag < 0.95:
                    scale_factor = 0.936 # Nominal SF
                    uncert_up = [0.123, 0.118, 0.110] # Uncertainty up of SF
                    uncert_down = [0.099, 0.118, 0.110] # Uncertainty down of SF
                elif btag < 0.98:
                    scale_factor = 0.930 # Nominal SF
                    uncert_up = [0.088, 0.102, 0.196] # Uncertainty up of SF
                    uncert_down = [0.085, 0.102, 0.196] # Uncertainty down of SF
                elif btag < 0.99:
                    scale_factor = 1.007 # Nominal SF
                    uncert_up = [0.126, 0.153, 0.0] # Uncertainty up of SF
                    uncert_down = [0.116, 0.153, 0.0] # Uncertainty down of SF
                else: # < 0.99
                    scale_factor = 1.204 # Nominal SF
                    uncert_up = [0.124, 0.152, 0.237] # Uncertainty up of SF
                    uncert_down = [0.112, 0.152, 0.237] # Uncertainty down of SF
            elif pt < 400:
                if btag < 0.8: # No scale
                    warnings.warn("ParticleNet tagger value is too low: %f" %btag)
                    scale_factor = 1 # Nominal SF
                    uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
                    uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
                elif btag < 0.9:
                    scale_factor = 1.022 # Nominal SF
                    uncert_up = [0.169, 0.291, 0.053] # Uncertainty up of SF
                    uncert_down = [0.154, 0.291, 0.053] # Uncertainty down of SF
                elif btag < 0.95:
                    scale_factor = 0.965 # Nominal SF
                    uncert_up = [0.102, 0.167, 0.091] # Uncertainty up of SF
                    uncert_down = [0.093, 0.167, 0.091] # Uncertainty down of SF
                elif btag < 0.98:
                    scale_factor = 0.884 # Nominal SF
                    uncert_up = [0.092, 0.101, 0.193] # Uncertainty up of SF
                    uncert_down = [0.078, 0.101, 0.193] # Uncertainty down of SF
                elif btag < 0.99:
                    scale_factor = 1.038 # Nominal SF
                    uncert_up = [0.133, 0.097, 0.175] # Uncertainty up of SF
                    uncert_down = [0.112, 0.097, 0.175] # Uncertainty down of SF
                else: # < 0.99
                    scale_factor = 1.180 # Nominal SF
                    uncert_up = [0.123, 0.138, 0.144] # Uncertainty up of SF
                    uncert_down = [0.107, 0.138, 0.144] # Uncertainty down of SF
            elif pt < 500:
                if btag < 0.8: # No scale
                    warnings.warn("ParticleNet tagger value is too low: %f" %btag)
                    scale_factor = 1 # Nominal SF
                    uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
                    uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
                elif btag < 0.9:
                    scale_factor = 1.217 # Nominal SF
                    uncert_up = [0.182, 0.375, 0.102] # Uncertainty up of SF
                    uncert_down = [0.169, 0.375, 0.102] # Uncertainty down of SF
                elif btag < 0.95:
                    scale_factor = 0.644 # Nominal SF
                    uncert_up = [0.213, 0.293, 0.093] # Uncertainty up of SF
                    uncert_down = [0.105, 0.293, 0.093] # Uncertainty down of SF
                elif btag < 0.98:
                    scale_factor = 0.821 # Nominal SF
                    uncert_up = [0.097, 0.164, 0.008] # Uncertainty up of SF
                    uncert_down = [0.088, 0.164, 0.008] # Uncertainty down of SF
                elif btag < 0.99:
                    scale_factor = 1.004 # Nominal SF
                    uncert_up = [0.141, 0.105, 0.121] # Uncertainty up of SF
                    uncert_down = [0.098, 0.105, 0.121] # Uncertainty down of SF
                else: # < 0.99
                    scale_factor = 1.131 # Nominal SF
                    uncert_up = [0.116, 0.139, 0.061] # Uncertainty up of SF
                    uncert_down = [0.109, 0.139, 0.061] # Uncertainty down of SF
            else: # > 500 GeV
                if btag < 0.8: # No scale
                    warnings.warn("ParticleNet tagger value is too low: %f" %btag)
                    scale_factor = 1 # Nominal SF
                    uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
                    uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
                elif btag < 0.9:
                    scale_factor = 0.931 # Nominal SF
                    uncert_up = [0.132, 0.431, 0.059] # Uncertainty up of SF
                    uncert_down = [0.123, 0.431, 0.059] # Uncertainty down of SF
                elif btag < 0.95:
                    scale_factor = 0.956 # Nominal SF
                    uncert_up = [0.099, 0.439, 0.064] # Uncertainty up of SF
                    uncert_down = [0.093, 0.439, 0.064] # Uncertainty down of SF
                elif btag < 0.98:
                    scale_factor = 0.914 # Nominal SF
                    uncert_up = [0.091, 0.207, 0.104] # Uncertainty up of SF
                    uncert_down = [0.084, 0.207, 0.104] # Uncertainty down of SF
                elif btag < 0.99:
                    scale_factor = 0.911 # Nominal SF
                    uncert_up = [0.115, 0.159, 0.080] # Uncertainty up of SF
                    uncert_down = [0.082, 0.159, 0.080] # Uncertainty down of SF
                else: # < 0.99
                    scale_factor = 1.125 # Nominal SF
                    uncert_up = [0.106, 0.311, 0.113] # Uncertainty up of SF
                    uncert_down = [0.099, 0.311, 0.113] # Uncertainty down of SF

        elif self.year == 2017:
            warnings.warn("Year not defined YET: %s" %self.year) # Values can be found in same analysis note as above
            # No scale
            scale_factor = 1 # Nominal SF
            uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
            uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
        elif self.year == 2016:
            warnings.warn("Year not defined YET: %s" %self.year) # Values can be found in same analysis note as above
            # No scale
            scale_factor = 1 # Nominal SF
            uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
            uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF
        else:
            warnings.warn("Year not defined: %s" %self.year)
            # No scale
            scale_factor = 1 # Nominal SF
            uncert_up = [0.0, 0.0, 0.0] # Uncertainty up of SF
            uncert_down = [0.0, 0.0, 0.0] # Uncertainty down of SF

        # Calculate total uncertaintity
        tot_uncert_up = sum(i*i for i in uncert_up)**(1/2)
        tot_uncert_down = sum(i*i for i in uncert_down)**(1/2)

        # Return SF and SF up/down
        return [scale_factor, scale_factor+tot_uncert_up, scale_factor-tot_uncert_down]

    # Run once for each event
    def analyze(self, event):

        if not "NMSSM" in self.input_file.split("FromDataset_")[-1]:
            # Fill the branches
            self.out.fillBranch("FatJets_btagSF_particleNet_nominal", 1.)
            self.out.fillBranch("FatJets_btagSF_particleNet_up", 1.)
            self.out.fillBranch("FatJets_btagSF_particleNet_down", 1.)
            return True

        # Get fatjet values
        fatjets = Collection(event, "FatJet") # AK8 jets
        try:
            fatjet_a = fatjets[getattr(event, "FatJetIndexA_particleNet")]
        except IndexError:
            fatjet_a = None
        try:
            fatjet_b = fatjets[getattr(event, "FatJetIndexB_particleNet")]
        except IndexError:
            fatjet_b = None

        # List of scale factor values
        scale_factors = []

        # Get correction factors for each of the jets
        for fatjet in [fatjet_a, fatjet_b]:

            if fatjet is None:
                scale_factors.append([1, 1, 1]) # No scale
                continue

            pt = getattr(fatjet, "pt_nom") # Jet pt
            btag = getattr(fatjet, "particleNet") # ParticleNet b-tag score

            # FatJet should be from b's
            if fatjet.hadronFlavour == 5:
                scale_factors.append(self.getSF(pt, btag)) # Returns list of nominal, uncertainty up, uncertainty down
            else:
                scale_factors.append([1, 1, 1]) # No scale

        # Calculate the total SF for both jets
        nominal = scale_factors[0][0] * scale_factors[1][0]
        up = scale_factors[0][1] * scale_factors[1][1]
        down = scale_factors[0][2] * scale_factors[1][2]

        # Fill the branches
        self.out.fillBranch("FatJets_btagSF_particleNet_nominal", nominal)
        self.out.fillBranch("FatJets_btagSF_particleNet_up", up)
        self.out.fillBranch("FatJets_btagSF_particleNet_down", down)

        return True
