#!/usr/bin/env python3

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Adds branch with the double-b-tag region for each event. Does not cut any events.
class DoublebtagSelection(Module):

    def __init__(self, btag="particleNet", isData=False, btag_sum_cut=1.3, btag_cr_cut=0.3, btag_vr_cut=0.8):

        self.isData = isData
        self.btag = btag
        self.btag_sum_cut = btag_sum_cut # Cut for btag region, the sum of the two fatjets b-tag value
        self.btag_cr_cut = btag_cr_cut # Cut for control region
        self.btag_vr_cut = btag_vr_cut # Upper cut for validation region (lower cut is the CR cut)

    def beginJob(self):

        # Count number of events passed
        self.nevents_passed_tr = 0

        # Set some variable depending on which b-tagger we're using
        if self.btag == "particleNet":
            self.btag_range = [0, 1]
        elif self.btag == "btagHbb":
            self.btag_range = [-1, 1]
        else:
            raise ValueError("Invalid b-tagger.")

    # Run once at the end
    def endJob(self):
        # Tag Region
        print("\nTotal number of all events in Tag Region (%s): %f" % (self.btag, self.nevents_passed_tr))

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        self.out.branch("FatJets_dbtRegion_%s" % (self.btag),"I") # Double b-tag region (TR = 1, VR = 2, CR = 3)

    def analyze(self, event):

        # Compute the event weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # Get the two chosen fatjets
        fatjets = Collection(event, "FatJet") # AK8 jets
        try:
            fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
            fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]
        except IndexError:
            self.out.fillBranch("FatJets_dbtRegion_%s" % (self.btag), -1)
            return True

        btag_a = getattr(fatjet_a, self.btag)
        btag_b = getattr(fatjet_b, self.btag)

        # Check that the fatjets are either in the btag or CR-btag region
        dbt_region = 0

        if btag_a + btag_b > self.btag_sum_cut:
            dbt_region = 1
            self.nevents_passed_tr += weight
        elif btag_a <= self.btag_cr_cut and btag_b <= self.btag_cr_cut:  # <= to avoid events "on the line" being assigned to neither cr_tag nor VR region
            dbt_region = 3
        elif (btag_a > self.btag_cr_cut and btag_a < self.btag_vr_cut) and (btag_b < (self.btag_range[0] + self.btag_vr_cut - self.btag_cr_cut)): # A square next to the CR-btag region
            dbt_region = 2
        elif (btag_b > self.btag_cr_cut and btag_b < self.btag_vr_cut) and (btag_a < (self.btag_range[0] + self.btag_vr_cut - self.btag_cr_cut)): # A square next to the CR-btag region
            dbt_region = 2

        self.out.fillBranch("FatJets_dbtRegion_%s" % (self.btag), dbt_region)

        return True