#!/usr/bin/env python3
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Saves the corresponding b-tagger values for particle net.
# Computed according to https://cms-nanoaod-integration.web.cern.ch/integration/cms-swCMSSW_10_6_X/mc106Xul17v2_doc.html#:~:text=FatJet_particleNetMD_Xbb
# Could be made general and handle Xbb and Xcc etc.
class ParticleNetTagger(Module):
    def __init__(self):
        self.lenVar = "nFatJet"

    def beginJob(self):
        pass

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        # Create branch that saves the cross section for all events
        self.out.branch("FatJet_particleNet", "F", lenVar=self.lenVar)

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        taggers = []
        fatjets = Collection(event, "FatJet") # AK8 jets

        for fatjet in fatjets:
            # Calculate the tagger values
            try:
                tagger = fatjet.particleNetMD_Xbb/(fatjet.particleNetMD_Xbb+fatjet.particleNetMD_QCD)
            except ZeroDivisionError:
                tagger = 0
            taggers.append(tagger)

        # Fill the branch
        self.out.fillBranch("FatJet_particleNet", taggers)

        return True
