#!/usr/bin/env python3
import random

import numpy as np
import matplotlib.pyplot as plt
import hist
from hist import Hist
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import hep,LLABEL,RLABEL,plot1DHist,plotRatio

# Section 6.2 in analysis note.
# Also compares the chosen fatjets with the MC truth to see if the two b-jets were chosen
class BTagCorrelationCheck(Module):
    def __init__(self, isData=False, ak8_pt_cut=300, ht_min=0, ht_max=None, btag="particleNet", massTraining="Retrained", btag_cut=0.3, btag_sum_cut=1.3, output_dir="./"):
        self.isData = isData
        self.ak8_pt_cut = ak8_pt_cut # GeV. Minimum jet pt for the chosen FatJets
        self.ht_min = ht_min # GeV. Minimum event HT
        self.ht_max = ht_max # GeV. NEVER USED??
        self.btag = btag # Which double b-tag discriminator to use
        self.massTraining = massTraining
        self.btag_cut = btag_cut # Some cut on the b-tag discriminator value...
        self.btag_sum_cut = btag_sum_cut # Cut for btag region, the sum of the two fatjets b-tag value
        self.output_dir = output_dir + "/" # Output directory for plots


    def beginJob(self):

        # Set some variable depending on which b-tagger we're using
        if self.btag == "particleNet":
            self.mass = self.mass = "particleNet_mass%s" % self.massTraining if self.isData else "particleNet_mass%s_corr" % self.massTraining
            self.mass_text = "ParticleNet"
            self.hist_range = [0, 1]
        elif self.btag == "btagHbb":
            self.mass = "msoftdrop"
            self.mass_text = "Softdrop"
            self.hist_range = [-1, 1]
        else:
            raise ValueError("Invalid b-tagger.")

        # Mass bin edges
        self.mass_bin_edges = [15, 25, 35, 45, 55]

        # Create a dictionary with one b-tag discriminator list per mass bin
        self.btag_discr_hist_dict = {}
        self.btag_discr_hist_dict2 = {}
        for i in range(len(self.mass_bin_edges) - 1):
            self.btag_discr_hist_dict[self.mass_bin_edges[i]] = Hist(hist.axis.Regular(bins=20, start=self.hist_range[0], stop=self.hist_range[1], name="x", label="FatJetA %s discriminator"%self.btag))
            # Same as above but with only two bins, smaller or larger than the btag_cut
            if self.hist_range[0] < self.btag_cut:
                self.btag_discr_hist_dict2[self.mass_bin_edges[i]] = Hist(hist.axis.Variable([self.hist_range[0], self.btag_cut, self.hist_range[1]], name="x", label="FatJetA %s discriminator"%self.btag))

        # Histogram of masses that passed the btag cut
        self.h_mass_pass = Hist(hist.axis.Regular(bins=20, start=0, stop=200, name="x", label="FatJetA %s mass [GeV]"%self.mass_text))
        # Histogram of masses that failed the btag cut
        self.h_mass_fail = Hist(hist.axis.Regular(bins=20, start=0, stop=200, name="x", label="FatJetA %s mass [GeV]"%self.mass_text))

        # Count the number of events
        self.nevents = 0

        # Count total number of b-jets in the events at different b-tag discriminator cuts
        self.nbtag_cuts = 100 # Number of btag cuts to test
        self.btag_cuts = np.linspace(-1, 1, self.nbtag_cuts) if self.btag == "btagHbb" else np.linspace(0, 1, self.nbtag_cuts) # List of all b-tag cuts to tests
        self.nJets_mc = 0 # Total number of jets in MC
        self.nbJets_mc = 0 # Total number of actual b-jets
        self.nbJets_tp_1d = np.zeros(self.nbtag_cuts) # Number of True Positive b-jets
        self.nbJets_fp_1d = np.zeros(self.nbtag_cuts) # Number of False Positive b-jets
        # Add stuff for c-jets?

        # Count total number of b-jets in the events at different b-tag discriminator cuts 2D
        self.nbtag_sum_cuts = 100 # Number of btag sum cuts to test
        self.btag_sum_cuts = np.linspace(-2, 2, self.nbtag_sum_cuts) if self.btag == "btagHbb" else np.linspace(0, 2, self.nbtag_sum_cuts) # List of all b-tag sum cuts to tests
        self.nbJets_tp_2d = np.zeros(self.nbtag_sum_cuts) # Number of True Positive b-jets (passed b-tag cut and has b parton flavour)
        self.nbJets_fp_2d = np.zeros(self.nbtag_sum_cuts) # Number of False Positive b-jets (passed b-tag cut and has non-b parton flavour)


    def endJob(self):

        # Number of correctly chosen b-jets
        if not self.isData:

            # Plot b-tag discriminator efficiencies
            fig, ax = plt.subplots()
            hep.cms.label(llabel=LLABEL, rlabel=RLABEL)
            plt.gca().set_aspect('auto', adjustable='box')
            plt.plot(self.btag_cuts, self.nbJets_tp_1d/self.nbJets_mc) # Only makes sense for signal...?
            plt.xlabel("%s cut" % (self.btag))
            plt.ylabel("Efficiency")
            plt.savefig(self.output_dir+"btag_vs_efficiency_%s.png" % (self.btag))
            plt.close()

            # Save 1D efficiencies to file
            file_1d = open(self.output_dir+"/../btag_efficiency_%s_1D.dat" % (self.btag), "w", encoding="utf-8")
            file_1d.write("btagCut nTruePositive nFalsePositive nJetsMC nbJetsMC \n")
            for i in range(self.nbtag_cuts):
                file_1d.write("%f %f %f %f %f\n" % (self.btag_cuts[i], self.nbJets_tp_1d[i], self.nbJets_fp_1d[i], self.nJets_mc, self.nbJets_mc))
            file_1d.close()

            # Save 2D efficiencies to file
            file_2d = open(self.output_dir+"/../btag_efficiency_%s_2D.dat" % (self.btag), "w", encoding="utf-8")
            file_2d.write("btagCut nTruePositive nFalsePositive nEvents\n")
            for i in range(self.nbtag_sum_cuts):
                file_2d.write("%f %f %f %f\n" % (self.btag_sum_cuts[i], self.nbJets_tp_2d[i], self.nbJets_fp_2d[i], self.nevents))
            file_2d.close()

        # Plot histograms binned in b-tag discriminator values
        legend_labels = [str(self.mass_bin_edges[i])+" < FatJetA "+self.mass_text+" Mass < "+str(self.mass_bin_edges[i+1])+" GeV" for i in range(len(self.mass_bin_edges)-1)]

        # Fraction of events per bin
        for key, h in self.btag_discr_hist_dict.items():
            if h.sum():
                self.btag_discr_hist_dict[key] = h/h.sum()

        plot1DHist(list(self.btag_discr_hist_dict.values()), ylabel="Fraction of events/bin", legend_labels=legend_labels, output_file=self.output_dir+"btag_vs_fractionOfEvents_%s.png"%self.btag)
        plot1DHist(list(self.btag_discr_hist_dict.values()), ylog=True, ylabel="Fraction of events/bin", legend_labels=legend_labels, output_file=self.output_dir+"btag_vs_fractionOfEvents_log_%s.png"%self.btag)

        # Fraction of events per bin
        if self.hist_range[0] < self.btag_cut:
            for key, h in self.btag_discr_hist_dict2.items():
                if h.sum():
                    self.btag_discr_hist_dict2[key] = h/h.sum()

            plot1DHist(list(self.btag_discr_hist_dict2.values()), ylabel="Fraction of events/bin", legend_labels=legend_labels, output_file=self.output_dir+"btag_vs_fractionOfEvents2_%s.png"%self.btag)
            plot1DHist(list(self.btag_discr_hist_dict2.values()), ylog=True, ylabel="Fraction of events/bin", legend_labels=legend_labels, output_file=self.output_dir+"btag_vs_fractionOfEvents2_log_%s.png"%self.btag)

        # Plot histograms binned in masses, unclear how they calculate it... probably wrong
        if self.h_mass_pass.sum() and self.h_mass_fail.sum():
            plotRatio(self.h_mass_pass/self.h_mass_pass.sum(), self.h_mass_fail/self.h_mass_fail.sum(), ylog=True, ylabel="Fraction of events/bin", num_label="Pass", denom_label="Fail", output_file=self.output_dir+"mass_vs_fractionOfEvents_"+self.btag+".png")

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Counts number of b-jets
    def bjetCounter(self, event):

        weight = event.weight_mc # Total weight
        fatjets = Collection(event, "FatJet") # Reconstructed AK8 jets

        # Count total number of fatjets
        self.nJets_mc += len(fatjets)*weight

        for fatjet in fatjets:

            # Get b-tag discriminator value
            btag_discr = getattr(fatjet, self.btag)

            # Get the type of particle the jet originated from
            # ValueError can happen when FatJet does not correspond to any generated jet
            # IndexError can happen due to some elements in the tail of the collection being dropped
            # More info: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD
            if not self.isData:

                genjets_ak8 = Collection(event, "GenJetAK8") # Generated AK8 jets

                try:
                    genjet_parton = genjets_ak8[fatjet.genJetAK8Idx].partonFlavour # Particle ID: https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
                except (IndexError, ValueError):
                    genjet_parton = 0

                # Count number of actual b-jets, assume that they are originating from Higgs decays
                if abs(genjet_parton) == 5:
                    self.nbJets_mc += weight

                # Count number of 1D True/False Positives for each b-tag discriminator value
                for i in range(self.nbtag_cuts - 1):
                    if btag_discr > self.btag_cuts[i] and fatjet.pt_nom > self.ak8_pt_cut: # The minimum b-tag discriminator value and pT we allow
                        if abs(genjet_parton) == 5:
                            self.nbJets_tp_1d[i] += weight
                        else:
                            self.nbJets_fp_1d[i] += weight

                # Check  btag discriminator vs parton flavour. For debug.
                # print(btag_discr, genjet_parton)

    # Counts number of b-jets in different tag regions
    def bjetCounter2D(self, event, fatjet_a, fatjet_b):

        weight = event.weight_mc # Total weight
        genjets_ak8 = Collection(event, "GenJetAK8") # Generated AK8 jets

        # Get the type of particle the jet originated from
        # ValueError can happen when FatJet does not correspond to any generated jet
        # IndexError can happen due to some elements in the tail of the collection being dropped
        # More info: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD
        try:
            fatjet_a_parton = genjets_ak8[fatjet_a.genJetAK8Idx].partonFlavour # Particle ID: https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
        except (IndexError, ValueError):
            fatjet_a_parton = 0
        try:
            fatjet_b_parton = genjets_ak8[fatjet_b.genJetAK8Idx].partonFlavour # Particle ID: https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
        except (IndexError, ValueError):
            fatjet_b_parton = 0

        # Loop over all sum cuts
        for i in range(self.nbtag_sum_cuts - 1):
            if getattr(fatjet_a, self.btag) + getattr(fatjet_b, self.btag) > self.btag_sum_cuts[i]:
                # Check if b quarks
                if abs(fatjet_a_parton) == 5 and abs(fatjet_b_parton) == 5:
                    self.nbJets_tp_2d[i] += weight
                else:
                    self.nbJets_fp_2d[i] += weight


    # Run once for each event. Never returns False as this is studying the data and not the actual physics analysis, thus don't want to stop processing the event.
    def analyze(self, event):

        # Check HT cut. Note: don't return False as this shouldn't interfere with modules further down the processing chain
        if event.HT_nom < self.ht_min:
            return True

        # Get the weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # Count number of events
        self.nevents += weight

        # Count number of b-jets for b-tagger efficiencies
        if not self.isData:
            self.bjetCounter(event)

        # Get the two chosen fatjets
        fatjets = Collection(event, "FatJet") # AK8 jets
        try:
            fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
            fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]
        except IndexError:
            return True

        # Count number of events passed b-tag sum cuts for efficiencies
        if not self.isData:
            self.bjetCounter2D(event, fatjet_a, fatjet_b)

        # Investigate events where one fatjet does not meet the b-tag cut
        if not getattr(fatjet_b, self.btag) < self.btag_cut:
            return True

        # Fill b-tag discriminator lists for the correct mass bin
        for i in range(len(self.mass_bin_edges) - 1):
            if getattr(fatjet_a, self.mass) > self.mass_bin_edges[i] and getattr(fatjet_a, self.mass) < self.mass_bin_edges[i+1]:
                self.btag_discr_hist_dict[self.mass_bin_edges[i]].fill(getattr(fatjet_a, self.btag), weight=weight)
                self.btag_discr_hist_dict2[self.mass_bin_edges[i]].fill(getattr(fatjet_a, self.btag), weight=weight)
                break

        # Fill the mass lists
        if getattr(fatjet_a, self.btag) < self.btag_cut:
            self.h_mass_fail.fill(getattr(fatjet_a, self.mass), weight=weight)
        else:
            self.h_mass_pass.fill(getattr(fatjet_a, self.mass), weight=weight)

        return True
