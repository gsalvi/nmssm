#!/usr/bin/env python3
import random

from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection

# If skim==True, returns False if the event does not have at least 2 FatJets
class FatJetSelection(Module):

    def __init__(self, noEventSelection=False, btag="particleNet", skim=False):
        self.noEventSelection = noEventSelection
        self.btag = btag
        self.skim = skim

    def beginJob(self):
        
        # Set random seed for reproducible results
        random.seed(27)   

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        # Create branches that saves the indices for the two FatJets
        self.out.branch("FatJetIndexA_%s" % (self.btag),"I")
        self.out.branch("FatJetIndexB_%s" % (self.btag),"I")


    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Returns a list of two FatJet objects and its index in the FatJet collection, highest b-tag discriminator value first.
    def chooseFatJets(self, event):

        fatjets = Collection(event, "FatJet") # AK8 jets

        fatjet_a, fatjet_b = None, None # The two chosen FatJets...
        index_a, index_b = -1, -1 # ... and their position in the FatJet Collection

        i = 0 # Index counter

        # Loop over all FatJets
        for fatjet in fatjets:
            # if not fatjet.pt_nom > 300: continue
            # if not abs(fatjet.eta) < 2.4: continue
            btag_discr = getattr(fatjet, self.btag)
            if fatjet_a is None:
                fatjet_a, index_a = fatjet, i
            elif btag_discr > getattr(fatjet_a, self.btag):
                fatjet_b, index_b = fatjet_a, index_a
                fatjet_a, index_a = fatjet, i
            elif fatjet_b is None:
                fatjet_b, index_b = fatjet, i
            elif btag_discr > getattr(fatjet_b, self.btag):
                fatjet_b, index_b = fatjet, i
            i += 1

        return [[fatjet_a, index_a], [fatjet_b, index_b]]

    # Run once for each event
    def analyze(self, event):

        # Choose FatJets corresponding to b-quark decay
        if self.noEventSelection:
            (fatjet_a, index_a), (fatjet_b, index_b) = self.chooseFatJets(event)
        else:
            (fatjet_a, index_a), (fatjet_b, index_b) = random.sample(self.chooseFatJets(event), 2) # Shuffle the FatJets so the largest b-tag discriminator isn't always first

        if self.skim and (index_a < 0 or index_b < 0):
            return False

        # Fill the branches with the FatJet index number
        self.out.fillBranch("FatJetIndexA_%s" % (self.btag), index_a)
        self.out.fillBranch("FatJetIndexB_%s" % (self.btag), index_b)

        return True
