#!/usr/bin/env python3
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module


# Module that computes HT using AK4 jets
# If HT cut is used, the event will stop processing if it is below/above the cuts
class JetHT(Module):

    def __init__(self, pt_cut=40, eta_cut=3.0, gen_ht_min=0, gen_ht_max=float("inf"), pt="pt", ht_cut=-1, isData=False):

        self.pt_cut = pt_cut # GeV. Minimum AK4 pt for calculating event HT
        self.eta_cut = eta_cut
        self.gen_ht_min = gen_ht_min # GeV. Minimum gen-level HT that is saved
        self.gen_ht_max = gen_ht_max # GeV. Maximum gen-level HT that is saved
        self.pt = pt # Which pt branch to use for computing HT
        self.ht_cut = ht_cut # GeV. Enable HT skim with the given threshold
        self.isData = isData 

    def beginJob(self):
        self.output_branch = "HT"+self.pt.replace("pt","") # Name of the output branch
        if self.output_branch == "HT": # all HT properties must have an underscore in the nanoAOD schema: https://coffeateam.github.io/coffea/api/coffea.nanoevents.NanoAODSchema.html
            self.output_branch = "HT_original"

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.out.branch(self.output_branch, "F") # Reco-level HT
        # Calculate gen-level HT if default pt branch
        if self.pt == "pt":
            self.out.branch("GenHT", "F")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        # Calculate gen-level HT if default pt branch
        if self.pt == "pt" and not self.isData:
            gen_jets = Collection(event, "GenJet")

            gen_ht = 0
            for j in gen_jets: # Sum the moomentum for all jets
                gen_ht += j.pt

            # Fill the branch
            if gen_ht >= self.gen_ht_min and gen_ht < self.gen_ht_max:
                self.out.fillBranch("GenHT", gen_ht)
            else:
                return False

        # Calculate reco-level HT
        jets = Collection(event, "Jet") # AK4 jets
        ht = 0

        for j in jets: # Sum the momentum for all jets
            if not getattr(j, self.pt) > self.pt_cut: continue
            if not abs(j.eta) < self.eta_cut: continue
            ht += getattr(j, self.pt)

        # HT skim
        if ht < self.ht_cut:
            return False

        # Fill the branch
        self.out.fillBranch(self.output_branch, ht)

        return True
