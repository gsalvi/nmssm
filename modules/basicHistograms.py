#!/usr/bin/env python3
import hist
from hist import Hist

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import plot1DHist,plot2DHist

# Creates basic histograms of pt, AK8 (FatJet) mass, HT etc.
class BasicHistograms(Module):
    def __init__(self, isData=False, btag="particleNet", massTraining="Retrained", ht_bin_edges=[1500, 2500, 3500], output_dir="./"):

        # Various constants and cuts
        self.isData = isData
        self.btag = btag # Which double b-tag discriminator to use. btagDDBvLV2 seems random
        self.massTraining = massTraining
        self.ht_bin_edges = ht_bin_edges # The left edge of each HT bin. Default 1500-2500, 2500-3500, 3500+ GeV
        self.output_dir = output_dir+"/" # Output directory for plots


    # Run once at the beginning of the job
    def beginJob(self):

        # Set some variable depending on which b-tagger we're using
        if self.btag == "btagHbb":
            self.mass = "msoftdrop"
            self.mass_text = "Softdrop" # For axis name
            self.btag_range = [-1, 1]
        elif self.btag == "particleNet":
            self.mass = self.mass = "particleNet_mass%s" % self.massTraining if self.isData else "particleNet_mass%s_corr" % self.massTraining
            self.mass_text = "ParticleNet" # For axis name
            self.btag_range = [0, 1]
        else:
            raise ValueError("Invalid b-tagger.")

        # HT with cross-section scaling
        self.h_reco_ht = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="Reco $H_{T}$ [GeV]"), storage=hist.storage.Weight())

        if not self.isData:
            # Gen HT
            self.h_gen_ht = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="Gen $H_{T}$ [GeV]"), storage=hist.storage.Weight())
            # HT JES up
            self.h_reco_ht_up = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="Reco $H_{T}$ [GeV]"), storage=hist.storage.Weight())
            # HT JES down
            self.h_reco_ht_down = Hist(hist.axis.Regular(bins=100, start=0, stop=10000, name="x", label="Reco $H_{T}$ [GeV]"), storage=hist.storage.Weight())


        # FatJet b-tag discriminator histograms
        self.h_btag = Hist(hist.axis.Regular(bins=100, start=self.btag_range[0], stop=self.btag_range[1], name="x", label="%s discriminator"%self.btag), storage=hist.storage.Weight())

        # FatJet mass
        self.h_mass = Hist(hist.axis.Regular(bins=140, start=0, stop=280, name="x", label="%s mass [GeV]"%self.mass_text), storage=hist.storage.Weight())

        # FatJet pT
        self.h_pt = Hist(hist.axis.Regular(bins=100, start=0, stop=2000, name="x", label="FatJet $p_{T}$ [GeV]"), storage=hist.storage.Weight())

        # FatJet eta
        self.h_eta = Hist(hist.axis.Regular(bins=100, start=-3, stop=3, name="x", label="FatJet $\eta$ [GeV]"), storage=hist.storage.Weight())

    # Run once when all files have been processed
    def endJob(self):

        # Plot HT histogram
        plot1DHist(self.h_reco_ht, ylabel="Events", ylog=True, output_file=self.output_dir+"ht_reco_%s.png" % self.btag)

        # Plot Gen HT and JES histogram comparison
        if not self.isData:
            plot1DHist(self.h_gen_ht, ylog=True, ylabel="Events", output_file=self.output_dir+"ht_gen_%s.png" % self.btag)
            plot1DHist([self.h_reco_ht, self.h_reco_ht_up, self.h_reco_ht_down], ylabel="Events", legend_labels=["Default", "JES Up", "JES Down"], output_file=self.output_dir+"ht_JES_%s.png" % self.btag)

        # Plot FatJet 1D histograms
        plot1DHist(self.h_btag, ylabel="Events", output_file=self.output_dir+"btag_%s.png" % (self.btag))
        plot1DHist(self.h_mass, ylabel="Events", output_file=self.output_dir+"%s.png" % (self.mass))
        plot1DHist(self.h_pt, ylabel="Events", output_file=self.output_dir+"pt_%s.png" % (self.btag))
        plot1DHist(self.h_eta, ylabel="Events", output_file=self.output_dir+"eta_%s.png" % (self.btag))

    # Main analysis code
    # Run once for every event, return True (go to next module) or False (fail, go to next event)
    def analyze(self, event):

        # Compute total weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # Fill HT histograms
        self.h_reco_ht.fill(event.HT_nom, weight=weight)
        if not self.isData:
            self.h_gen_ht.fill(event.GenHT, weight=weight)
            self.h_reco_ht_up.fill(event.HT_jesTotalUp, weight=weight)
            self.h_reco_ht_down.fill(event.HT_jesTotalDown, weight=weight)

        # Fill FatJet histograms
        fatjets = Collection(event, "FatJet") # AK8 jets
        for fatjet in fatjets:
            self.h_mass.fill(getattr(fatjet,self.mass), weight=weight)
            self.h_pt.fill(getattr(fatjet, "pt_nom"), weight=weight)
            self.h_eta.fill(getattr(fatjet, "eta"), weight=weight)
            self.h_btag.fill(getattr(fatjet, self.btag), weight=weight)

        return True
