#!/usr/bin/env python3
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Saves the total calculated MC weights in a branch,
# Put in the analysis chain (before any weights are used).
# A place-holder value of 1 fb^-1 is used as integrated luminosity, if not specified, to give the expected yield per fb^-1
class WeightsMC(Module):

    def __init__(self, lumi=1000, input_files=None, maxEntries=None, extra_weight=None, btag="particleNet"):
        self.lumi = lumi # pb^-1
        self.input_files = input_files
        self.maxEntries = maxEntries # Maximum number of entries to process PER input file
        self.extra_weight = extra_weight # Additional weight, e.g. if given xsec is slightly off
        self.btag = btag

    def beginJob(self):
        self.nevents_mc_dict = {} # Dictionary with the number of MC events. One entry per dataset (if stitching files together).

        # Sum the MC generator weight for all inputs used
        for f in self.input_files:
            # Open input file as dataframe
            rdf = ROOT.RDataFrame("Events", f)

            # Only process the first maxEntries entries
            if self.maxEntries and self.maxEntries < rdf.Count().GetValue():
                rdf = rdf.Range(self.maxEntries)

            # Count MC gen weight
            nevents_mc = rdf.Sum("genWeight").GetValue()

            # Save number of MC events in correct dataset key
            if "FromDataset" in f:
                dataset = f.split("FromDataset_")[-1].split("_Skim")[0]
                if dataset not in self.nevents_mc_dict:
                    self.nevents_mc_dict[dataset] = 0
                self.nevents_mc_dict[dataset] += nevents_mc
            else: # Only works if there is one type of unknown
                if "Unknown" not in self.nevents_mc_dict:
                    self.nevents_mc_dict["Unknown"] = 0
                self.nevents_mc_dict["Unknown"] += nevents_mc

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.input_file = inputFile.GetName()
        self.dataset = self.input_file.split("FromDataset_")[-1].split("_Skim")[0] if "FromDataset" in self.input_file else "Unknown" # Current dataset that is being processed

        # Create branch that saves the weights for all events
        self.out.branch("weight_mc", "F")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        # Is it computationally inefficient to include the luminosity scaling etc here?
        # But it makes the code in the other modules easier...

        gen_weight = event.genWeight # MC generator weight
        xsec = event.xsec # Cross section scale
        nevents_predicted = xsec * self.lumi # Predicted number of events
        xsec_weight = nevents_predicted / self.nevents_mc_dict[self.dataset] # Weight of the cross section
        weight_mc =  xsec_weight * gen_weight # Total MC weight

        # Additional weight, e.g. if given xsec is slightly off
        if self.extra_weight:
            weight_mc = weight_mc * self.extra_weight

        # Fill the branches
        self.out.fillBranch("weight_mc", weight_mc)

        return True
