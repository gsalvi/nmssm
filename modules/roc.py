#!/usr/bin/env python3
import random

import numpy as np
import matplotlib.pyplot as plt
import hist
from hist import Hist
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import *

# Create files with ROC curve values if the event is in a signal mass region
class ROC(Module):

    def __init__(self, btag="particleNet", massTraining="Retrained", output_dir="./"):
        self.btag = btag # Which double b-tag discriminator to use
        self.massTraining = massTraining
        self.output_dir = output_dir + "/" # Output directory for plots


    def beginJob(self):

        # Count total number of b-jets in the events at different b-tag discriminator cuts 2D
        self.nevents = 0 # Number of events in any of the mass signal bins
        self.nbtag_sum_cuts = 100 # Number of btag sum cuts to test
        self.btag_sum_cuts = np.linspace(-2, 2, self.nbtag_sum_cuts) if self.btag == "btagHbb" else np.linspace(0, 2, self.nbtag_sum_cuts) # List of all b-tag sum cuts to tests
        self.nbJets_tp_2d = np.zeros(self.nbtag_sum_cuts) # Number of True Positive b-jets (passed b-tag cut and has b parton flavour)
        self.nbJets_fp_2d = np.zeros(self.nbtag_sum_cuts) # Number of False Positive b-jets (passed b-tag cut and has non-b parton flavour)


    def endJob(self):

        # Open btag efficiency file in read mode
        file_2d = open(self.output_dir+"/../btag_efficiency_%s_2D.dat" % (self.btag), "r", encoding="utf-8")
        lines = file_2d.readlines()
        file_2d.close()

        # Open btag efficiency file in write mode
        file_2d = open(self.output_dir+"/../btag_efficiency_%s_2D.dat" % (self.btag), "w", encoding="utf-8")

        # Save 2D efficiencies to the end of each line in file
        for i, line in enumerate(lines):
            if i == 0:
                file_2d.write(line.strip("\n") + " nTruePositive_PostMass nFalsePositive_PostMass nEvents_PostMass\n")
            else:
                file_2d.write(line.strip("\n") + " %f %f %f\n" % (self.nbJets_tp_2d[i-1], self.nbJets_fp_2d[i-1], self.nevents))
        file_2d.close()


    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Counts number of b-jets in different tag regions
    def bjetCounter2D(self, event, fatjet_a, fatjet_b):

        weight = event.weight_mc # Total weight
        genjets_ak8 = Collection(event, "GenJetAK8") # Generated AK8 jets

        # Get the type of particle the jet originated from
        # ValueError can happen when FatJet does not correspond to any generated jet
        # IndexError can happen due to some elements in the tail of the collection being dropped
        # More info: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD
        try:
            fatjet_a_parton = genjets_ak8[fatjet_a.genJetAK8Idx].partonFlavour # Particle ID: https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
        except (IndexError, ValueError):
            fatjet_a_parton = 0
        try:
            fatjet_b_parton = genjets_ak8[fatjet_b.genJetAK8Idx].partonFlavour # Particle ID: https://pdg.lbl.gov/2007/reviews/montecarlorpp.pdf
        except (IndexError, ValueError):
            fatjet_b_parton = 0

        # Loop over all sum cuts
        for i in range(self.nbtag_sum_cuts - 1):
            if getattr(fatjet_a, self.btag) + getattr(fatjet_b, self.btag) > self.btag_sum_cuts[i]:
                # Check if b quarks
                if abs(fatjet_a_parton) == 5 and abs(fatjet_b_parton) == 5:
                    self.nbJets_tp_2d[i] += weight
                else:
                    self.nbJets_fp_2d[i] += weight

    # Run once for each event. Never returns False as this is studying the data and not the actual physics analysis, thus don't want to stop processing the event.
    def analyze(self, event):

        if getattr(event, "passPS_%s_nom" % (self.btag)):
            weight = event.weight_mc # Total weight

            # Get the two chosen fatjets
            fatjets = Collection(event, "FatJet") # AK8 jets
            try:
                fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
                fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]
            except (ValueError, IndexError):
                return True
            mass_sum_region = getattr(event, "FatJets_massSumRegion_%s%s_%s" % (self.btag, self.massTraining, 'nominal'))
            mass_difference_region = getattr(event, "FatJets_massDiffRegion_%s%s_%s" % (self.btag, self.massTraining, 'nominal'))

            if mass_sum_region and mass_difference_region != 1 and mass_difference_region != 5:

                # Count number of events in signal mass region
                self.nevents += weight

                self.bjetCounter2D(event, fatjet_a, fatjet_b)

        return True
