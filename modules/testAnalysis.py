#!/usr/bin/env python3

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
import hist
from hist import Hist

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import plot1DHist,plot2DHist
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.modules.massEventSelection import MassGrid

# Analysis module
# Creates histograms of HT etc.
class TestAnalysis(Module):
    def __init__(self, isData=False, debug=False, btag="particleNet", massTraining="Retrained", new_mass_grid=None, output_dir="./"):

        # Various constants and cuts
        self.isData = isData
        self.debug = debug
        self.btag = btag # Which double b-tag discriminator to use. btagDDBvLV2 seems random
        self.massTraining = massTraining
        self.new_mass_grid = new_mass_grid # Which mass grid to be used. True yields ParticleNet default.
        self.output_dir = output_dir + "/" # Output directory for plots 


    # Run once at the beginning of the job
    def beginJob(self):

        # Set some variable depending on which b-tagger we're using
        if self.btag == "particleNet":
            self.btag_range = [0, 1]
            t = self.massTraining.replace("Original", "")
            self.mass = "particleNet_mass%s" % t if self.isData else "particleNet_mass%s_corr" % t
            self.mass_text = "ParticleNet"
            self.mass_systs = ['nominal'] if self.isData else ['nominal', 'JMR_up', 'JMR_down', 'JMS_up', 'JMS_down']
            self.new_mass_grid = True if self.new_mass_grid is None else self.new_mass_grid
        elif self.btag == "btagHbb":
            self.btag_range = [-1, 1]
            self.mass = "msoftdrop"
            self.mass_text = "Softdrop"
            self.mass_systs = ['nominal']
            self.new_mass_grid = False if self.new_mass_grid is None else self.new_mass_grid
        else:
            raise ValueError("Invalid b-tagger.")
            
        # FatJet b-tag discriminator histogram
        self.btag_hist = Hist(hist.axis.Regular(bins=100, start=self.btag_range[0], stop=self.btag_range[1], name="x", label="FatJetA " + self.btag + " discriminator"),
                                                hist.axis.Regular(bins=100, start=self.btag_range[0], stop=self.btag_range[1], name="y", label="FatJetB " + self.btag + " discriminator"),
                                                storage=hist.storage.Weight()
                                            )

        # Instantiate mass grid
        self.mass_grid = MassGrid(self.new_mass_grid)

        # Keep track of number of events in signal/sideband regions

        columns=["R", "R_err", "S", "S_err", "T", "T_err", "U", "U_err", "D", "D_err", "R_CR", "R_CR_err", "S_CR", "S_CR_err", "T_CR", "T_CR_err", "U_CR", "U_CR_err", "D_CR", "D_CR_err", "R_VR", "R_VR_err", "S_VR", "S_VR_err", "T_VR", "T_VR_err", "U_VR", "U_VR_err", "D_VR", "D_VR_err", "R_pred", "R_pred_err", "S_pred", "S_pred_err", "T_pred", "T_pred_err", "R_vr_pred", "R_vr_pred_err", "S_vr_pred", "S_vr_pred_err", "T_vr_pred", "T_vr_pred_err", "F_R", "F_S", "F_T"]

        self.df = pd.DataFrame(np.zeros((self.mass_grid.num_signal_regions, len(columns))), columns=columns, index=range(1,self.mass_grid.num_signal_regions+1))

        # 2D mass histogram
        self.mass_hist = Hist(hist.axis.Regular(bins=self.mass_grid.hmax//2, start=0, stop=self.mass_grid.hmax, name="x", label="FatJetA %s mass [GeV]" % self.mass_text),
                                                hist.axis.Regular(bins=self.mass_grid.hmax//2, start=0, stop=self.mass_grid.hmax, name="y", label="FatJetB %s mass [GeV]" % self.mass_text),
                                                storage=hist.storage.Weight()
                                            )
        self.mass_hist_tr = Hist(hist.axis.Regular(bins=self.mass_grid.hmax//2, start=0, stop=self.mass_grid.hmax, name="x", label="FatJetA %s mass [GeV]" % self.mass_text),
                                                hist.axis.Regular(bins=self.mass_grid.hmax//2, start=0, stop=self.mass_grid.hmax, name="y", label="FatJetB %s mass [GeV]" % self.mass_text),
                                                storage=hist.storage.Weight()
                                            )
        self.mass_hist_cr = Hist(hist.axis.Regular(bins=self.mass_grid.hmax//2, start=0, stop=self.mass_grid.hmax, name="x", label="FatJetA %s mass [GeV]" % self.mass_text),
                                                hist.axis.Regular(bins=self.mass_grid.hmax//2, start=0, stop=self.mass_grid.hmax, name="y", label="FatJetB %s mass [GeV]" % self.mass_text),
                                                storage=hist.storage.Weight()
                                            )
        # For debug, create scatter plot
        if self.debug:
            fig, ax = plt.subplots(num="debug" + self.btag)


    # Run once when all files have been processed
    def endJob(self):

        # Check that histograms has entries
        if self.btag_hist.sum().value:
            # Plot 1D histogram
            plot1DHist(self.btag_hist.project("x"), ylabel="Events", output_file=self.output_dir+"btagA_%s.pdf" % (self.btag))
            plot1DHist(self.btag_hist.project("y"), ylabel="Events", output_file=self.output_dir+"btagB_%s.pdf" % (self.btag))

            # Plot 2D histogram, fraction of events per bin
            plot2DHist(self.btag_hist/self.btag_hist.sum().value, cbar_log=True, cbar_label="Fraction of events per bin", output_file=self.output_dir+"btagA_vs_btagB_%s.pdf" % (self.btag))
        else:
            print("Warning: B-tag histogram is empty.")

        # Compute predicted number of events in signal region
        for i in range(1, self.mass_grid.num_signal_regions+1):
            try:
                F_R = self.df.at[i, "R_CR"] / (self.df.at[i, "U_CR"] + self.df.at[i, "D_CR"])
                F_S = self.df.at[i, "S_CR"] / (self.df.at[i, "U_CR"] + self.df.at[i, "D_CR"])
                # Note, owing to the randomisation of FatJets A and B the R and T yields can be combined (like U + D) to give a combined F factor. This is not yet done here to leave open the option of not symmetrising.
                F_T = self.df.at[i, "T_CR"] / (self.df.at[i, "U_CR"] + self.df.at[i, "D_CR"])
            except ZeroDivisionError:
                F_R = 0
                F_S = 0
                F_T = 0

            U = self.df.at[i, "U"]
            D = self.df.at[i, "D"]
            U_vr = self.df.at[i, "U_VR"]
            D_vr = self.df.at[i, "D_VR"]

            self.df.at[i, "F_R"] = F_R
            self.df.at[i, "R_pred"] = F_R * (U + D)
            self.df.at[i, "R_vr_pred"] = F_R * (U_vr + D_vr)

            self.df.at[i, "F_S"] = F_S
            self.df.at[i, "S_pred"] = F_S * (U + D)
            self.df.at[i, "S_vr_pred"] = F_S * (U_vr + D_vr)

            self.df.at[i, "F_T"] = F_T
            self.df.at[i, "T_pred"] = F_T * (U + D)
            self.df.at[i, "T_vr_pred"] = F_T * (U_vr + D_vr)

            # Calculate errors
            self.df.at[i, "R_err"] = np.sqrt(self.df.at[i, "R_err"])
            self.df.at[i, "S_err"] = np.sqrt(self.df.at[i, "S_err"])
            self.df.at[i, "T_err"] = np.sqrt(self.df.at[i, "T_err"])
            self.df.at[i, "U_err"] = np.sqrt(self.df.at[i, "U_err"])
            self.df.at[i, "D_err"] = np.sqrt(self.df.at[i, "D_err"])

            self.df.at[i, "R_CR_err"] = np.sqrt(self.df.at[i, "R_CR_err"])
            self.df.at[i, "S_CR_err"] = np.sqrt(self.df.at[i, "S_CR_err"])
            self.df.at[i, "T_CR_err"] = np.sqrt(self.df.at[i, "T_CR_err"])
            self.df.at[i, "U_CR_err"] = np.sqrt(self.df.at[i, "U_CR_err"])
            self.df.at[i, "D_CR_err"] = np.sqrt(self.df.at[i, "D_CR_err"])

            self.df.at[i, "R_VR_err"] = np.sqrt(self.df.at[i, "R_VR_err"])
            self.df.at[i, "S_VR_err"] = np.sqrt(self.df.at[i, "S_VR_err"])
            self.df.at[i, "T_VR_err"] = np.sqrt(self.df.at[i, "T_VR_err"])
            self.df.at[i, "U_VR_err"] = np.sqrt(self.df.at[i, "U_VR_err"])
            self.df.at[i, "D_VR_err"] = np.sqrt(self.df.at[i, "D_VR_err"])

            # Assume statistical errors of F are 0
            err_ud = np.sqrt(self.df.at[i, "U_err"]**2 + self.df.at[i, "D_err"]**2) # Error of U + D in TR
            err_ud_cr = np.sqrt(self.df.at[i, "U_CR_err"]**2 + self.df.at[i, "D_CR_err"]**2) # Error of U + D in CR
            err_ud_vr = np.sqrt(self.df.at[i, "U_VR_err"]**2 + self.df.at[i, "D_VR_err"]**2) # Error of U + D in VR

            self.df.at[i, "R_pred_err"] = err_ud
            self.df.at[i, "S_pred_err"] = err_ud
            self.df.at[i, "T_pred_err"] = err_ud

            self.df.at[i, "R_vr_pred_err"] = err_ud_vr
            self.df.at[i, "S_vr_pred_err"] = err_ud_vr
            self.df.at[i, "T_vr_pred_err"] = err_ud_vr

        # Plot 2D mass histogram
        figure_name = "mass" + self.btag
        fig, ax = plt.subplots(num=figure_name)
        self.mass_grid.plotRegions(figure_name)
        plot2DHist(self.mass_hist, figure_name=figure_name, cbar_log=True, output_file=self.output_dir+"massA_vs_massB_%s.png" % (self.btag))
        # Tag region
        figure_name = "mass" + self.btag + "tr"
        fig, ax = plt.subplots(num=figure_name)
        self.mass_grid.plotRegions(figure_name)
        plot2DHist(self.mass_hist_tr, figure_name=figure_name, cbar_log=True, output_file=self.output_dir+"massA_vs_massB_TR_%s.png" % (self.btag))
        # CR-tag region
        figure_name = "mass" + self.btag + "cr-tr"
        fig, ax = plt.subplots(num=figure_name)
        self.mass_grid.plotRegions(figure_name)
        plot2DHist(self.mass_hist_cr, figure_name=figure_name, cbar_log=True, output_file=self.output_dir+"massA_vs_massB_cr-TR_%s.png" % (self.btag))

        # Plot the debug scatter plot
        if self.debug:
            figure_name = "debug" + self.btag
            self.mass_grid.plotRegions(figure_name)
            # Plot labels
            plt.xlabel("FatJetA %s mass [GeV]" % self.mass_text)
            plt.ylabel("FatJetB %s mass [GeV]" % self.mass_text)
            # Set style
            plt.xlim([0, 200])
            plt.ylim([0, 200])
            plt.gca().set_aspect('equal', adjustable='box') # Equal scale on x/y-axis
            # Save figure
            plt.savefig(self.output_dir+"debug_massA_vs_massB_%s.png" % (self.btag))
            plt.close()

        # For Debug
        pd.set_option('display.max_columns', None)
        pd.set_option('display.expand_frame_repr', False)
        print("\n\nNumber of events in signal and sideband regions, %s:\n" % (self.btag))
        print(self.df, "\n")
        print("Sum of all bins:")
        print(self.df.sum(axis = 0, skipna = True), "\n") # TODO Don't write out the errors


    # Main analysis code
    # Run once for every event, return True (go to next module) or False (fail, go to next event)
    def analyze(self, event):

        if getattr(event, "passPS_%s_nom" % (self.btag)):
            # Compute total weight
            if self.isData:
                weight = 1
            else:
                weight = event.weight_mc # Total weight

            fatjets = Collection(event, "FatJet") # AK8 jets

            # The two chosen FatJets corresponding to b-quark decays
            try:
                fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
                fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]
            except IndexError:
                return True

            syst = 'nominal' # TODO loop over all systematics?

            btag_a = getattr(fatjet_a, self.btag)
            btag_b = getattr(fatjet_b, self.btag)
            mass_a = getattr(fatjet_a, self.mass)
            mass_b = getattr(fatjet_b, self.mass)
            dbt_region = getattr(event, "FatJets_dbtRegion_%s" % (self.btag)) # Double b-tag region (TR = 1, VR = 2, CR = 3)
            mass_sum_region = getattr(event, "FatJets_massSumRegion_%s%s_%s" % (self.btag, self.massTraining, syst)) # Signal region or bin
            mass_difference_region = getattr(event, "FatJets_massDiffRegion_%s%s_%s" % (self.btag, self.massTraining, syst)) # Sideband region or bin

            # Fill histogram of the FatJet b-tag discriminator values
            self.btag_hist.fill(btag_a, btag_b, weight=weight)

            # Fill FatJet mass histograms
            self.mass_hist.fill(mass_a, mass_b, weight=weight)
            if dbt_region == 1:
                self.mass_hist_tr.fill(mass_a, mass_b, weight=weight)
            elif dbt_region == 3:
                self.mass_hist_cr.fill(mass_a, mass_b, weight=weight)

            # Choose the debug plot to plot in
            if self.debug:
                plt.figure("debug" + self.btag)

            # Factors used to chequer the mass regions in the debug plot
            c_S = 0.5**(mass_sum_region % 2)
            c_RT = 0.5**((1 + mass_sum_region) % 2)

            # Calculate the yields

            # Outside the area of interest
            if not mass_sum_region and not mass_sum_region and self.debug:
                    plt.plot(mass_a, mass_b, 'x', color=(0.5, 0.5, 0.5)) # For debug
            # Inside the D sideband
            elif mass_difference_region == 1:
                    if dbt_region == 1:
                        self.df.at[mass_sum_region, "D"] += weight
                        self.df.at[mass_sum_region, "D_err"] += weight**2
                        if self.debug:
                            plt.plot(mass_a, mass_b, 'o', color=(0, 0, c_S, mass_sum_region/self.mass_grid.num_signal_regions))
                    elif dbt_region == 3:
                        self.df.at[mass_sum_region, "D_CR"] += weight
                        self.df.at[mass_sum_region, "D_CR_err"] += weight**2
                        if self.debug:
                            plt.plot(mass_a, mass_b, 'o', color=(c_S, c_S, 0, mass_sum_region/self.mass_grid.num_signal_regions))
                    elif dbt_region == 2:
                        self.df.at[mass_sum_region, "D_VR"] += weight
                        self.df.at[mass_sum_region, "D_VR_err"] += weight**2
                        if self.debug:
                            plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, mass_sum_region/self.mass_grid.num_signal_regions))
            # Inside the lower part of the split signal region
            elif mass_difference_region == 2:
                if dbt_region == 1:
                    self.df.at[mass_sum_region, "R"] += weight
                    self.df.at[mass_sum_region, "R_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, c_RT, 0, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 3:
                    self.df.at[mass_sum_region, "R_CR"] += weight
                    self.df.at[mass_sum_region, "R_CR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(c_RT, 0, c_RT, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 2:
                    self.df.at[mass_sum_region, "R_VR"] += weight
                    self.df.at[mass_sum_region, "R_VR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, mass_sum_region/self.mass_grid.num_signal_regions))
            # Inside the central signal region
            elif mass_difference_region == 3:
                if dbt_region == 1:
                    self.df.at[mass_sum_region, "S"] += weight
                    self.df.at[mass_sum_region, "S_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, c_S, 0, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 3:
                    self.df.at[mass_sum_region, "S_CR"] += weight
                    self.df.at[mass_sum_region, "S_CR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(c_S, 0, c_S, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 2:
                    self.df.at[mass_sum_region, "S_VR"] += weight
                    self.df.at[mass_sum_region, "S_VR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, mass_sum_region/self.mass_grid.num_signal_regions))
            # Inside the upper split part
            elif mass_difference_region == 4:
                if dbt_region == 1:
                    self.df.at[mass_sum_region, "T"] += weight
                    self.df.at[mass_sum_region, "T_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, c_RT, 0, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 3:
                    self.df.at[mass_sum_region, "T_CR"] += weight
                    self.df.at[mass_sum_region, "T_CR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(c_RT, 0, c_RT, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 2:
                    self.df.at[mass_sum_region, "T_VR"] += weight
                    self.df.at[mass_sum_region, "T_VR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, mass_sum_region/self.mass_grid.num_signal_regions))
            # Inside the U sideband
            elif mass_difference_region == 5:
                if dbt_region == 1:
                    self.df.at[mass_sum_region, "U"] += weight
                    self.df.at[mass_sum_region, "U_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(c_S, 0, 0, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 3:
                    self.df.at[mass_sum_region, "U_CR"] += weight
                    self.df.at[mass_sum_region, "U_CR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, c_S, c_S, mass_sum_region/self.mass_grid.num_signal_regions))
                elif dbt_region == 2:
                    self.df.at[mass_sum_region, "U_VR"] += weight
                    self.df.at[mass_sum_region, "U_VR_err"] += weight**2
                    if self.debug:
                        plt.plot(mass_a, mass_b, 'o', color=(0, 0, 0, mass_sum_region/self.mass_grid.num_signal_regions))

        return True
