#!/usr/bin/env python3
import numpy as np
import matplotlib.pyplot as plt

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.NMSSM.utilities.plotUtils import plot2DHist


# Mass grid definition
class MassGrid():

    def __init__(self, new_mass_grid=None):

        self.new_mass_grid = new_mass_grid # Which mass grid to be used. True yields ParticleNet default.

        # Constants for mass event selection
        s1_lower_point_x_orig = 40 # x-coordinate for the lowest signal region point (S1) in the published analysis
        s1_lower_point_y_orig = 17.6 # y-coordinate
        signal1_h_orig = 20.64973221 # height of S1 in the published analysis
        signal1_w_orig = 25.55026779 # half-width (x-y) of S1 (at the top) in the published analysis
        signal_h_orig = 22.905585 # height of S2-S10 in the published analysis

        # Choose mass grid
        if self.new_mass_grid: # ParticleNet default
            self.num_signal_regions = 18
            self.hmax = 400 # GeV. Maximum mass histogram value
            self.signal_h = signal_h_orig # the vertical/horisontal distance between signal regions, except for S1
            self.signal1_h = self.signal_h # height of S1. Doesn't need to be smaller than signal_h when using particleNet (less QCD bkg).
            self.width_factor = 0.8 # Customise the width of the central band. Factor relative to the published analysis.
            self.split = 0.5 # Fractional position of the split in the central band (disabled if zero, as in the published analysis).
            self.sideband_area_ratio = (2. - self.width_factor) / self.width_factor # Area of the sidebands relative to central band. Calculated to keep the total grid size the same as in the published analysis.
            self.scaleHeightWithWidth = True # Recalculate S1 so that its height is the same as its average width (calculated such that the position and width of the "bottom line" of S1 is unchanged). Subsequent region heights then increase with width.
            self.width_scale = 1.5 # Requires scaleHeightWithWidth. Factor by which height is smaller than width (allows narrower binning in height because signal peak in general won't be in the bin centre, unlike for width).
            self.xplusy_intercept_shift = 40.  # Requires scaleHeightWithWidth. Change the value of (x+y) at which the grid has zero width (relative to the published analysis, where the value is -89.23 GeV). This makes the grid wider at larger mH.
        else: # btagHbb default
            self.num_signal_regions = 10
            self.hmax = 200 # GeV. Maximum mass histogram value
            self.signal_h = signal_h_orig # the vertical/horisontal distance between signal regions, except for S1
            self.signal1_h = signal1_h_orig # height of S1.
            self.width_factor = 1. # Customise the width of the central band. Factor relative to the published analysis.
            self.split = 0. # Fractional position of the split in the central band (disabled if zero, as in the published analysis).
            self.sideband_area_ratio = (2. - self.width_factor) / self.width_factor # Area of the sidebands relative to central band. Calculated to keep the total grid size the same as in the published analysis.
            self.scaleHeightWithWidth = False

        # Define mass grid lines for mass event selection

        # Adjust width of S1 (at the top)
        signal1_w = signal1_w_orig * self.width_factor

        # Adjust lower points of S1 to new width
        s1_lower_point_x_orig_h = s1_lower_point_x_orig + (self.width_factor - 1.) * (s1_lower_point_x_orig - s1_lower_point_y_orig) / 2.
        s1_lower_point_y_orig_h = s1_lower_point_y_orig - (self.width_factor - 1.) * (s1_lower_point_x_orig - s1_lower_point_y_orig) / 2.

        # Further adjust lower points to change S1 height from signal1_h_orig to self.signal1_h
        dW = (signal1_w - (s1_lower_point_x_orig - s1_lower_point_y_orig) * self.width_factor) * self.signal1_h / signal1_h_orig
        signal1_new_w_bottom =  signal1_w - dW
        self.s1_lower_point_x = (s1_lower_point_x_orig_h + s1_lower_point_y_orig_h + signal1_h_orig - self.signal1_h + signal1_new_w_bottom) / 2.
        self.s1_lower_point_y = (s1_lower_point_x_orig_h + s1_lower_point_y_orig_h + signal1_h_orig - self.signal1_h - signal1_new_w_bottom) / 2.

        if self.scaleHeightWithWidth:
            split_angle_tmp = np.arctan( dW * (1. - self.split) / self.signal1_h )
            if self.xplusy_intercept_shift:
                xplusy_intercept_orig =  (s1_lower_point_x_orig + s1_lower_point_y_orig) - (s1_lower_point_x_orig - s1_lower_point_y_orig) * signal1_h_orig / (signal1_w_orig - s1_lower_point_x_orig + s1_lower_point_y_orig)
                w1 = (self.s1_lower_point_x - self.s1_lower_point_y) * (1. - self.split)
                l1 = (self.s1_lower_point_x + self.s1_lower_point_y - xplusy_intercept_orig)
                a_orig = np.arctan( w1 / l1)
                a_new = np.arctan( w1 / (l1 - self.xplusy_intercept_shift) )
                split_angle_tmp = split_angle_tmp + a_new - a_orig
            tan_split_tmp = np.tan(split_angle_tmp) / self.width_scale
            self.signal1_h = 2. * (1. - self.split) * signal1_new_w_bottom / (1. - tan_split_tmp) / self.width_scale
            f = (1. + tan_split_tmp)/(1. - tan_split_tmp)
            signal1_w = signal1_new_w_bottom * f

        # Calculate S1 upper points
        self.s1_upper_point_x = (self.s1_lower_point_x + self.s1_lower_point_y + self.signal1_h + signal1_w) / 2. # 51.9
        self.s1_upper_point_y = (self.s1_lower_point_x + self.s1_lower_point_y + self.signal1_h - signal1_w) / 2. # 26.349732

        # Calculate the other region 1 upper and lower points
        self.sideband_lower_point_x = self.s1_upper_point_x + self.sideband_area_ratio * (self.s1_upper_point_x - self.s1_upper_point_y) / 2. # 64.6751 # x-coordinate for one point of the lower D line
        self.sideband_lower_point_y = self.s1_upper_point_y - self.sideband_area_ratio * (self.s1_upper_point_x - self.s1_upper_point_y) / 2. # 13.574602

        self.sideband_evenlower_point_x = self.s1_lower_point_x + self.sideband_area_ratio * (self.s1_lower_point_x - self.s1_lower_point_y) / 2. # 51.2 # x-coordinate for lowest point of the lower D line (ignoring the triangular adjustment for mass region 1)
        self.sideband_evenlower_point_y = self.s1_lower_point_y - self.sideband_area_ratio * (self.s1_lower_point_x - self.s1_lower_point_y) / 2. # 6.4

        self.split_lower_point_x = self.s1_upper_point_x - self.split * (self.s1_upper_point_x - self.s1_upper_point_y) / 2.
        self.split_lower_point_y = self.s1_upper_point_y + self.split * (self.s1_upper_point_x - self.s1_upper_point_y) / 2.

        self.split_evenlower_point_x = self.s1_lower_point_x - self.split * (self.s1_lower_point_x - self.s1_lower_point_y) / 2.
        self.split_evenlower_point_y = self.s1_lower_point_y + self.split * (self.s1_lower_point_x - self.s1_lower_point_y) / 2.

        self.signal_angle = -np.pi/4. + np.arctan( (self.s1_upper_point_x - self.s1_lower_point_x) / (self.s1_upper_point_y - self.s1_lower_point_y) ) # the angle from pi/4 (x=y) of the lines that correspond to the signal region
        self.sideband_angle = -np.pi/4. + np.arctan( (self.sideband_lower_point_x - self.sideband_evenlower_point_x) / (self.sideband_lower_point_y - self.sideband_evenlower_point_y) ) # the lines that correspond to the up/down background regions
        self.split_angle = -np.pi/4. + np.arctan( (self.split_lower_point_x - self.split_evenlower_point_x) / (self.split_lower_point_y - self.split_evenlower_point_y) ) # the lines that correspond to the split in the central band

        # The y(0) values for the negative lines of the mass regions
        self.mass_regions = [self.s1_lower_point_x + self.s1_lower_point_y] + [(self.s1_upper_point_x + self.s1_upper_point_y) + self.signal_h * n for n in range(self.num_signal_regions)]

        if self.scaleHeightWithWidth:
            tan_split_tmp = np.tan(self.split_angle) / self.width_scale
            f = (1. + tan_split_tmp)/(1. - tan_split_tmp)
            self.mass_regions[0] = self.s1_lower_point_x + self.s1_lower_point_y
            for n in range(self.num_signal_regions):
                self.mass_regions[n+1] = self.mass_regions[n] + self.signal1_h * np.power(f, n)         # height = width / width_scale
                # self.mass_regions[n+1] = self.mass_regions[n] + self.signal1_h * np.power(f, n / 2.)  # geometric average between height = width / width_scale and height = constant
                # print("SR{}: mass: {}, height: {}, width: {}".format(n+1, (self.mass_regions[n+1] + self.mass_regions[n]) / 4., self.signal1_h * np.power(f, n) / 2., self.signal1_h * np.power(f, n) * self.width_scale /  (1. - self.split) / 2.))

        # Equations for the lines that make up the signal and sideband regions
        self.down_line = lambda x: np.tan(np.pi/4 - self.sideband_angle) * (x - self.sideband_lower_point_x) + self.sideband_lower_point_y
        self.signal_down_line = lambda x: np.tan(np.pi/4 - self.signal_angle) * (x - self.s1_lower_point_x) + self.s1_lower_point_y
        self.signal_up_line = lambda x: np.tan(np.pi/4 + self.signal_angle) * (x - self.s1_lower_point_y) + self.s1_lower_point_x
        self.up_line = lambda x: np.tan(np.pi/4 + self.sideband_angle) * (x - self.sideband_lower_point_y) + self.sideband_lower_point_x
        self.split_down_line = lambda x: np.tan(np.pi/4 - self.split_angle) * (x - self.split_lower_point_x) + self.split_lower_point_y
        self.split_up_line = lambda x: np.tan(np.pi/4 + self.split_angle) * (x - self.split_lower_point_y) + self.split_lower_point_x
        # Lines that make up the n negative mass region line
        self.mass_line = lambda x, n: - x + self.mass_regions[n]
        # Lines for the first sideband region
        self.d1_line = lambda x: (self.sideband_lower_point_y - self.s1_lower_point_y) / (self.sideband_lower_point_x - self.s1_lower_point_x) * (x - self.s1_lower_point_x) + self.s1_lower_point_y
        self.u1_line = lambda x: (self.sideband_lower_point_x - self.s1_lower_point_x) / (self.sideband_lower_point_y - self.s1_lower_point_y) * (x - self.s1_lower_point_y) + self.s1_lower_point_x


   # Function for plotting the signal/sideband regions
    def plotRegions(self, figure_name):

        # Activate the specified figure
        plt.figure(figure_name)

        # The 10 mass region lines
        for i in range(self.num_signal_regions + 1):
            if i == 0:
                x_tmp, y_tmp = self.s1_lower_point_x, self.s1_lower_point_y
            else:
                x_tmp = self.sideband_lower_point_x + (self.mass_regions[i] - self.mass_regions[1]) / (1+np.tan(np.pi/4-self.sideband_angle))
                y_tmp = self.mass_line(x_tmp, i)
            x, y = [x_tmp, y_tmp], [y_tmp, x_tmp]
            plt.plot(x, y, 'r', linewidth=2)

        # The "long" lines...
        x_max_split = self.split_lower_point_x + (self.mass_regions[self.num_signal_regions] - self.mass_regions[1]) / (1+np.tan(np.pi/4-self.split_angle))
        y_max_split = self.mass_line(x_max_split, self.num_signal_regions)
        x_signal_max = (self.mass_regions[self.num_signal_regions] - self.mass_regions[0]) / (1+np.tan(np.pi/4-self.signal_angle)) # The x-coordinate for where the long signal region ends
        x1, y1 = [self.sideband_lower_point_x, x_tmp], [self.down_line(self.sideband_lower_point_x), y_tmp]
        x1_split, y1_split = [self.split_evenlower_point_x, x_max_split], [self.split_down_line(self.split_evenlower_point_x), y_max_split]
        x2, y2 = [self.s1_lower_point_x, self.s1_lower_point_x+x_signal_max], [self.signal_down_line(self.s1_lower_point_x), self.signal_down_line(self.s1_lower_point_x+x_signal_max)]
        x3, y3 = y2, x2
        x4, y4 = y1, x1
        x4_split, y4_split = y1_split, x1_split

        plt.plot(x1, y1, 'r', linewidth=2)
        plt.plot(x2, y2, 'r', linewidth=2)
        plt.plot(x3, y3, 'r', linewidth=2)
        plt.plot(x4, y4, 'r', linewidth=2)
        if self.split:
            plt.plot(x1_split, y1_split, 'r', linewidth=2)
            plt.plot(x4_split, y4_split, 'r', linewidth=2)

        # The special first sideband region lines
        x5, y5 = [self.s1_lower_point_x, self.sideband_lower_point_x], [self.d1_line(self.s1_lower_point_x), self.d1_line(self.sideband_lower_point_x)]
        x6, y6 = [self.s1_lower_point_y, self.sideband_lower_point_y], [self.u1_line(self.s1_lower_point_y), self.u1_line(self.sideband_lower_point_y)]

        plt.plot(x5, y5, 'r', linewidth=2)
        plt.plot(x6, y6, 'r', linewidth=2)



# Using the two fatjets chosen to correspond to the b-jets, count the number of events in signal and sideband regions.
class MassEventSelection(Module):

    def __init__(self, isData=False, btag="particleNet", new_mass_grid=None):

        self.isData = isData
        self.btag = btag
        self.new_mass_grid = new_mass_grid # Which mass grid to be used. True yields ParticleNet default.

    def beginJob(self):

        # Set some variable depending on which b-tagger we're using
        if self.btag == "particleNet":
            self.trainings = ["Original", "Retrained", "Alternative"]
            ts = [t.replace("Original", "") for t in self.trainings]
            self.mass = [("particleNet_mass%s" % t if self.isData else "particleNet_mass%s_corr" % t) for t in ts]
            self.mass_systs = ['nominal'] if self.isData else ['nominal', 'JMR_up', 'JMR_down', 'JMS_up', 'JMS_down']
            self.new_mass_grid = True if self.new_mass_grid is None else self.new_mass_grid
        elif self.btag == "btagHbb":
            self.trainings = [""]
            self.mass = ["msoftdrop"]
            self.mass_systs = ['nominal']
            self.new_mass_grid = False if self.new_mass_grid is None else self.new_mass_grid
        else:
            raise ValueError("Invalid b-tagger.")

        # Instantiate mass grid
        self.mass_grid = MassGrid(self.new_mass_grid)


    # Run once at the end
    def endJob(self):
        pass


    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        # Create branches that save the signal mass regions for the two fat jets
        for t in self.trainings:
            for syst in self.mass_systs:
                self.out.branch("FatJets_massSumRegion_%s%s_%s" % (self.btag, t, syst),"I") # The signal/sideband bin number
                self.out.branch("FatJets_massDiffRegion_%s%s_%s" % (self.btag, t, syst),"I") # Sideband or signal


    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        # Get the two chosen fatjets
        fatjets = Collection(event, "FatJet") # AK8 jets
        try:
            fatjet_a = fatjets[getattr(event, "FatJetIndexA_%s" % (self.btag))]
            fatjet_b = fatjets[getattr(event, "FatJetIndexB_%s" % (self.btag))]
        except IndexError:
            for i, m in enumerate(self.mass):
                for syst in self.mass_systs:
                    self.out.fillBranch("FatJets_massSumRegion_%s%s_%s" % (self.btag, self.trainings[i], syst), -1)
                    self.out.fillBranch("FatJets_massDiffRegion_%s%s_%s" % (self.btag, self.trainings[i], syst), -1)
            return True

        # Mass region filling for nominal mass and systematic variations
        for i, m in enumerate(self.mass):
            for syst in self.mass_systs:

                mass_variable = m if syst == 'nominal' else m + '_' + syst

                mass_a = getattr(fatjet_a, mass_variable)
                mass_b = getattr(fatjet_b, mass_variable)

                # Find which mass signal region the jets potentially belong to. It checks the "upper"/"lower" edges. Region 0 corresponds to outside the area of interest
                mass_sum_region = 0
                mass_difference_region = 0

                for n in range(self.mass_grid.num_signal_regions + 1):
                    if mass_b < self.mass_grid.mass_line(mass_a, n):
                        mass_sum_region = n
                        break

                # Below the Down line, i.e. outside the area of interest
                if mass_b < self.mass_grid.down_line(mass_a) or mass_sum_region == 0:
                    pass

                # Inside the D sideband
                elif mass_b < self.mass_grid.signal_down_line(mass_a):
                    mass_difference_region = 1
                    if mass_sum_region == 1 and mass_b < self.mass_grid.d1_line(mass_a):
                        mass_difference_region = 0

                # Inside the lower part of the split signal region
                elif mass_b < self.mass_grid.split_down_line(mass_a):
                    mass_difference_region = 2

                # Inside the central part of the signal region
                elif mass_b < self.mass_grid.split_up_line(mass_a):
                    mass_difference_region = 3

                # Inside the upper split part
                elif mass_b < self.mass_grid.signal_up_line(mass_a):
                    mass_difference_region = 4

                # Inside the U sideband
                elif mass_b < self.mass_grid.up_line(mass_a):
                    mass_difference_region = 5
                    if mass_sum_region == 1 and mass_b < self.mass_grid.u1_line(mass_a):
                        mass_difference_region = 0

                # Above the Up line, i.e. outside the area of interest
                else:
                    pass

                if not mass_difference_region:
                    mass_sum_region = 0

                self.out.fillBranch("FatJets_massSumRegion_%s%s_%s" % (self.btag, self.trainings[i], syst), mass_sum_region)
                self.out.fillBranch("FatJets_massDiffRegion_%s%s_%s" % (self.btag, self.trainings[i], syst), mass_difference_region)

        return True
