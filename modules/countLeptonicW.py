#!/usr/bin/env python3
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

class CountLeptonicW(Module):
    def __init__(self):
        pass

    def beginJob(self):
        pass

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        # Create branch that identifies the ttbar channel
        self.out.branch("nLeptonicW", "I")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        partons = Collection(event, "GenPart") # Gen partons

        # Find gen partons whose mother exists and was a W boson but that are not themselves a W boson
        Wdaughters = [p for p in partons if p.genPartIdxMother >= 0 and abs(partons[p.genPartIdxMother].pdgId) == 24 and not abs(p.pdgId) == 24]

        # Count number of leptonic W boson decays.
        # Count based on number of daughter neutrinos instead of charged leptons because additional charged leptons can be produced in a radiative decay (with gamma->e+e-)
        nLeptonicW = 0

        for daughter in Wdaughters:
            if abs(daughter.pdgId) == 12 or abs(daughter.pdgId) == 14 or abs(daughter.pdgId) == 16:
                nLeptonicW = nLeptonicW + 1

        # Fill the branch
        self.out.fillBranch("nLeptonicW", nLeptonicW)

        return True
