#!/usr/bin/env python3
import ROOT
ROOT.PyConfig.IgnoreCommandLineOptions = True
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection

# Adds branches with the Jet Mass uncertainties for ParticleNet
class JetMassUncertainties(Module):

    def __init__(self, year):
        self.year = year

    def beginJob(self):

        # Check the different mass types for particleNet mass
        self.mass_types = ["mass", "massRetrained", "massAlternative"] # Currently hardcoded...

        # Length variable for branches
        self.lenVar = "nFatJet"

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree

        mass_types = self.mass_types.copy() # Copy list so we can modify the original one

        # Loop over the different types of particleNet masses
        for mass_type in mass_types:

            # Remove mass types that do not exist in the input files
            if not inputTree.GetBranchStatus("FatJet_particleNet_%s" % mass_type):
                self.mass_types.remove(mass_type)
                continue

            # Create branches that save the total corrected masses
            self.out.branch("FatJet_particleNet_%s_corr" % mass_type, "F", lenVar=self.lenVar)
            self.out.branch("FatJet_particleNet_%s_corr_JMR_up" % mass_type, "F", lenVar=self.lenVar)
            self.out.branch("FatJet_particleNet_%s_corr_JMR_down" % mass_type, "F", lenVar=self.lenVar)
            self.out.branch("FatJet_particleNet_%s_corr_JMS_up" % mass_type, "F", lenVar=self.lenVar)
            self.out.branch("FatJet_particleNet_%s_corr_JMS_down" % mass_type, "F", lenVar=self.lenVar)

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Returns JMS for corrections for ParticleNet mass regression
    def getJMS(self):

        if self.year == 2016:
            jms = 1
            uncert_up = 0.002
            uncert_down = 0.002
        elif self.year == 2017:
            jms = 1.002
            uncert_up = 0.006
            uncert_down = 0.006
        elif self.year == 2018:
            jms = 0.994
            uncert_up = 0.001
            uncert_down = 0.007
        else:
            raise ValueError("Year not defined: %i" %self.year)

        return [jms, jms+uncert_up, jms-uncert_down]

    # Returns JMR for corrections for ParticleNet mass regression
    def getJMR(self):

        if self.year == 2016:
            jmr = 0.028
            uncert_up = 0.035
            uncert_down = 0.021
        elif self.year == 2017:
            jmr = 0.026
            uncert_up = 0.033
            uncert_down = 0.017
        elif self.year == 2018:
            jmr = 0.031
            uncert_up = 0.044
            uncert_down = 0.025
        else:
            raise ValueError("Year not defined: %i" %self.year)

        return [jmr, jmr+uncert_up, jmr-uncert_down]

    # Run once for each event
    def analyze(self, event):

        # Get JMS and JMR values
        jms_vals = self.getJMS()
        jmr_vals = self.getJMR()

        # Loop over all mass types
        for mass_type in self.mass_types:

            # List of corrected masses
            jet_masses_corr = []
            jet_masses_corr_jmr_up = []
            jet_masses_corr_jmr_down = []
            jet_masses_corr_jms_up = []
            jet_masses_corr_jms_down = []

            # Loop over all fatjets
            fatjets = Collection(event, "FatJet") # AK8 jets
            for fatjet in fatjets:

                # Get mass
                mass = getattr(fatjet, "particleNet_%s" % mass_type)

                # Get the type of particle the jet originated from
                # ValueError can happen when FatJet does not correspond to any generated jet
                # IndexError can happen due to some elements in the tail of the collection being dropped
                # More info: https://twiki.cern.ch/twiki/bin/view/CMSPublic/WorkBookNanoAOD
                genjets_ak8 = Collection(event, "GenJetAK8") # Generated AK8 jets
                try:
                    genjet_mass = getattr(genjets_ak8[fatjet.genJetAK8Idx], "mass")
                except (IndexError, ValueError):
                    genjet_mass = mass

                # Avoid incorrect jet matching
                try:
                    if not abs((genjet_mass-mass)/(genjet_mass+mass)) < 0.3:
                        genjet_mass = mass
                except ZeroDivisionError:
                    # print("Zero division, denominator: %f + %f" % (mass, genjet_mass))
                    genjet_mass = mass

                # Mass difference
                mass_diff = genjet_mass - mass

                # Corrected masses
                mass_corr = mass * jms_vals[0] - mass_diff * jmr_vals[0]
                mass_corr_jmr_up = mass * jms_vals[0] - mass_diff * jmr_vals[1]
                mass_corr_jmr_down = mass * jms_vals[0] - mass_diff * jmr_vals[2]
                mass_corr_jms_up = mass * jms_vals[1] - mass_diff * jmr_vals[0]
                mass_corr_jms_down = mass * jms_vals[2] - mass_diff * jmr_vals[0]

                # Add corrected masses
                jet_masses_corr.append(mass_corr if mass_corr > 0 else 0)
                jet_masses_corr_jmr_up.append(mass_corr_jmr_up if mass_corr_jmr_up > 0 else 0)
                jet_masses_corr_jmr_down.append(mass_corr_jmr_down if mass_corr_jmr_down > 0 else 0)
                jet_masses_corr_jms_up.append(mass_corr_jms_up if mass_corr_jms_up > 0 else 0)
                jet_masses_corr_jms_down.append(mass_corr_jms_down if mass_corr_jms_down > 0 else 0)

            # Fill corrected branches
            self.out.fillBranch("FatJet_particleNet_%s_corr" % mass_type, jet_masses_corr)
            self.out.fillBranch("FatJet_particleNet_%s_corr_JMR_up" % mass_type, jet_masses_corr_jmr_up)
            self.out.fillBranch("FatJet_particleNet_%s_corr_JMR_down" % mass_type, jet_masses_corr_jmr_down)
            self.out.fillBranch("FatJet_particleNet_%s_corr_JMS_up" % mass_type, jet_masses_corr_jms_up)
            self.out.fillBranch("FatJet_particleNet_%s_corr_JMS_down" % mass_type, jet_masses_corr_jms_down)

        return True
