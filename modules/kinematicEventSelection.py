#!/usr/bin/env python3

from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Saves the indices of the two chosen FatJets mostly likely corresponding to b quark decays.
# If skim==True, returns False if the event does not pass a loose version of the KinematicEventSelection for any systematic variation
class KinematicEventSelection(Module):

    def __init__(self, isData=False, btag="particleNet", ak8_pt_cut=300, ak4_pt_cut=300, ak8_eta_cut=2.4, ak4_eta_cut=3.0, deltaR_cut=1.4, JME_systs=[], skim=False):

        self.isData = isData
        self.btag = btag # Which double b-tag discriminator to use. btagDDBvLV2 seems random
        self.ak8_pt_cut = ak8_pt_cut # GeV. Minimum jet pt for the chosen FatJets
        self.ak4_pt_cut = ak4_pt_cut # GeV. Minimum jet pt for at least one AK4 jet in each event.
        self.ak8_eta_cut = ak8_eta_cut
        self.ak4_eta_cut = ak4_eta_cut
        self.deltaR_cut = deltaR_cut # distance parameter between jets
        self.JME_systs = ["original", "nom"] + JME_systs
        # print('JME variations:', self.JME_systs)
        # Note: pt_nom is after nanoAODTools jetReCalibrator. pt is the original pt branch from the input NANOAOD, before jetReCalibrator: https://github.com/cms-nanoAOD/nanoAOD-tools/blob/master/python/postprocessing/modules/jme/fatJetUncertainties.py#L471
        self.skim = skim

    def beginJob(self):

        # Count the number of events passed for debug
        self.nevents = 0 # Total number of events processed
        self.nevents_passed_ak8pt = 0 # Two fatjets found
        self.nevents_passed_ak4jet = 0 # Matching AK4 jet found/Total number of events passed


    def endJob(self):
        # Number of events passed
        print("\nTotal number of events (out of %f) that passed the Kinematic Event Selction (%s): %f" % (self.nevents, self.btag, self.nevents_passed_ak4jet))
        print("    - AK8 pT cut (>%i GeV): %f (%f%%)" % (self.ak8_pt_cut, self.nevents_passed_ak8pt, self.nevents_passed_ak8pt/self.nevents*100 if self.nevents else 0))
        print("    - Matching AK4 jet: %f (%f%%)" % (self.nevents_passed_ak4jet, self.nevents_passed_ak4jet/self.nevents*100 if self.nevents else 0))


    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        for s in self.JME_systs:
            self.out.branch("KinematicRegion_%s_%s" % (self.btag, s), "I") # Did the event pass the kinematic selection? (<2 FatJets = -1, fails_ak4_and_ak8 = 0, ak8_only = 1, ak4_only = 2, passes_ak4_and_ak8 = 3)
            self.out.branch("KinematicRegionLoose_%s_%s" % (self.btag, s), "I") # Did the event pass the loose kinematic selection? (<2 FatJets = -1, fails_ak4_and_ak8 = 0, ak8_only = 1, ak4_only = 2, passes_ak4_and_ak8 = 3)
            self.out.branch("passPS_%s_%s" % (self.btag, s), "O")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass


    # Returns True if both FatJets/AK8 jets pass the AK8 kinematic selection
    def passAK8Selection(self, fatjet_a, fatjet_b, ak8_pt_cut, ptsuffix):

        for fatjet in [fatjet_a, fatjet_b]:
            if not getattr(fatjet, "pt%s" % (ptsuffix)) > ak8_pt_cut:
                return False
            if not abs(fatjet.eta) < self.ak8_eta_cut:
                return False
        
        # Passed the selection
        return True


    # Returns True if at least one of the AK4 jets pass the AK4 kinematic selection
    def passAK4Selection(self, event, fatjet_a, fatjet_b, ak4_pt_cut, deltaR_cut, ptsuffix):

        jets = Collection(event, "Jet") # AK4 jets

        for j in jets:
            if not getattr(j, "pt%s" % (ptsuffix)) > ak4_pt_cut: continue
            if not abs(j.eta) < self.ak4_eta_cut: continue
            if not j.DeltaR(fatjet_a) > deltaR_cut: continue
            if not j.DeltaR(fatjet_b) > deltaR_cut: continue
            # Passed the selection
            return True

        return False


    # Run once for each event
    def analyze(self, event):

        # Compute the event weight
        if self.isData:
            weight = 1
        else:
            weight = event.weight_mc # Total weight

        # Count number of events
        self.nevents += weight

        iA = getattr(event, "FatJetIndexA_%s" % (self.btag))
        iB = getattr(event, "FatJetIndexB_%s" % (self.btag))

        if (iA < 0) or (iB < 0):
            # At least two FatJets not found (negative FatJetIndex)
            for s in self.JME_systs:
                self.out.fillBranch("KinematicRegion_%s_%s" % (self.btag, s), -1)
                self.out.fillBranch("KinematicRegionLoose_%s_%s" % (self.btag, s), -1)
                self.out.fillBranch("passPS_%s_%s" % (self.btag, s), False)
            return True

        # Get the two chosen FatJets
        fatjets = Collection(event, "FatJet") # AK8 jets
        fatjet_a = fatjets[iA]
        fatjet_b = fatjets[iB]

        passloose = False

        for s in self.JME_systs:
            passPS = False
            KinematicRegion = 0
            KinematicRegionLoose = 0
            ptsuffix = "" if s == "original" else "_" + s

            # Check if at least two FatJets does not pass the AK8 selection
            passAK8 = self.passAK8Selection(fatjet_a, fatjet_b, self.ak8_pt_cut, ptsuffix)
            # Check this event if no matching AK4 jets pass the selection
            passAK4 = self.passAK4Selection(event, fatjet_a, fatjet_b, self.ak4_pt_cut, self.deltaR_cut, ptsuffix)

            passAK8loose = self.passAK8Selection(fatjet_a, fatjet_b, self.ak8_pt_cut - 100, ptsuffix)
            passAK4loose = self.passAK4Selection(event, fatjet_a, fatjet_b, self.ak4_pt_cut - 100, 1.0, ptsuffix)

            if passAK8loose and passAK4loose:
                passloose = True

            if s == "nom" and passAK8: self.nevents_passed_ak8pt += weight
            if (passAK8 and passAK4):
                KinematicRegion = 3
                if s == "nom": self.nevents_passed_ak4jet += weight
                if event.passTrigger: passPS = True
            elif (not passAK8 and passAK4): KinematicRegion = 2
            elif (passAK8 and not passAK4): KinematicRegion = 1

            if (passAK8loose and passAK4loose): KinematicRegionLoose = 3
            elif (not passAK8loose and passAK4loose): KinematicRegionLoose = 2
            elif (passAK8loose and not passAK4loose): KinematicRegionLoose = 1

            self.out.fillBranch("KinematicRegion_%s_%s" % (self.btag, s), KinematicRegion)
            self.out.fillBranch("KinematicRegionLoose_%s_%s" % (self.btag, s), KinematicRegionLoose)
            self.out.fillBranch("passPS_%s_%s" % (self.btag, s), passPS)

        if self.skim:
            return passloose
        else:
            return True