#!/usr/bin/env python3
from PhysicsTools.NanoAODTools.postprocessing.framework.datamodel import Collection
from PhysicsTools.NanoAODTools.postprocessing.framework.eventloop import Module

# Dictionaries of MC cross section scales and errors
# Taken from: https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns

# QCD cross section values for different HT bins (in pb).
xsec_qcd_dict = {
    "HT100to200": (27990000, 4073),
    "HT200to300": (1712000, 376.3),
    "HT300to500": (347700, 74.81),
    "HT500to700": (32100, 7),
    "HT700to1000": (6831, 1.7),
    "HT1000to1500": (1207, 0.5),
    "HT1500to2000": (119.9, 0.06),
    "HT2000toInf": (25.24, 0.02)
}

# TTJets cross section values
# Note, older cross sections than for the separate channels below.
xsec_ttjets_dict = {
    "None": (831.76, 45.63), # Errors are specified as +19.77 -29.20 +35.06 -35.06...
    "HT-600to800": (1.821, 0.005332),
    "HT-800to1200": (0.7532, 0.002196),
    "HT-1200to2500": (0.1316, 0.0003817),
    "HT-2500toInf": (0.001430, 0.000017)
}

# TT separate channels
WtoL = 0.03*10.86 # https://pdg.lbl.gov/2023/tables/contents_tables.html
xsecTT = 833.9 # https://twiki.cern.ch/twiki/bin/view/LHCPhysics/TtbarNNLO
uncTT = 43.0 # https://twiki.cern.ch/twiki/bin/view/LHCPhysics/TtbarNNLO

BF2L = WtoL*WtoL
BF1L = WtoL*(1.-WtoL)*2.
BF0L = (1.-WtoL)*(1.-WtoL)

xsec_tt2L_dict = {
    "None": (xsecTT*BF2L, uncTT*BF2L)
}
xsec_tt1L_dict = {
    "None": (xsecTT*BF1L, uncTT*BF1L)
}
xsec_tt0L_dict = {
    "None": (xsecTT*BF0L, uncTT*BF0L)
}


# W+jets cross section value from AN
xsec_wjets_dict = {
    "HT-800toInf": (34, 0.1)
}

# Z+jets cross section value from AN
xsec_zjets_dict = {
    "HT-800toInf": (18.69, 0.06)
}

# TTH->bb cross section values from https://twiki.cern.ch/twiki/bin/viewauth/CMS/SummaryTable1G25ns#ttH
xsec_tthbb_dict = {
    "None": (0.2934, 0.038)
}

# TTZ->bb cross section values
xsec_ttzbb_dict = {
    "None": (0.1121, 0.0001591)
}

# ZZ->4b cross section values
xsec_zz4b_dict = {
    "None": (0.3763, 0.00357)
}

# GluGluToBulkGravitonToHHTo4B
xsec_gluglu_dict = {
    "M-1000": (0.2171, 0.00004591),
    "M-2000": (0.007043, 0.000001493)
}

# NMSSM Cascade. Gluino and squark pair-production xsecs from nnllfast-1.1. Values for 3200 are extrapolated.
xsec_nmssm_dict = {
    "mSUSY-800": (7.720E+00, 5.120E-01),
    "mSUSY-1000": (1.952E+00, 1.413E-01),
    "mSUSY-1200": (5.823E-01, 4.600E-02),
    "mSUSY-1400": (1.941E-01, 1.670E-02),
    "mSUSY-1600": (6.971E-02, 6.547E-03),
    "mSUSY-1800": (2.629E-02, 2.716E-03),
    "mSUSY-2000": (1.029E-02, 1.175E-03),
    "mSUSY-2200": (4.114E-03, 5.260E-04),
    "mSUSY-2400": (1.668E-03, 2.398E-04),
    "mSUSY-2600": (6.792E-04, 1.096E-04),
    "mSUSY-2800": (2.765E-04, 5.049E-05),
    "mSUSY-3000": (1.117E-04, 2.341E-05),
    "mSUSY-3200": (4.512E-05, 1.086E-05),
    "mSquark-800": (3.280E-01, 3.113E-02),
    "mSquark-1000": (6.880E-02, 7.701E-03),
    "mSquark-1200": (1.710E-02, 2.246E-03),
    "mSquark-1400": (4.780E-03, 7.324E-04),
    "mSquark-1600": (1.440E-03, 2.578E-04),
    "mSquark-1800": (4.570E-04, 9.536E-05),
    "mSquark-2000": (1.500E-04, 3.642E-05),
    "mSquark-2200": (5.070E-05, 1.433E-05),
    "mSquark-2400": (1.740E-05, 5.751E-06),
    "mSquark-2600": (5.990E-06, 2.331E-06),
    "mSquark-2800": (2.070E-06, 9.593E-07),
    "mSquark-3000": (7.200E-07, 3.943E-07),
    "mSquark-3200": (2.504E-07, 1.621E-07)
}

# H->bb branching fractions from published analysis. Not yet used.
br_higgs_dict = {
    "mH-30": 0.868,
    "mH-35": 0.867,
    "mH-40": 0.865,
    "mH-50": 0.858,
    "mH-60": 0.850,
    "mH-70": 0.840,
    "mH-80": 0.829,
    "mH-90": 0.816,
    "mH-100": 0.795,
    "mH-110": 0.749,
    "mH-120": 0.652,
    "mH-125": 0.581
}

# Saves the MC cross-sections for different HT bins
class CrossSectionScales(Module):
    def __init__(self):
        pass

    def beginJob(self):
        pass

    def endJob(self):
        pass

    # Run once for every input file
    def beginFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        self.out = wrappedOutputTree
        self.input_file = inputFile.GetName()

        # Get the HT bin from the input file name
        if "HT" in self.input_file and ("QCD" in self.input_file or "TTJets" in self.input_file or "WJets" in self.input_file or "ZJets" in self.input_file):
            self.dict_bin = [x for x in self.input_file.split("_") if "HT" in x][0]
        elif "GluGluToBulkGravitonToHHTo4B" in self.input_file:
            self.dict_bin = [x for x in self.input_file.split("_") if "M-" in x][0]
        elif "NMSSMCascade_m" in self.input_file:
            input_file_corrected = self.input_file.replace("mSUSY_", "mSUSY-").replace("mSquark_", "mSquark-")
            self.dict_bin = "_".join([x for x in input_file_corrected.split("_") if "mSUSY" in x or "mSquark" in x])
            self.dict_bin_BR = "_".join([x for x in input_file_corrected.split("_") if "mH" in x])
        else:
            self.dict_bin = "None"

        # Create branches that saves the cross section for all events
        self.out.branch("xsec", "F")
        self.out.branch("xsec_err", "F")

    def endFile(self, inputFile, outputFile, inputTree, wrappedOutputTree):
        pass

    # Run once for each event
    def analyze(self, event):

        # Get the cross section value, if not specified use 1
        if "QCD" in self.input_file:
            xsec, xsec_err = xsec_qcd_dict[self.dict_bin]
        elif "TTJets" in self.input_file:
            xsec, xsec_err = xsec_ttjets_dict[self.dict_bin]
        elif "TTTo2L2Nu" in self.input_file:
            xsec, xsec_err = xsec_tt2L_dict[self.dict_bin]
        elif "TTToSemiLeptonic" in self.input_file:
            xsec, xsec_err = xsec_tt1L_dict[self.dict_bin]
        elif "TTToHadronic" in self.input_file:
            xsec, xsec_err = xsec_tt0L_dict[self.dict_bin]
        elif "WJetsToQQ" in self.input_file:
            xsec, xsec_err = xsec_wjets_dict[self.dict_bin]
        elif "ZJetsToQQ" in self.input_file:
            xsec, xsec_err = xsec_zjets_dict[self.dict_bin]
        elif "ttHTobb" in self.input_file:
            xsec, xsec_err = xsec_tthbb_dict[self.dict_bin]
        elif "TTZToBB" in self.input_file:
            xsec, xsec_err = xsec_ttzbb_dict[self.dict_bin]
        elif "ZZTo4B01j_5f" in self.input_file:
            xsec, xsec_err = xsec_zz4b_dict[self.dict_bin]
        elif "GluGluToBulkGravitonToHHTo4B" in self.input_file:
            xsec, xsec_err = xsec_gluglu_dict[self.dict_bin]
        elif "NMSSMCascade_m" in self.input_file:
            xsec, xsec_err = xsec_nmssm_dict[self.dict_bin]
        else:
            xsec, xsec_err = 1, 0

        # Fill the branches
        self.out.fillBranch("xsec", xsec)
        self.out.fillBranch("xsec_err", xsec_err)

        return True
